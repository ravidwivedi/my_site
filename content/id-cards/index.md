---
title: "Problems with National ID Cards"
date: 2022-03-12T12:57:50+05:30
draft: false
---
- RS Sharma, former director general of the UIDAI shared his Aadhaar number on Twitter challenging people to show “one concrete example where you can do any harm to me!” Within hours, Twitter users managed to dig out his personal address, phone number, PAN number and [other personal information](https://thewire.in/tech/trai-rs-sharma-aadhaar) which shows how insecure Aadhar is.

- Highlighting the [security flaws](https://scroll.in/article/1025342/first-it-was-activists-now-even-government-bodies-are-flagging-security-flaws-with-aadhaar) of Aadhar card system in India.

- History shows that the victims of a flawed scheme are almost always the poor, the marginalised and the vulnerable. This is [true of Aadhaar as well](https://medium.com/karana/a-rant-on-aadhaar-6213e002f064#.lsklysxx2). 

- Digital ID, writ large, poses one of the [gravest risks](https://www.wired.com/story/digital-ids-are-more-dangerous-than-you-think/) to human rights of any technology that we have encountered. Worse, we are rushing headlong into a future where new technologies will converge to make this risk much more severe. 

	The only data which cannot be abused is the one never collected.

- National ID cards [won't make us more secure](https://www.schneier.com/essays/archives/2004/04/a_national_id_card_w.html). 

- [5 problems with National ID Cards](https://www.aclu.org/other/5-problems-national-id-cards).

	The main points are: 

	(1) Terrorists and criminals will continue to be able to obtain -- by legal and illegal means -- the documents needed to get a government ID, such as birth certificates.

	(2) A National ID card won't be limited to its stated purpose. Once put in place, it will be used for other things as well. Social Security numbers, for example, were originally intended to be used only to administer the retirement program. But that limit has been routinely ignored and steadily abandoned over the past 50 years. A national ID system would threaten the privacy of citizens and gradually increase the control that government and business wields over everyday citizens.

	(3) What happens when an ID card is stolen? What proof is used to decide who gets a card? A national ID would require a governmental database of every person in the U.S. containing continually updated identifying information. It would likely contain many errors, any one of which could render someone unemployable and possibly much worse until they get their ""file"" straightened out.And once that database was created, its use would almost certainly expand. Law enforcement and other government agencies would soon ask to link into it, while employers, landlords, credit agencies, mortgage brokers, direct mailers, landlords, private investigators, civil litigants, and a long list of other parties would begin seeking access, further eroding the privacy that Americans have always expected in their personal lives.

	(4)  The end result could be a nation where citizens' movements inside their own country are monitored and recorded through these ""internal passports.""

	(5) ID cards would foster new forms of discrimination and harassment.

	(6) It will cost a lot of taxpayer's money which is not worth spending.
