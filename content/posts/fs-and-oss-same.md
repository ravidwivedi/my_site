---
title: "Are Free Software and Open-Source Software the same thing?"
date: 2021-09-25T03:40:00+05:30
---

Practically, the terms free software and open source software refer to the same class of software.

The real difference between these two terms is not the actual software but the values behind the respective movements.

Free Software movement campaigns for users' freedom and that users must control the software running in their own devices. While, open-source movement does not put the issue in ethical terms like users' freedoms or rights, but this is rather a development model for them. 

Perhaps the difference would be more clear if we look into why open-source movement was created when free software movement already existed. [Quoting Open Source Initiative on coining the term open source](https://opensource.org/history), "The “open source” label was created at a strategy session held on February 3rd, 1998 in Palo Alto, California, shortly after the announcement of the release of the Netscape source code. The strategy session grew from a realization that the attention around the Netscape announcement had created an opportunity to educate and advocate for the superiority of an open development process. The conferees believed the pragmatic, business-case grounds that had motivated Netscape to release their code illustrated a valuable way to engage with potential software users and developers, and convince them to create and improve source code by participating in an engaged community. The conferees also believed that it would be useful to have a single label that identified this approach and distinguished it from the philosophically- and politically-focused label "free software." Brainstorming for this new label eventually converged on the term "open source", originally suggested by Christine Peterson." 

Open Source movement was created in rejection to free software principles. Free Software is a social movement to create a Free Society based on sharing knowledge and building commons. Open Source is a term created by people who are uncomfortable talking in terms of ethical issues like freedom. 

I would like to quote Pirate Praveen who beautifully illustrated the difference with an analogy, "It is like [five blind people describing an elephant](https://en.wikipedia.org/wiki/Blind_men_and_an_elephant). They are all describing the same thing, but people get a different idea from each of their descriptions. Similarly, the underlying software is the same, but different terms convey different meanings. So, the answer to why you need such software is different too. We think why we care about something is important too. In some situations, this difference leads to different answers as well. For example Tivo, a TV recording device runs on Linux kernel. [Open Source proponents are happy](https://lkml.org/lkml/2007/6/13/289) because the software code is available and it is powerful, but [free software proponents are unhappy because users cannot run their modified versions](https://en.wikipedia.org/wiki/Tivoization)."

I would like to quote Praveen again to elucidate the difference in one line, "Open Source wants to create better software, Free Software wants to create better society."

Free Software is necessary to build a free(as in freedom) society. If you would like to build a free society, please use the term 'Free Software'. 

In the end, I would also like to [quote FSF Europe on the same](https://fsfe.org/freesoftware/comparison.en.html) which says that we should not divide our community based on what term their members choose:

> This diversity is good, as it shows that Free Software provides many advantages in many different areas of our life. But we should not divide our community just by the term someone prefers. No matter what term someone uses and what their initial motivation is, in the end they works on the same set of software and on the enhancement of software freedom and any other aspect of Free Software. 

<b>Last updated on: Thursday 02 December 2021</b> to incude the point that it is not a good idea to divide the community on the basis of what terms people prefer.  

### Further Reading:

- [Why Open Source Misses the Point of Free Software](https://www.gnu.org/philosophy/open-source-misses-the-point.en.html)
- [Evgeny Morozov's article on the same topic](https://thebaffler.com/salvos/the-meme-hustler)
- [Why "Open Source" still misses the point!](https://abhas.io/open-source-still-misses-the-point/)
- [Why Open Source Misses the Point of Free Software And Ruins the Moral Fabric of the Tech Community](https://asd.learnlearn.in/open-source-misses-point-woefully/)
- [Free Software, Open Source, FOSS, FLOSS - same same but different](https://fsfe.org/freesoftware/comparison.en.html).
- [Why “Free Software” is better than “Open Source”](https://www.gnu.org/philosophy/free-software-for-freedom.html).
