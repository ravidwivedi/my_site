---
title: "Scribus: When Freedom-Respecting Software Saved The Day" 
date: 2021-11-24
draft: false
---

Please check [this URL](/free-software) to see what freedom-respecting software means or look at the poster attached below.

<figure>
<figcaption>  
<a title="Jeison Yehuda Amihud, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:4-freedoms-poster.png"><img width="256" alt="4-freedoms-poster" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/4-freedoms-poster.png/256px-4-freedoms-poster.png"></a>
<br>
Credits: Jeison Yehuda Amihud
<br>
Source: https://www.gnu.org/graphics/amihud-4-freedoms.html 
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure> 

### The story

The full story is [here](https://poddery.com/posts/4691002). I have sumarized it below. 

Janayugom is a newspaper in Kerala, India, which publishes news in Malayalam language. Earlier they were using Adobe Pagemaker for publishing which only had support for ASCII encoding. They considered using a better software with more features. They briefly considered the proprietary software Adobe InDesign, but then realized that the use of software requires subscription which they could not afford. Then, through some contacts, the newspaper came to know about freedom-respecting software 'Scribus', which they now use to publish. 

Scribus is a desktop publishing software which earlier supported publishing only in Latin languages -- like English, Spanish etc. The developers of a community project funded by the Oman Government, called HOST-Oman (House of Open Source Technologies - Oman), introduced support for Non-Latin languages with Complex Text Layout in Scribus. This was done to introduce Arabic support in Scribus. Due to this, the Malayalam support was added in Scribus. And therefore, Janayugom newspaper now publishes using Scribus. They don't have to pay any license fee or subscription to anyone. They are also free to make any further changes in the software if the need arises. Scribus respects freedom in all apects as shown in the poster above.  

Thanks to all the people who made it possible!  

This is one story of freedom-respecting software serving the humanity. If the source code of Scribus was not available, was it possible to introduce the support for Non Latin languages, like Arabic and Malayalam? We would have to beg the developer to add the feature. What would have happened if the developer doesn't want to add the feature we want? Since Scribus is freedom-respecting software, the users have the source code (Freedom 1), and therefore they can add the feature themselves. Also, due to the freedom to use the software(Freedom 0), the newspapers can use it for any purpose they want without asking for anyone's permission. Another freedom that was exercised was the freedom to share the modified versions(Freedom 3). So, if someone introduces a feature in the software, it benefits the whole society. In this case, the developers in Oman introduced a feature which benefited newspapers in Kerala to publish. 

To understand the value of freedom given by freedom-respecting software, think of what the world would have been if all the software was nonfree/proprietary software. Usually, with nonfree software:

-  You need to ask permission to use it from the developer of a nonfree software.

-  You don't have the source code, so you cannot study, inspect, adapt the program. 

- No freedom to share the software with others. 

This is a real example to show how freedom-respecting software empowers users. There are endless possiblities if the software respects freedom. Nonfree software serves the developer of the software while Free Software (same as freedom-respecting software) serves the user. 

We need freedom-respecting software to build a free society. Nonfree/proprietary software, on the other hand, [is a malware](https://gnu.org/malware), [a cancer that needs to be eradicated completely](https://videos.fsci.in/videos/watch/6ccd62b6-0547-41e6-8860-f718bd57d427). 
