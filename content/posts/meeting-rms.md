---
title: "My Meeting with Richard Stallman"
draft: false
date: 2021-07-31
---
Updated on 14 December 2021 to add my photo with Richard Stallman.

This happened on 1st March 2016, more than 5 years before writing this article, and so I do not remember much. This is my recreation from the memory. Some things from the meeting are naturally forgotten by me. And other things I don't remember correctly.

At that time, I was a student studying in an  undergraduate from Acharya Narendra Dev College which is a constituent college of University of Delhi.

Our principal, Dr. Savithri Singh, was addressing all the students in a common hall (for some reason I don't remember). In the discussion, she said that the founder of Linux was coming to our college that day and whoever is interested to meet him can come to the principal office at 4 PM. I was sitting with my friend, Shivam Rai, when the principal announced this. He insisted that we should go and meet him. I am from mathematics background and hardly knew anything about Linux. I heard the word Linux and knew that it is a kernel of some operating system. But nothing more. 

Just around 4 PM, I was a bit tired and wanted to go back to my room and sleep. I was not completely sure what type of conversation can I do with this person who was visiting the college. I did not think of people from software industry like that person turned out to be. Furthermore, I thought they discuss programming or coding like mathematicians discuss theorems and proofs. Turns out it was a right decision to go to principal office and meet him.  

Around 4 PM, my friend said that we can now go to principal office to meet the visitor. Upon his insistence, I agreed. We went to the principal office and after some time, the visitor arrived along with a person from Kerala. My friend and I shook hands with the visitor, and he said, "Hi, my name is Richard Stallman". We also introduced ourselves. The person from Kerala also introduced himself. Immediately, Richard Stallman asked us a question in a joking manner, "If I were to start a company in India, what should I name it?". We were silent. Then he said, "Mahadeva" followed by a big laughter. 

Turned out that, even though the principal invited the whole college to meet Richard Stallman, only two of us actually went to meet him.

Next, since the principal said that he is a founder of Linux, we asked about it to him. Something along the lines of "You are the founder of Linux?". And he became very angry and said, "You should ask that question from Linus Torvalds." I was confused at this point, and so was my friend. 

Then we asked, "what do you work for?". 

He said, "I work for GNU."

I asked, "What's that?"

He said, "GNU's not Unix."

I was very confused at this point as I didn't know what's GNU, what's Unix, and we already pissed him off by associating him with Linux. 

Then, he said, "I work for free software."

I got more confused. Why would someone work for free-of-cost software?

Then he explained what is free software. 

He said, "Free Software is software that gives users four freedom. 

Freedom 0 is the freedom to run the software as you wish.

Freedom 1 is the freedom to study and modify the program. You need to have the source code of the program to exercise this freedom.

Freedom 2 is the freedom to share the program.

Freedom 3 is the freedom to share your modified versions.

The 'free' does not mean free of cost. It means freedom. Think of it like free speech and not free beer."

At that point, I only knew that a lot of programs do not have freedom 1. I didn't know that a lot of software do not give users the freedom to share or run. So naturally, I didn't understand why he has to emphasize these freedoms and why they are important. Also, I used to think every software have freedoms 0,2,3. I remember having a (wrong) notion  that the freedom to study and modify the source code is for the programmers only. 

Then, he said, "Any software which does not give users these four freedoms is called a nonfree/proprietary software. A proprietary software is under developer's control, and you are at the mercy of the developer. A free software is controlled by its users. I use only free software and reject all proprietary software and, you should do it too."

I was getting some understanding at this point. Also, he was explaining all this with a lot of passion and enthusiasm, and this attracted me to listen to him. He was not boring, and the concepts he told us were new to us. We never saw ethical issues in software like the ones he told us.

Further, he told us that Microsoft Windows is an example of a proprietary software. It mistreats its users, it is a malware and a backdoor was also found in Microsoft Windows sometime ago.

I didn't know what backdoor means. Maybe he explained there and I forgot.

Richard Stallman has hearing problems, so we had to ask our questions in a very loud voice. 

My friend then asked about Apple's software. Something like "Is Apple's software secure?"

Stallman replied something related to Apple's locking of user and Apple's software is also nonfree. He said a lot, which I forgot.

I remember that he also said that Google services are surveillance systems, and he does not use them. He insisted that we should also refuse to use them. NSA spies on Americans as well as other countries' citizens using Google services like Gmail, Google Maps.

He also mentioned that Google Maps does not load if he disables the Javascript code sent by the site. Earlier, it used to load without Javascript. He kind of implied that sites should load without Javascript. And, he probably gave a few more examples when disabling Javascript does not load the website. 

At that time, I didn't understand why would someone disable Javascript when visiting a website.

Then, our principal reminded us to take photos with Richard Stallman. Stallman agreed with the condition that we never upload his photo on Facebook, Instagram or WhatsApp, because that company is a surveillance engine, and we should not feed it. 

We agreed to the condition and clicked photos. 

<figure>
<img src="/images/photo-with-rms.jpg" width=400 height=300>
<figcaption>My photo with RMS</figcaption>
</figure>

Richard Stallman was then treated with Dal Vada from our college canteen, and he really liked that a lot. 

He told us that we can drop him a mail at rms at gnu dot org, if we would like to contact him. 

After that, we greeted goodbye to all the people in the room, came out of the principal office and went back to our room. 

The next day, I told my friends in the college that I met Richard Stallman and explained to them what he told us (Frankly, I didn't understand it very well myself at that point). This is the first time I encountered the famous "I have nothing to hide" argument. When I told one college friend about surveillance that Facebook, Google etc. do, they said that he does not care about privacy, and he has nothing to hide. This type of argument made me very angry, as I remember. 

This meeting raised some questions in my mind. Every company that I knew was involved in our discussion as making software which was morally evil-- Microsoft, Apple, Google etc. I cared about my privacy and used to think of my dependence on Google and what can we do about it. I didn't like that, to use these services, we have to sign arbitrary terms and conditions by a company like Google which also says that we reserve the right to change these terms at any time in the future, and you agree to those future terms as well.

So, I had questions in the back of my mind. What are the alternatives of these softwares? What are the alternatives of Gmail? and so on. It took four years from there, in 2020, I started to act on switching my life to free software and reduce my dependence on Google and other such services. 

It was nice meeting you, RMS :)
