---
title: "Use Openstreetmap"
date: 2022-04-04T17:49:23+05:30
draft: true
---
### Problems with proprietary map services

Since Google Maps is one of the popular map services used around the world, so let me start from Google Maps. 

What are the problems with Google Maps? Think about it. It is very convenient and getting around might even seem very difficult, if not impossible, without it.

But, do you know how are you paying for using this free-of-cost service? 

Google records your location via the Google Maps, thus putting you under surveillance. It [hands over that data to USA government agencies](https://www.theguardian.com/world/2014/jan/27/nsa-gchq-smartphone-app-angry-birds-personal-data) as well. Who knows how many other entities know your live location from the data by Google Maps? 

Further, Google Maps are free as in "free beer", not as in "free speech". Basically, Google Maps is free-of-cost but it does not respect users' freedom. Google owns the data of Google Maps and therefore Google can even refuse you to use it. You do not have freedom to use it as you wish or freedom to modify the maps. 

Since Google is a surveillance company, I think it is wise not to use any of its Google Map services. Similar thing applies to other proprietary mapping services as well, where the mapping data is exclusively owned by them and you cannot use it without their permission.

### Freedom with OpenStreetMap 

[OpenStreetMap](https://www.openstreetmap.org) is a project that creates and distributes [free](http://www.openstreetmap.org/copyright) geographic data for the world. You are [free to share, create and adapt the OpenStreetMap](https://opendatacommons.org/licenses/odbl/summary/) provided that you credit the OpenStreetMap contributors, release the modified work under the ODbL license and redistribute your changes without using restrictive technologies like DRM. So, you don't need anybody's permission to use these maps, unlike Google Maps.

There are many [Free Software](/free-software) apps which can be used to access the OpenStreetMap data, like [Organic Maps](https://f-droid.org/en/packages/app.organicmaps/) is one such nice Android app that I use.

I don't use Google Maps at all and I contribute to OpenStreetMap so that it benefits the community. Please join me in contributing to OpenStreetMap and use it for getting directions. If you see any area around you which is not mapped, please add it to OpenStreetMap. If you are confused about something, please refer to their wiki or ask in the communication channels. For more details, please check the [Get Involved page](https://wiki.openstreetmap.org/wiki/Getting_Involved). Help others use and edit the map. Raising awareness is the most important thing you can do.

### Please Donate

The servers which power OpenStreetMap are run by OpenStreetMap Foundation and it is not cheap. [Donate](https://donate.openstreetmap.org/) some amount to help them in covering these costs. Also, you can donate to the app you are using to access or edit the OpenStreetMap data, for example, Organic Maps.

That's it for now. Hopefully, you will try OpenStreetMaps for your personal use too and contribute to make the world a better place!
