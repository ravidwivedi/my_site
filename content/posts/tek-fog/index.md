---
title: "Tek Fog App: An App With BJP Footprints for Cyber Troops to Automate Hate, Manipulate Trends"
date: 2022-03-05
---
From the analysis of The Wire, [Tek Fog is an app used by the Indian political party, BJP](https://thewire.in/tekfog/en/1.html) (Bhartiya Jasoos Party), to spread their misinformation and propaganda, and to dismantle opponents and critics, [to slut shame and harass women journalists](https://www.nytimes.com/2018/05/22/opinion/india-journalists-slut-shaming-rape.html) whose reports are critical of BJP. The app is used to manipulate trends on Twitter, by automatically sharing or retweeting posts, and promoting existing hashtags to trending levels, to make it look like some trends are more popular than they are. We came to know about this app from a whistleblower of the BJP IT Cell.

Tek Fog can [also use inactive WhatsApp accounts](https://thewire.in/tekfog/en/3.html) to forward messages to their contacts to spread BJP's propaganda. Further, the person whose account has been hijacked by the Tek Fog is accountable to spread the communal poison and BJP's propaganda while they were not the ones to send it.

Here are the links to articles and videos:

- [Part 1 analysis](https://thewire.in/tekfog/en/1.html) by The Wire.

- [Part 2 analysis](https://thewire.in/tekfog/en/2.html) by The Wire.

- [Part 3 analysis](https://thewire.in/tekfog/en/3.html) by The Wire.

- The [Bloomberg article](https://www.bloomberg.com/opinion/articles/2022-01-12/india-s-tek-fog-shrouds-an-escalating-political-war-against-modi-s-critics).

- [A video](https://thewire.in/government/tek-fog-and-a-dangerous-new-world) by The Wire explaining Tek Fog app.

- The Wire explains the consequences of Tek Fog by [comparing it with a fictional world where false supporters of the emergency drown out the real opponents](https://thewire.in/tech/tek-fog-social-media-manipulation-politics).
