---
title: "Privacy implications of DNS"
draft: true
---

DNS is short form of Domain Name System. Think of DNS as an address book of websites. When you enter the website URL like https://eff.org, your browser requests the IP Address of the website from your DNS provider, usually provided by your Internet Service Provider(like Jio), and the DNS provider returns a number like `173.239.79.196`(try searching this number in your URL bar). 

Your ISP can associate your personal IP address with the names of all the sites you connect to and store this data indefinitely.

Websites are usually censored via DNS. Your DNS provider does not tell your browser the correct IP Address of the website and it can't visit the site. This is the most popular method of blocking websites.  

I advice you to switch to a privacy-respecting DNS provider. First, visit [this link](https://www.privacytools.io/providers/dns/) to choose a DNS provider. Let's say you have choosen [Uncensored DNS](https://blog.uncensoreddns.org/) as your DNS provider. Copy the link of DNS hostname from their website -- `anycast.censurfridns.dk`. Now, in Android, go to Settings -> Connection Settings -> More Connection Settings -> Private DNS -> Check on Private DNS provider hostname -> Enter `anycast.censurfridns.dk`. 

Visit the [link](https://www.dnsleaktest.com/) and select the option 'Standard Test'. If your provider is Anycast, then your DNS provider is changed. 

Congratulations, your DNS provider has been changed. This method will also unblock all the websites censored at DNS level. 
