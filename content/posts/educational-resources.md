---
title: "Free the educational resources"
draft: true
---

This article is about digital educational resources like video lectures, lecture notes, academic papers etc, choosing an appropriate license and platform for them. It does not deal about the (deeper) issues like how education can be more accessible etc.    

What does it mean for educational resources to be really free (as in freedom)? Does releasing lecture videos on YouTube make it free?

To inspect whether an educational resource is free, I would like to ask the following questions:

1. Under which license is the work released?

    - Does the license permit users to copy and redistribute the material in any medium or format without asking for permission from the contributors?
    
    - Does the license permit commercial sharing without permission from the contributors?
    
    - Does the license permit modification of the work? 
    
    - Does the license permit redistribution of the modified works? 

2. Can it be viewed with using [free software](/posts/free-sw) only? 

3. Can the users access the material without giving away their privacy or any other fundamental human rights? 

4. Is the material downloadable in the user's device?

5. Is there an EULA to sign or DRM restrictions imposed on the material? (If the answer to 2 is yes, then the answer to 5 should be automatically no.)

### Arriving at a defintion of free educational resource 

Academic books are, in general, very expensive. People who would like to learn from an academic book, even for their courses, cannot really buy every book they would like to read (usually a small portion of an academic book is read) or like to use as a reference. One way to access such books is to go to a library and read books there or borrow from the library. The problem with this is that very often libraries have scarcity of books. In today's technology, we can easily copy a digital file, like an e-book or an academic paper, very easily in our own computers and that too at zero marginal cost. If sharing is allowed, then only person need to buy that book and they can share it with everyone else without charging them any money. People in academic institutes usually have access to internet and therefore can easily read these digital copies. Prohibiting sharing of such educational material will be against the freedom of the users and will also result in discrimination against the poor who cannot buy expensive books or papers. Many academic books are priced Rs 5000 per copy, which is not really accessible to people in India. 

So, I think we can conclude that sharing must be allowed in order for the educational resource to be free. 

Further, if people want to get printed copies and a shop can just print the digital file and sell it, this also makes the book more accessible than the original print sold by the publisher. So, I think commercial sharing must also be allowed in order for educational resources to be called 'free'.

Further, people should also get freedom to build upon or adapt the work according to their needs. In all the cases, the original authors will get attribution and that will respect their contribution.

Note that the commercial sharing is not usually allowed and the general copyright law even prohibits sharing of copies, but users of these resources do it anyway without regard for any copyright law. I don't see any professor or a writer of these books going bankrupt due to this practice. 

**Conclusion: Users of educational resources must have the following freedoms: commercial use, redistribution and modification.**



   
