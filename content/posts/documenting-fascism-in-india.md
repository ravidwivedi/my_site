---
title: "Documenting What Is Happening In Fascist India"
date: 2022-02-25
draft: false
---
India is moving towards a fascist state. The current times seem to have an important role in the history of India. For the curiosity of future generations on what Modi's India was like, I have decided to document some things on this blog. Personal experiences can be very powerful story teller.

First of all, we need to ask: What is fascism? Why am I calling India as 'fascist'? Why the label of fascism matters?

Fascism is not a well-defined concept. I will take the signs of fascism from [Arundhati Roy's interview with The Wire](https://thewire.in/politics/full-text-narendra-modis-star-is-falling-arundhati-roy).

According to her, following are the signs of fascism:

- **A deep social crisis which is seen as a threat to existing social hierarchies** 

	This sign can be seen in "social crisis" like, Mandal commission report, the rise of the new caste parties: Mayawati’s party (the Bahujan Samaj Party), Lalu Yadav’s (the Rashtriya Janata Dal), Mulam Yadav’s (the Samajwadi Party), all of them are threat for upper caste Hindus who are at the top of the caste hierarchy.

- **For some mythic, national past; fantasizing about a super pure, racial superman and the restoration of national vitality** 

	A society that believes Brahmins are Bhudevas (literally, a god on Earth) just has to make a lateral shuffle.

- **A mass movement with the support of some sectors of the national ruling class**

	 Examples of this are- Advani’s rath yatra; the movement to demolish the Babri Masjid. Simultaneously, the Indian markets open and privatisation comes in, which is going to now mitigate the whole business of reservation which has now become a huge issue in the UP Elections.

- **The emergence of a single dominant authoritarian party** 

	India under Modi is an [authoritarian state](https://thewire.in/politics/india-modi-authoritarianism) and it is very dominant too.

- **The fabrication of external and internal threat to the nation, often racial, religious or ethnic in form** 

	We can see that happening in the form of BJP spreading obvious misinformation and labelling everything as a foreign conspiracy, be it [defending themselves against allegations of inserting Pegasus spyware into citizens' phones](https://thewire.in/politics/pegasus-spyware-india-bjp-defence) or [defending themselves in Rafale case](https://thewire.in/politics/rafale-politics-bjp-conspiracy-hollande-revelations-congress). Even in the [Hijab ban case](https://economictimes.indiatimes.com/news/politics-and-nation/hijab-row-udupi-bjp-mla-calls-it-international-conspiracy-demands-nia-probe/videoshow/89547787.cms). It is also seen when they falsely claim that the [Muslim population is increasing and further that it is a threat to Hindus](https://www.hindujagruti.org/news/107001.html), despite Muslim population increase is a [bogus claim](https://thewire.in/communalism/lynchings-are-increasing-because-of-muslim-population-growth-bjp-mp).

- **An attack by an authoritarian state on the working class and an attempt to suppress any challenge to capitalism from below**

- **The foreclosure of democratic or autonomous means of expression**

	BJP government is telling us [what to wear](https://thewire.in/rights/hijab-ban-narendra-modi-muslim-women-rights), [what to eat](https://www.bbc.com/news/world-asia-india-40116811), [whom to marry](https://www.theguardian.com/world/2022/jan/21/they-cut-him-into-pieces-indias-love-jihad-conspiracy-theory-turns-lethal) etc. It also crushes opposition and dissent.

- **The existence of street fighting elements or militias to intimidate, terrorise and, in some cases, murder oppositional forces**

	[RSS Milita, Bajrang Dal, Vishwa Hindu Parishad (VHP)](https://www.theguardian.com/world/2020/feb/20/hindu-supremacists-nationalism-tearing-india-apart-modi-bjp-rss-jnu-attacks) are examples.

- **A militarized rhetoric of masculinity, anti-feminism, casteism, racism (in the cases of Europe and America), and Xenophobia.**

- **Creating paranoia in citizens' minds**

	[Inserting Pegasus spyware in citizens' phones](https://thewire.in/media/pegasus-project-spyware-indian-journalists).

According to [Wikipedia](https://en.wikipedia.org/wiki/Fascism):

"Fascism is a form of far-right, authoritarian ultranationalism characterized by dictatorial power, forcible suppression of opposition, and strong regimentation of society and the economy that rose to prominence in early 20th-century Europe."

India fits this definition too. Although it has not become completely a fascist state, but the government and their ideology is definitely fascist. 

These signs are very much present in the present-day India. If you read these signs, I don't think I need to mention how mightmarish they are and the situation comes at a cost of many citizens' life and compromise of the democratic values we believe in. And this is why it matters. 

India is going through unique challenges. Nobody knows the outcome of the actions of the fascist state. This uncertainty contributes to fear. See [this list](/remove-bjp) for how BJP is tearing India apart. There is [hope due to farmers' movement](https://ravidwivedi.in/posts/farmers-giving-hope/) and other mass protests in response to CAA bill](https://thewire.in/rights/caa-protests-worldwide), which is a huge [discrimination against Muslims](https://www.hrw.org/report/2020/04/09/shoot-traitors/discrimination-against-muslims-under-indias-new-citizenship-policy). There is hope due to India still conducting elections and that means there is a chance to change the government.  Below I present other people's testimonies of what they are going through.

My testimonies/experiences will be posted in the subsequent posts.

### Further Reading:

- The fear of being visited by official investigative agencies and even being arrested can [scare most people who may then decide that discretion is the better part of valour](https://thewire.in/rights/speak-out-get-raided-a-handbook-for-living-in-new-india). 

- Rana Ayyub's post on [India being a fascist state](https://ranaayyub.substack.com/p/india-is-a-fascist-state).

- [India: The New 'Republic of Fear'](https://thewire.in/rights/india-the-new-republic-of-fear).

- [Living in a woman’s body: hate has taken hold in India and I am restless with grief](https://www.theguardian.com/lifeandstyle/2022/feb/14/living-in-a-womans-body-hate-has-taken-hold-in-india-and-i-am-restless-with-grief).

- [Living through India’s pandemic: ‘Oxygen is the new currency’](https://amp.theguardian.com/books/2021/may/07/living-through-indias-pandemic-oxygen-is-the-new-currency).
