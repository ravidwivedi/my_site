---
title: "Hijab Ban Is An Attack On Muslim Women's Agency To Decide For Themselves"
draft: false
date: 2022-03-10T03:14:18+05:30
---
In January 2022, Six female Muslim students were told to [either remove their headscarves or stop attending class](https://indianexpress.com/article/cities/bangalore/hijab-row-udupi-college-asks-students-to-opt-for-online-classes-local-leaders-cite-divide-politics-7746661/) at a government college in Udupi, Karnataka, India. Soon after the event, other colleges of Karnataka followed suit [due to Hindu groups forcing boys to wear saffron shawls](https://www.indiatoday.in/india/story/after-hijab-row-in-karnataka-hindu-groups-force-boys-to-wear-saffron-shawls-in-classrooms-1908745-2022-02-04). Muslim students argued that wearing hijab is their fundamental right to practice religion. The same women were allowed to wear hijabs in classrooms earlier and were shocked by the school denying them access to education.

[Quoting Arunima](https://thewire.in/communalism/who-takes-exception-to-the-hijab), who put it brilliantly, "Any amount of logical rebuttals are of no value here as this has cleverly been turned into a question of upholding dress codes in schools, which with the legal turn is tied to a court judgment."

If we see the issue in isolation, we will miss the point. Karnataka is ruled by the Hindu nationalist BJP government which [spreads hatred towards Muslims and even supports killing them](/remove-bjp/#bigotry-and-hindu-nationalism). They are using open-ended questions like this to discriminate against Muslims. Yes, the question is complicated if you refer to the constitution. All this happening when five states are going through elections is not a coincidence either.

Let's ask some crucial questions:

- Should we deny Muslim women education if they wear headscarfs in classrooms?

- Why only hijab has come under fire?  Neither turban, nor bindi, nor saffron scarf, which were donned by students in direct counterprotest of their Muslim peers – has been brought up before the High Court?

- Why is that everything Hindu is Indian and cultural, while Muslim and Christian practices are not intrinsically Indian, they can never be culture, they will remain narrowly religious?

- What was the impact on Muslim women and society overall when hijab was banned in France?

- What would be the psychological impact on the students who are being denied entry by their own schools? for example, if a teacher or school denies you entry for a clothing you wear, which you think is an integral part of your culture, would you be able to integrate with the same education system if the ban is lifted tomorrow?

- Why are schools still not allowing after Muslim women not attending classes and many of them protesting against the move? Isn't it their responsibility to hear their voices and accommodate them?

- Isn't telling women what to wear and what not to wear a loss of their agency? Can't women think themselves?

- Why is all this happening in between elections of five states?

- What is the need to do all this when India is going through an economic crisis and an [unprecedented rise in unemployment](https://www.hindustantimes.com/india-news/unemployment-rate-highest-in-45-years-touches-6-1/story-VOxt06G3gCHFXkTf5DUv0J.html)?

In summary, I would like to say that we should let Muslim women decide if they want to wear hijab or not. Denying education for wearing hijab does not solve any problems. It is against the women's agency to decide for themselves, and against the freedom of religion. After all, allowing a headscarf is nothing harmful.

But the evil BJP government has one central goal-- to oppress Muslims and that is what it is doing.

### Links for further reading

- Hindutva bigotry [lies behind the attack on Hijab-Clad students in Karnataka](https://thewire.in/communalism/hindutva-bigotry-lies-behind-the-attack-on-hijab-clad-students-in-karnataka).

- [Why do secularists see Hinduism as ‘cultural’ and Islam as ‘religious’](https://thewire.in/rights/why-do-secularists-see-hinduism-as-cultural-and-islam-as-religious)?

- Activists say [hijab ban is tantamount to hate crime](https://www.thehindu.com/news/national/activists-say-hijab-ban-is-tantamount-to-hate-crime/article38409456.ece).

- The "confused" civil society in India has been debating if it is conditioning or choice in the matter of hijab, because [it can only give a free pass to Muslims who do not associate themselves with their faith](https://thewire.in/rights/make-no-mistake-progressive-indians-too-have-problems-with-overt-muslimness).

	Quoting the article, "Is it really the hijab of the Muslim that should be under scrutiny or the value system of this society where progressiveness has an aesthetic criterion? Civil society should be critically asking itself: "Who designed the ideal outfit of the progressive woman?" and "Who are they unconsciously answering to when they are scared to stand with a minority community facing such segregation?""

- [Dear Prime Minister, If You Really Stand With Muslim Women, Don’t Tell Us What To Wear](https://thewire.in/rights/hijab-ban-narendra-modi-muslim-women-rights).

- While the BJP represents itself as a secular feminist vanguard in the hijab row discourse, its actions and rhetoric [underline a potent bigotry and prejudice against Muslims](https://scroll.in/article/1019151/with-karnatakas-hijab-ban-the-us-stands-at-crossroads-over-its-relationship-with-the-modi-regime).

- [The right answer to the wrong hijab question is still a wrong answer](https://theprint.in/opinion/the-right-answer-to-the-wrong-hijab-question-is-still-a-wrong-answer/826663/) as the controversy was conjured up to reignite Hindu-Muslim divide, so that while the hoodlums do their job on the street, intellectuals debate the merits of hijab. Quoting the article, "It is distressing that the hijab-saffron scarf row in Karnataka is forcing even the well-meaning voices of modernity and liberalism to defend the regressive practice of covering up young women. Beyond whataboutery of ghoonghats and turbans, Indians shouldn’t allow this to become another political bruising Shah Bano moment that haunts our future....The assumption here is that young women do not wear hijab on their own volition. They are being coerced or pressurized. Hence, all Indians must step in to prevent this violation of freedom."

- [The Quint asks](https://www.thequint.com/voices/opinion/hijab-ban-women-legal-ramifications): In a country which suffers from extremely low rate of girl education, why should their uniform get any priority?

- [In Karnataka, it’s not about Hijab, but bigotry and apartheid](https://theprint.in/opinion/in-karnataka-its-not-about-hijab-but-bigotry-and-apartheid/824996/). 

- The French ban on hijab [actually alienated Muslim women from the French society](https://www.washingtonpost.com/politics/2019/06/03/how-will-austrias-new-headscarf-ban-affect-muslims/), as the data shows – they chose to leave the school system which lead to drop in employment rate, having more children after the ban, due to a feeling of being singled out, targeted and discriminated, in addition receiving the signal that their two identities were incompatible and that one could not be French without being secular as in law.
