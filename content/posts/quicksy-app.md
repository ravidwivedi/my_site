---
title: "Quicksy: Freedom and Privacy can be convenient too"
date: 2021-12-31
draft: false
---
Updated on Monday 03 January 2022, thanks to [this comment](https://post.lurk.org/@freebliss/107557993276058403).

I am happy to note that XMPP foundation [mentioned this blog post in their January 2022 newsletter](https://xmpp.org/2022/02/the-xmpp-newsletter-december-2021-january-2022/).

#### What is Quicksy? 

<figure>
<img src="/images/quicksy_logo.png" width="100" height="100">
<figcaption>Quicksy app logo</figcaption>
</figure>

[Quicksy](https://quicksy.im) is a chatting app for Android which is available on [Google Play Store](https://play.google.com/store/apps/details?id=im.quicksy.client&hl=en_IN&gl=US) and [F-Droid](https://www.f-droid.org/en/packages/im.quicksy.client/).  

Quicksy offers the same convenience, i.e., signup using phone number, as WhatsApp, Telegram and Signal. Signal respects users [freedom](/free-software) and privacy, but Quicksy, in addition, is interoperable while Signal is not. iOS does not have Quicksy but Monal or Siskin IM on ioS are compatible with Quicksy. 

[Here is a video tutorial to get started with Quicksy](https://yewtu.be/watch?v=pb7b6i7fgfk). 

<figure>
<img src="/images/Quicksy.png" width="150" height="300">
<figcaption>Welcome screen of Quicksy app</figcaption>
</figure>

**List of a few features of Quicksy**:

- Available on F-Droid and Play Store.

- Quicksy supports encryption by default and due to software code being available, we can verify that it does encrypt messages (without any cheating!). Encryption means only the users exchanging messages can read them. Quicksy respects users privacy in this way. The Quicksy server cannot read your messages.  

<figure>
<img src="/images/quicksy_omemo.png" width="150" height="300">
<figcaption>OMEMO Encryption in Quicksy app.</figcaption>
</figure>

- Very good video/audio call support.

<figure>
<img src="/images/quicksy_call.jpeg" width="150" height="300">
<figcaption>Video calling in Quicksy app. Credits: <a href="https://www.f-droid.org/en/packages/im.quicksy.client/">F-Droid Quicksy page</a></figcaption>
</figure>

- Sharing of photos/videos/voice message, in encrypted form. 

<figure>
<img src="/images/quicksy_pic.png" width="150" height="300">
<figcaption>Sharing pictures in Quicksy app.</figcaption>
</figure>

- If any of your contacts register with Quicksy service, then Quicksy automatically detects and shows them in your Quicksy contact list. 

- There are many apps and services compatible with Quicksy, like Conversations, Blabber, Monal etc and therefore, you are not locked into one service provider, unlike WhatsApp, Telegram, Signal.  

- One server goes down does not imply that the whole XMPP communications goes down. This is due to the decentralized nature of XMPP which Quicksy is a part of.  

- Easy migration: If Quicksy does something in future that you do not agree with, you can just use another XMPP service and still be able to talk to your contacts.

#### Why is Quicksy different?

Popular messenger apps like WhatsApp, Telegram and Signal have problems that users do not control them. For details, please [check this article](https://fsf.org.in/article/better-than-whatsapp). WhatsApp uses the control over its users to put them under surveillance. Telegram and Signal are free software but centralized. WhatsApp is nonfree software as well as centralized. Centralized services are easily suspectible to backdoors, can be compromised later on due to change of privacy policy or terms and conditions, are easily for government to ban etc. The owner of the platform dictates the decisions of a centralized system, so even if it is good for now, it cannot be trusted to remain good forever.  

Quicksy is [free/swatantra/mukt/libre software](/free-software) ('free' as in freedom) and therefore users can inspect, modify and share the software. If users want to add a feature, fix a problem with the software, they don't have to beg the developer. In other words, the software is under user's control. 

Quicksy is a part of XMPP protocol which decentralized and federated. To understand federation, we give example of mobile telecom operators. A person using a BSNL SIM can talk to a user having Vodafone SIM, by call and SMS. WhatsApp, Telegram and Signal do not give you that freedom. Your all the contacts need to be registered with the same service provider. These apps have a fundamental problem of vendor lock-in -- that if you uninstall WhatsApp, for example, you lose all your contacts. With federation, it is like changing SIM. If you have a problem with a telecom operator and you buy a new SIM of another company, you do not lose all your contacts. You can still call and message your earlier contacts. This way we can control our communications rather than relying on a single entity for our communications. 

Let's choose Quicksy for a free society rather than being locked by WhatsApp, Telegram or Signal. 

#### A word for Free Software Community

Dear Free Software Community, let's raise awareness about Quicksy. We don't have to recommend our geeky solutions to everyone. If some people switch to free software, decentralized and federated app like Quicksy, that is better compared to them finding xmpp/matrix/IRC difficult to use and not using it. We need to work to advertise this option and raise awareness about it. 

### Further Reading:

- [Introduction to Free Software](/free-software).

- [Why Free Software and decentralization are necessary for privacy](/posts/free-software-important-for-privacy).

- [WhatsApp is malware](/whatsapp).

- [What Does The Facebook Outage Teach Us](/posts/what-does-facebook-outage-teach-us).

- [Choosing a privacy-respecting chatting app](/posts/chatting-apps).

- [Reclaim privacy in instant messaging with Free Software and choice of service providers](https://fsci.in/blog/reclaim-privacy-messengers/).
