---
title: "Prav App Project Plans To Launch A Privacy Respecting Messaging Service Under A Cooperative Society, Which Needs Your Involvement"
draft: false
date: 2022-03-07
---
<b>Summary by Praveen: PravApp project is a plan to get a lot of people to invest small amounts to run an interoperable messaging service that is easier to join and discover contacts.

It will be based on [Quicksy](https://quicksy.im) which gives a similar experience to [WhatsApp](/whatsapp) without locking users to its service by interoperability with any [XMPP](https://xmpp.org) service provider.  

We want to offer this service under a Multi State Cooperative Society in India. Would you join the cooperative? 

Learn more at [prav.app](https://prav.app)</b>

<iframe src="https://social.masto.host/@praveen/107936691787584582/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe>

### The problem

In the era where massive surveillance is a [business for corporates](https://en.wikipedia.org/wiki/Surveillance_capitalism) and a [tool for government repression](https://en.wikipedia.org/wiki/COINTELPRO), corporates and governments [will do whatever they can](https://arstechnica.com/tech-policy/2019/10/35-billion-facial-recognition-lawsuit-against-facebook-moving-forward/) to collect your personal and sensitive data. [Proprietary Software](https://gnu.org/malware) is one root cause of this, because users do not control the software and they cannot verify if there is spyware present in the software. Another way this is done is by [locking users](https://en.wikipedia.org/wiki/Vendor_lock-in) into [centralized services](/glossary/#centralized-services). For example, it is [hard for people to move out of services like WhatsApp](https://seirdy.one/2021/01/27/whatsapp-and-the-domestication-of-users.html), because the service locks the user -- in other words, refusing to use the WhatsApp service leads to losing all the contacts and instead, the user needs to convince every contact to switch to a new service. Also, the whole world depending on a single company for their communications [is not sustainable](/posts/what-does-facebook-outage-teach-us/), or even wise.

We need a [Free Software](/free-software) chatting app, powered by Free Software services, which are decentralized and [federated](/glossary/#federated-services).

### Introducing Prav

I am a part of this project named Prav which is a, yet to be released, [Free Software](/free-software), decentralized, federated [XMPP](https://xmpp.org) chat app. Free Software allows users to verify the software for any spyware, and make changes. An example of federation is: A person having Airtel SIM can contact users using BSNL. This allows for more freedom and does not lock users into a single service. Users can change their SIM provider and can still talk to their already existing contacts. Prav will automatically [encrypt](/glossary/#end-to-end-encryption) the messages and therefore, even we cannot read your messages.

We want to give users more empowerment than just software freedom or freedom of service providers. We want diverse voices to take part in the decision-making. That's why we decided that we will register it as a [Multi State Cooperative Society](https://mscs.dac.gov.in/). This will ensure a democratic decision making structure. With involvement of many members in a cooperative society, we can safeguard the values behind the project (like providing freedom and privacy to users, ensuring an inclusive society, etc.). It is like the app asking you to vote for the privacy policy or the features to be put there, provided you are a member of the cooperative society. We also have a [Code of Conduct](https://prav.app/coc/) and a [Social Contract](https://prav.app/social-contract/) that we will follow.

**Since the goal is user empowerment, users need to get involved themselves. You can get involved in various ways: by [becoming a member](https://prav.app/become-a-member/) of the cooperative society, helping us financially, by buying the subscription, or advertising the concept and the app to your friends and contacts.**

Prav will be funded directly by the users in the form of a subscription fee, since we will not collect user data-- the privacy policy is yet to be drafted. We have estimated that if there are 10,000 users registered on Prav, the subscription fee per user for first 3 years will be ₹200, which amounts to ₹5-6 per month. I think users can afford this amount to exercise their fundamental right to privacy and the freedom to whisper without prying eyes.

The account registration will be done via phone number to make it convenient and easy to use. People who are not willing to give service their phone number can register on any other [XMPP service](https://joinjabber.org/) and still talk to Prav users. If you do not want to pay for the service, you can use the Android app [Quicksy](https://quicksy.im), Siskin IM for iOS and Monal for MacOS, and still contact Prav users. This freedom of choice gives people right to exercise their own acceptable levels of anonymity online. 

**Let's fix the problem of privacy ourselves before it is too late. I will update once the app is released and available for download. Stay tuned!**
