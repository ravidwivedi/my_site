---
title: "Early Signs of Fascism Can be Seen in India"
date: 2022-02-25
draft: false
---
TMC MP Mahua Moitra [argued that India is already showing early signs of fascism](https://thewire.in/politics/tmc-mp-mahua-moitra-criticise-government-parliamentary-speech).

Let's check India against [The 12 Early Warning Signs of Fascism](https://washingtonmonthly.com/2017/01/31/the-12-early-warning-signs-of-fascism/):

<img src="https://i0.wp.com/washingtonmonthly.com/wp-content/uploads/2017/01/12earlywarningsignsoffascism.jpg?w=320&ssl=1">

### 1. Powerful and continuing nationalism

This is very visible in India. For example, Devendra Fadnavis [said](https://www.dnaindia.com/india/report-if-you-want-to-live-in-this-country-you-have-to-say-bharat-mata-ki-jai-devendra-fadnavis-2197574), "If you want to live in this country, you have to say Bharat Mata Ki Jai". National pride has now become interlinked with a deep, [visceral hatred for people identified as anti-nationals](https://thewire.in/politics/narcissism-victimhood-and-revenge-the-three-sounds-of-neo-nationalism) – secularists, liberals, beef-eaters, Muslims, immigrants and critics of the government.

This rise in nationalism is also [used as a political tool to distract people from the real issues](https://www.theguardian.com/commentisfree/2021/aug/20/narendra-modi-exploits-nationalism-distract-indians-failing-economy).

### 2. Disdain for human rights

[Here is the report by Human Rights Watch](https://www.hrw.org/world-report/2020/country-chapters/india) to show the disdain for human rights that Indian government has.

### 3. Identification of enemies as a unifying cause

This article covers [this part](https://thewire.in/politics/narcissism-victimhood-and-revenge-the-three-sounds-of-neo-nationalism). Dissent is "[anti-national](https://www.hrw.org/news/2019/12/13/dissent-anti-national-modis-india)" in Modi's India. They also claim [everything is a foreign conspiracy to defame India](https://thewire.in/politics/rafale-politics-bjp-conspiracy-hollande-revelations-congress).

### 4. Supremacy of the military

### 5. Rampant sexism

Modi is [mysogynist and sexist](https://www.cpiml.net/liberation/2015/07/modis-misogyny-not-slip-tongue). Another BJP person [launched sexist remarks at Priyanka Gandhi](https://livewire.thewire.in/gender-and-sexuality/twitter-priyanka-gandhi-sexist-outpouring/).

Even the beti bachao abhiyaan is [deeply patriarichal](https://www.huffpost.com/archive/in/entry/kavita-krishnan-beti-bachao-patriarchy-fearless-freedom_in_5e58d636c5b60102210ef442).

### 6. Controlled mass media

This sign is present everywhere in India. Most of the Indian media is [controlled by the government and parrots their propaganda](https://theprint.in/india/indian-media-controlled-by-a-few-a-risk-to-press-freedom-and-pluralism-study/243285/). [Control of mass media](https://thewire.in/rights/india-no-longer-democracy-electoral-autocracy-v-dem-institute-report-bjp-narendra-modi) was a huge factor in declaring India an electoral autocracy by a Swedish institute. 

Indian government also has a [full team to monitor how media covers Modi and Amit Shah](https://thewire.in/media/narendra-modi-amit-shah-media-watch-punya-prasun-bajpai). The government has their own sophisticated app [to manipulate social media trends](https://thewire.in/tekfog/en/2.html).

### 7. Obsession with national security

India is [highly obsessive with national security](https://science.thewire.in/the-sciences/remote-sensing-policy-earth-observation-data-national-security-deregulation/), even Rohingya Muslim refugees [labelled a threat to national security for India](https://thewire.in/politics/rohingya-modi-government-affidavit) by Modi.

### 8. Religion and government intertwined

Can be seen pretty clearly.

### 9. Corporate power protected

For example, Adani power project was on the brink of bankruptcy – but the [BJP government in Gujarat saved it](https://scroll.in/article/915109/adani-power-project-was-on-the-brink-of-bankruptcy-but-the-bjp-government-in-gujarat-saved-it). Even the Union government was complicit in a decision that will result in consumers in the state paying higher electricity prices.

### 10. Labor power suppressed

For example, [this](https://thewire.in/labour/bharat-bandh-central-trade-union-strike).

### 11. Disdain for intellectual and the arts

This sign is [clearly visible too](https://thewire.in/rights/why-modi-governments-persecution-of-intellectuals-should-worry-us-all).

### 12. Obsession with crime and punishment

For example, [extrajudicial killings in UP by the BJP's Yogi Adityanath](https://thewire.in/rights/extrajudicial-killings-in-up-being-covered-up-even-nhrc-flouting-norms-report).

### 13. Rampant cronyism and corruption

### 14. Fraudlent Elections

[EVM tampering demonstrated by an AAP MLA](https://thewire.in/politics/aap-evm-tampering-bjp).

India is going through unique challenges. Nobody knows the outcome of the actions of the fascist state. This uncertainty contributes to fear. See [this list](/remove-bjp) for how BJP is tearing India apart. There is [hope due to farmers' movement](https://ravidwivedi.in/posts/farmers-giving-hope/) and other [mass protests in response to CAA bill](https://thewire.in/rights/caa-protests-worldwide), which is a huge [discrimination against Muslims](https://www.hrw.org/report/2020/04/09/shoot-traitors/discrimination-against-muslims-under-indias-new-citizenship-policy). There is hope due to India still conducting elections and that means there is a chance to change the government. The presence of these signs fill many Indians with fear.

### Further Reading:

- TMC MP Mahua Moitra [argued that India is already showing early signs of fascism](https://thewire.in/politics/tmc-mp-mahua-moitra-criticise-government-parliamentary-speech).

- The fear of being visited by official investigative agencies and even being arrested can [scare most people who may then decide that discretion is the better part of valour](https://thewire.in/rights/speak-out-get-raided-a-handbook-for-living-in-new-india). 

- Rana Ayyub's post on [India being a fascist state](https://ranaayyub.substack.com/p/india-is-a-fascist-state).

- [India: The New 'Republic of Fear'](https://thewire.in/rights/india-the-new-republic-of-fear).

- [Living in a woman’s body: hate has taken hold in India and I am restless with grief](https://www.theguardian.com/lifeandstyle/2022/feb/14/living-in-a-womans-body-hate-has-taken-hold-in-india-and-i-am-restless-with-grief).

- [Living through India’s pandemic: ‘Oxygen is the new currency’](https://amp.theguardian.com/books/2021/may/07/living-through-indias-pandemic-oxygen-is-the-new-currency).
