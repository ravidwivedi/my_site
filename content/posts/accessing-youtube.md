------
title : "Accessing YouTube without giving away your privacy"
author: "Ravi Dwivedi" 
draft: false
date: 2021-07-12
-------  

YouTube is a platform where people upload and watch videos. I don't think YouTube needs any introduction among the internet users. The thing that is not usually mentioned about YouTube is that [it tracks its users](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy). Further, YouTube's algorithms [distort the truth](https://www.theguardian.com/technology/2018/feb/02/how-youtubes-algorithm-distorts-truth), it [censors works that are fair use under the copyright law](https://www.dailydot.com/unclick/youtube-mashup-remix-copyright-universal/) and doesn't let you download videos in your device, even the ones you purchased. 

An analysis of YouTube's privacy policy is [here](https://tosdr.org/en/service/274).

In short, it is not YouTube, it is [TheirTube](https://www.their.tube/). Knowing someone is always watching you has [inhibiting effects](https://www.socialcooling.com/). I don't want to get tracked or get manipulated by YouTube's algorithms. Fortunately, YouTube is accessible using [free software](https://www.gnu.org/philosophy/free-software-even-more-important.html) in ways which respect your privacy. I will list some ways to watch YouTube videos using workarounds. All the software mentioned here are free software(which means they respect your freedom). Please read [this article](https://www.gnu.org/philosophy/free-software-even-more-important.html) to understand why we must insist on free software.

In Desktop, YouTube videos can be watched using [Invidious](https://github.com/iv-org/invidious). You can visit any of the Invidious instances [here](https://instances.invidious.io/) from any browser and search for the video you would like to watch. Invidious also allows you to visit via Tor which hides your IP Address. You can use [Privacy Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/) plugin in Firefox and Tor to automatically redirect YouTube links to Invidious. This plugin also redirects Twitter, Instagram, Reddit, Google Maps, Google Translate links to their privacy-respecting front-ends. You can also check out a [bunch of other freedom-respecting software](https://github.com/iv-org/invidious#made-with-invidious) that you can use to watch YouTube videos.

[Piped](https://github.com/TeamPiped/Piped#piped) is a free software which acts as a frontend to YouTube. You can watch YouTube videos using Piped in your browser by visiting this [URL](https://piped.kavin.rocks/). Piped does not expose your IP address to YouTube. 

[FreeTube](https://freetubeapp.io/) app can also be used to watch YouTube videos. The app is available for all the GNU/Linux distros, macOS and Windows. FreeTube respects your privacy as well. Please read [privacy section of their FAQ](https://docs.freetubeapp.io/usage/privacy/) for details. 

[Youtube-dl](https://github.com/ytdl-org/youtube-dl) is another software you can use on Desktop to watch videos. It is a command-line program that downloads videos from YouTube in your system. It is my favorite, and I fell in love with it when I realized how capable it is. It is very powerful, and it [supports downloading videos from many other sites](https://ytdl-org.github.io/youtube-dl/supportedsites.html) as well. You can also download audio files by extracting audio from YouTube videos using YouTube-dl. It supports downloading playlists as well. I plan to write a blog in future on using command-line to download using youtube-dl for beginners.

For Android, [Newpipe app](https://newpipe.net/) can be used to watch YouTube videos. Newpipe app shows no ads, [does not track you](https://newpipe.net/#privacy), can run in background, can download audio/video files in your phone. Newpipe can also stream videos which are being live-streamed on YouTube. In addition to YouTube, you can access SoundCloud, media.ccc.de, PeerTube instances ,Bandcamp using NewPipe. 

For iOS, I don't know any workarounds. If you know of any, please [let me know](https://ravidwivedi.in/contact). iOS is, in general, very hostile to user freedom, so the only way to freedom is to avoid iOS.

While these workarounds work well, we cannot rely on Google to let them work. The workarounds do not really solve the problem. The real solution is that the videos are posted on a platform which respects user's freedom(which is a bit more complicated issue than the freedom of software installed in your system). You can upload videos on Peertube or your own website to respect user's freedom and allow them to download the videos as well. I have a [channel](https://videos.fsci.in/video-channels/ravidwivedi_channel) on Peertube on the instance set up by [FSCI](https://fsci.in). You can set up a donation based model to earn money from your videos. Users can send you money directly in your account for your work. You can also earn money by selling merchandise. For this model to be successful, we need more people to join in rejecting YouTube and similar tracking services. 
