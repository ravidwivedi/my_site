---
title: "The Importance of Communities"
draft: false
date: 2021-11-21
---
Wikipedia defines 'Community' as:

> A community is a social unit (a group of living things) with commonality such as norms, religion, values, customs, or identity.

Communities can be of many types and they might exist for different set of goals. In this post, I will share my experience with the [free software commmunity that I am part of](https://fsci.in) and its importance in my life. The topic has been touched in [this blog post](https://fsci.in/blog/self-hosting-and-federation/) by FSCI earlier. The point was that communities can self-host and maintain free software powered services for the benefit of all because every individual self-hosting their services is not possible(for, say, people who do not know how to do) and not sustainable. Therefore, people can collaborate and run services. 

That aspect is very important and that helps me greatly in my use of free software. In this post, I will touch on another aspect of the importance of communities, that is-- the psychological boost that we get by being in a group which shares our goals. I continually say that I use only [free software](/posts/free-sw) for my computing. But how is that possible in the era where proprietary software dominates? First comes my realization that software must respect user's freedom. Then, my willpower to use only free/swatantra software. Then I also rely on the aspect touched earlier in the post about community-run services. I use community-run services(like the ones by FSCI) for my daily use. [Abhas' services](https://mostlyharmless.io) helped me largely too. But one of the most important aspects is: that I am a part of the community which has a goal of promoting freedom-respecting software and full of people who understand the value of privacy. I think that if I was doing it all alone, I would probably would have been demotivated. I don't know if I were able to keep it up. I keep getting demotivated but when I see that I am not alone in this activism, it definitely helps. Then I have friends from Free Software Communities outside of FSCI too. Many of them I know from [Mastodon](https://joinmastodon.org). 

Also, I like how our community is inclusive and welcoming to all. Plus, it is nonhierarchial too. This aspect has made me hate hierarhical and centralized social structures as well(more on this later ;) ). We are also working on [increasing the diversity in the community](https://camp.fsci.in) too. I hope that that people from all backgrounds (and not only privileged) get benefit and freedom that freedom-respecting software provides to the user.

I wanna thank you all for making this possible :) 

Similar read: Arun Mathai also has a [post on why he likes communities](https://arunmathaisk.in/2021/06/14/why-i-love-healthy-communities/). 
