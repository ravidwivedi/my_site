---
title: "List of freedom-respecting software"
date: 2021-07-12
---
<a name="top"></a>

Last Updated: Friday 24 June 2022 09:49:02 PM IST

[Free software](/free-software) means that the users have the freedom to run, copy, distribute, study, change and improve the software. Thus, “free software” is a matter of liberty, not price. I maintain this list to raise awareness about the free software replacements of the commonly used proprietary tools. My intention is to make a curated list of software and not an encyclopedic list. 

Whenever the word 'free' is mentioned, I mean freedom-respecting in the sense we just defined. The term ['open-source' is never used](/posts/fs-and-oss-same).

If you are using Android, then I would like to suggest you to download [F-Droid](f-droid.org) which is a repository of free software and a replacement of Google Play Store. F-Droid can be used to explore free software as well by using its search function.

<a title="FSF Europe, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Fdroid-2.jpg"><img width="512" alt="Fdroid-2" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Fdroid-2.jpg/512px-Fdroid-2.jpg"></a> 

For the software which require network services to connect for their function(like chatting apps), I have only suggested the decentralized and federated (if applicable, for example, search engines can be decentralized but the definition of federation does not apply) ones. 

Mostly, I have copied the description of these software from the Git repository of the project or from the project's official site.

Please help me in improving this list by sending your suggestions/comments about this list to ravi at ravidwivedi dot in . I will first use the software myself and then add it to the list if I think it is okay to suggest.

Index

- [Accounting](#accounting)
- [Bookmarks Sync](#bookmarks-sync)
- [Browser Add-on(or plugins)](#browser-add-on-or-plugins)
- [Chatting](#chatting)
- [Chess Engine](#chess-engine)
- [Collaborative Editing](#collaborative-editing)
- [Dating](#dating)
- [Digital Painting](#digital-painting)
- [Downloading YouTube Videos](#downloading-youtube-videos)
- [Email Clients](#email-clients)
- [Feed Reader](#feed-reader)
- [File hosting software](#file-hosting-software)
- [Front-ends](#front-ends)
- [Hosting software development](#hosting-software-development)
- [Keyboard](#keyboard)
- [LiveStreaming](#livestreaming)
- [Maps](#maps)
- [Music/Video Player](#musicvideo-player)
- [Note-Making](#note-making)
- [Office Suite](#office-suite)
- [Offline Reading](#offline-reading)
- [Operating System](#operating-system) 
- [PDF Reader](#pdf-reader)
- [Password Manager](#password-manager)
- [Playing Chess](#playing-chess)
- [Reddit](#reddit)
- [Remote Storage(incorrectly known as 'cloud')](#remote-storage)
- [Scanner App](#scanner-app)
- [Screen-Recorder](#screen-recorder)
- [Search Engine](#search-engine)
- [Sending donations/ Funding Creators](#sending-donations-funding-creators)
- [Social Network](#social-network)
- [Text Editor](#text-editor)
- [Translation](#translation)
- [VPN](#vpn)
- [Video Editor](#video-editor)
- [Video Games](#video-games)
- [Video Meeting](#video-meeting)
- [Video Platform](#video-platform)
- [Voice Chat/podcasts](#voice-chatpodcasts)
- [Web Browser](#web-browser)
- [Wepage Downloader](#webpage-downloader)

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Accounting

- [GNUKhata](https://www.gnukhata.in/)

- [Akaunting](https://akaunting.com/)

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Bookmarks sync 

- [xBrowserSync](https://www.xbrowsersync.org/) : xBrowserSync is a completely freedom-respecting tool for syncing your bookmarks and browser data between your desktop browsers. xBrowserSync respects your privacy and gives you complete anonymity — no sign up is required and no personal data is ever collected. It is also completely secure; your data is encrypted client-side with military grade encryption, Even if someone intercepted your data, only you can decrypt it.

List of public instances of xBrowserSync is [here](https://www.xbrowsersync.org/).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Browser Add-on (or plugins)

- [uBlock Origin](https://ublockorigin.com/) : uBlock Origin respects your privacy and freedom. The developer of uBlock Origin present arguments on [why you can trust the ublock origin add-on](https://github.com/gorhill/uBlock/wiki/Can-you-trust-uBlock-Origin%3F). It is highly cutomizable as well, so that advanced users can use custom filters. If you want to block an add, click the ublock origin icon, then the ![](/images/ublock-icon.png)  icon, then find the element you want removed, click it, then click Create. This ad won't appear again. You can also add this [host file](https://nixnet.services/hosts.txt) in ublock origin to block malicious domains. Just go to ublock origin -> Fiter Lists -> Import (at the very bottom of the page) -> Enter URL https://nixnet.services/hosts.txt -> Apply Changes. Similarlt, you can also block domains from Pegasus by using this [host file](https://raw.githubusercontent.com/jjjxu/NSO_Pegasus_Blocklist/master/Pegasus-Hosts-Formatted.txt).  

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Chatting 

WhatsApp is a nonfree software and a data-collection tool of Facebook. So, it is obvious I won't recommend WhatsApp. For more details, check my [list of reasons not to use WhatsApp](/whatsapp). In fact, I would advice you to take steps to uninstall or at least minimize your dependence on WhatsApp.

Signal and Telegram are not listed here because they are centralized messengers.

All the apps in this list, except Quicksy, do not ask for phone number or email or any personal details as mandatory to register. Also, all the apps listed here encrypt messages by default, except for IRC and possibly Rocket Chat (I don't know if Rocket Chat encrypts messages by default). 

<figure>
<img src="/images/shredder.png" width="300" height="300">
<figcaption> End-to-end encryption
<br>
Source:<a href="https://cryptpad.fr/customize/images/shredder.png">Cryptpad</a> 
</figcaption>
</figure>

I have written an article on how to choose a privacy-respecting chatting app,you can [read here](/chatting-apps).

- IRC : IRC's full form is Internet Relay Chat. It is a bit complicated and I am still exploring it. An introduction to IRC is [here](https://www.irchelp.org/faq/new2irc.html). 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

#### Federated chat systems:

Federation means that separate instances of a service communicate - the best example of this is email servers, in which it's possible to send mail between difference service providers -- a user from @gmail.com address can send and receive emails from a user from @protonmail.com. 

- [Matrix](https://matrix.org) : Matrix is decentralized, which means there is no central point - anyone can host their own server and have control over their data. 

Further, it allows you to host your own server, giving you control over all of your data. Self hosting also gives you the ability to customize your server to fit your needs including giving you the ability to bridge to other chat networks (such as IRC, XMPP, Discord, Telegram, etc) or to host bots.

All the personal chats are end-to-end encrypted by default.

Some examples of Matrix apps are [Element](https://element.io), [Fluffychat](https://fluffychat.im/), [Nheko](https://github.com/Nheko-Reborn/nheko). A full list of Matrix clients is [here](https://matrix.org/clients/).

- [XMPP](https://xmpp.org/) : XMPP is a federated chatting system. I see XMPP and Matrix as same in terms of respecting user's freedom but different in the features and practical use. XMPP apps usually support OMEMO encryption. But not all the XMPP apps support encrypting messages. All the XMPP apps mentioned [here](https://xmpp.org/software/clients.html) encrypt messages by default.

Examples of XMPP apps are - [Conversations](https://conversations.im), [Quicksy](https://quicksy.im),[Monocles Chat](https://f-droid.org/en/packages/de.monocles.chat/), [Siskin](https://siskin.im), [Gajim](https://gajim.org/), [Dino](https://dino.im/) etc.  

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

#### Peer-to-peer apps

Peer-to-peer apps exchange messages directly without involving a server. This means all the messages are stored only on your and your contact's devices. Your [metadata](https://www.eff.org/deeplinks/2013/06/why-metadata-matters) is never revealed to any third-party.

Users exchanging messages need to be online at the same time. You can run the app in the background mode so that app does not disconnect from the internet.  

- [Briar](https://briarproject.org/) : Briar is a peer-to-peer encrypted messaging app for Android and doesn’t rely on a central server - messages are synchronized directly between the users’ devices. If the internet’s down, Briar can sync via Bluetooth or Wi-Fi, keeping the information flowing in a crisis. If the internet’s up, Briar can sync via the Tor network, protecting users and their relationships from surveillance. Check [here](https://briarproject.org/how-it-works/) for description.

- [GNU Jami](https://jami.net/) : Jami is a distributed peer-to-peer and SIP-compatible [softphone](https://en.wikipedia.org/wiki/Softphone) and instant messenger and it is availble for GNU/Linux, macOS, Windows, Android and iOS.

- [Tox](https://tox.chat/) - A list of apps that can communicate using Tox protocol is [here](https://wiki.tox.chat/clients). Neither the Tox protocol nor the implementation have undergone peer review, and its exact security properties and network behaviour are not well-understood, yet. [Toktok project](https://toktok.ltd/) is actively working on improving that situation.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

#### Other Chatting Apps

- [Rocket.Chat](https://rocket.chat/) : Rocket Chat is a free software and can be self-hosted. It is usually used for project collaboration. Github repo link [here](https://github.com/RocketChat/Rocket.Chat).

- [Silence](silence.im) : Sends SMS in encrypted form if both the users have Silence App. It can send and receive normal SMS messages from users who are not using the Silence app but they will be unencrypted.

- [Delta Chat](delta.chat) : Sends emails as chat if both the users are using Delta Chat. Needs login using email. Sends normal email to other contacts who are not using Delta Chat. 

There are other chatting apps, I haven't tried-- [Session](https://getsession.org/), [Status](https://status.im/), [Threema](https://threema.ch/), 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Chess Engine

A chess engine is a computer program that analyzes chess or chess variant positions, and generates a move or list of moves that it regards as strongest.

- [Stockfish](https://stockfishchess.org/) : Stockfish is a freedom-respecting, powerful UCI chess engine derived from Glaurung 2.1. 

You can read the description [here](https://github.com/official-stockfish/Stockfish#overview).

- [Leela Chess Zero](https://lczero.org) : Leela Chess Zero is a [neural network](https://en.wikipedia.org/wiki/Neural_network)–based chess engine and distributed computing project. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Collaborative Editing

- [Etherpad](https://etherpad.org/) : (Replacement of Google Docs) Etherpad allows you to edit documents collaboratively in real-time. Write articles, press releases, to-do lists, etc. together with your friends, fellow students or colleagues, all working on the same document at the same time. 

Check out the description of Etherpad [here](https://github.com/ether/etherpad-lite#about).

List of [Etherpad instances](https://github.com/ether/etherpad-lite/wiki/Sites-that-run-Etherpad).

- [Cryptpad](https://cryptpad.fr) (Replacement of Google Docs)  : CryptPad is a collaboration suite that is end-to-end-encrypted and free software. It is built to enable collaboration, synchronizing changes to documents in real time. Because all data is encrypted, the service and its administrators have no way of seeing the content being edited and stored.
Check description [here](https://github.com/xwiki-labs/cryptpad#cryptpad).

- [Private Bin](https://github.com/PrivateBin/PrivateBin) : PrivateBin is a minimalist and freedom-respecting pastebin that has zero knowledge of pasted data. Everything is encrypted and decrypted in-browser using 256bit AES in Galois Counter mode.

An example instance is [here](https://bin.nixnet.services).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Dating

If you are using Tinder, then choose a replacement below.

- [Alovoa](https://alovoa.com): Alovoa is a Free Software dating app. I haven't tried it yet, so I don't know much about it. It has app on [F-Droid](https://f-droid.org/en/packages/com.alovoa.alovoa/).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Digital Painting

If you are using Adobe Photoshop, then you can replace it by the following software whichever is suitable.

- [Krita](https://krita.org/en/)  

- Inkscape

- GIMP

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Downloading YouTube videos

- [youTube-dl](https://github.com/ytdl-org/youtube-dl) : youtube-dl is a command-line program to download videos from YouTube.com and it [supports a lot of other sites](https://ytdl-org.github.io/youtube-dl/supportedsites.html). It is very powerful and you can download videos, full playlists, even full channels by using a single command. It can also extract audio and download the audio files from YouTube and other audio sites. Check the official description [here](https://github.com/ytdl-org/youtube-dl#description). 

- [Newpipe](https://newpipe.net/): NewPipe is an alternative front-end of YouTube. NewPipe does not use any Google framework libraries, nor the YouTube API. Websites are only parsed to fetch required info, so this app can be used on devices without Google services installed. Also, you don't need a YouTube account to use NewPipe, which is [copylefted](https://www.gnu.org/licenses/copyleft.en.html) libre software. NewPipe is a privacy-respecting way of watching YouTube videos.

Keep in mind that your IP address is exposed to YouTube when you view using NewPipe. 

- [FreeTube](https://freetubeapp.io/) : FreeTube is a desktop app for watching Youtube videos. See [here](https://docs.freetubeapp.io/usage/privacy/) for details on how FreeTube respects your privacy. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Email Clients

Email clients are the software in which you can login using your email and password and then check emails from the app. One advantage is doing this is that a lot of email clients support gpg encryption while usually webmails do not. All the listed apps support gpg encryption. 

 - [pep](pep.security) - Encrypts emails automatically when both users use pep. Encryption means that only the sender and the recipient of the mail can read its contents(body and subject), the mail provider cannot read the encrypted mails. Although, the mail provider has access to the metadata-- the mail address you contacted with and the time of all the incoming and outgoing emails.Currently, the Thunderbird add-on doesn't work well as it disables OpenPGP in Thunderbird. pep has apps for Android, iOS, and plugins for Thunderbird and Outlook. 

 is highly recommended by me! 

- [Thunderbird](thunderbird.net) : Thunderbird is a freedom-respecting email, newsfeed, chat, and calendaring client, that’s easy to set up and customize. One of the core principles of Thunderbird is the use and promotion of open standards.

It also [supports OpenPGP](https://support.mozilla.org/en-US/kb/openpgp-thunderbird-howto-and-faq) and can be used for encrypting emails with gpg.

- [K9 Mail](https://k9mail.app/) :K-9 Mail is a free software (freedom-respecting) email client for Android. It can encrypt emails with OpenPGP using the freedom-respecting Android app  [OpenKeychain](https://openkeychain.org/). Check [this link](https://github.com/k9mail/k-9#k-9-mail) for more details. 


<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Feed Reader

A feed reader is an app which can fetch blogs using [RSS Feeds](ncase.me/rss). Updated articles from the blogs you follow can be read in the reader itself. It is a privay-repecting replcement to newsletters.

The feed readers are:

- [Thunderbird](https://thunderbird.net) : Thunderbird is an email client as well as a feed reader. It is available for all desktop operating systems. 

- [Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/) : Feeder is a fully free/libre feed reader. It supports all common feed formats, including JSONFeed. It doesn't track you. It doesn't require any setup. It doesn't even need you to create an account! Just setup your feeds, or import them from your old reader via OPML, then get on with syncing and reading. It is available for Android.

- [Tiny Tiny RSS](https://tt-rss.org/) : It is a free software RSS Feed reader which can be self-hosted on your server. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### File hosting software

- [Linx](https://github.com/andreimarcu/linx-server) : Linx is a dead-simple temporary file hosting service. A publicly available server is [here](https://up.nixnet.services/). The max file size is 4 GB and uploads expire after 1 day. Just upload a file and share the link with the person you would like to share the file with.

- [Lufi](https://framagit.org/fiat-tux/hat-softwares/lufi) : Lufi means Let's Upload that FIle. 

It's an end-to-end encrypted file sharing software. It stores files and allows you to download them.
Is that all? No. All the files are encrypted by the browser! It means that your files never leave your computer unencrypted.
The administrator of the Lufi instance you use will not be able to see what is in your file, neither will your network administrator, or your Internet Service Provider. The administrator can only see the file's name, its size and its mimetype (what kind of file it is: video, text, etc.).

You can try Lufi at Disroot's instance [here](https://upload.disroot.org).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Front-ends

These are front-ends for accessing some websites which track you but the front-end are free and respect privacy. Use [Privacy Redirect plugin](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/) in your browser to automatically redirect to these front-ends. 

- [Invidious](https://github.com/iv-org/invidious): Invidious is a privacy-respcting, free software, alternative front-end to YouTube without any ads or tracking. You can visit any of these public [instances](https://github.com/iv-org/documentation/blob/master/Invidious-Instances.md#list-of-public-invidious-instances-sorted-from-oldest-to-newest) in your web browser and watch YouTube videos on Invidious. 

- [Piped](https://github.com/TeamPiped/Piped) : You can watch YouTube videos using Piped by visiting [this URL](https://piped.kavin.rocks/) in any browser. Piped does not expose your IP address to YouTube. Official description [here](https://github.com/TeamPiped/Piped#piped).

- [Newpipe](https://newpipe.net/) : NewPipe is an alternative front-end of YouTube. NewPipe does not use any Google framework libraries, nor the YouTube API. Websites are only parsed to fetch required info, so this app can be used on devices without Google services installed. Also, you don't need a YouTube account to use NewPipe, which is [copylefted](https://www.gnu.org/licenses/copyleft.en.html) libre software. NewPipe is a privacy-respecting way of watching YouTube videos.

- [CloudTube](https://tube.cadence.moe/) : An alternative freedom-respecting frontend to YouTube. Check description [here](https://sr.ht/~cadence/tube/).

- [Nitter](https://github.com/zedeus/nitter/wiki/Instances) : Privacy-respecting alternative front-end for Twitter. There is no tracking, javascript or ads. You can view profiles, tweets, videos, stats, etc. Also supports RSS feeds. As an example, [visit NY Times official Twitter page using Nitter](https://nitter.nixnet.services/nytimes). 

- [Fritter](https://github.com/jonjomckay/fritter) : A privacy-friendly Twitter frontend for Android. This app is available on F-Droid [here](https://f-droid.org/packages/com.jonjomckay.fritter/).

- [Bibliogram](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md) : Bibliogram is a website that takes data from Instagram's public profile views and puts it into a friendlier page that loads faster, gives downloadable images, eliminates ads, generates RSS feeds, and doesn't urge you to sign up. [See an example](https://bibliogram.art/#featured-profiles).

- [Libreddit](https://libredd.it/): An alternative privacy-respecting front-end to Reddit.

- [Teddit](teddit.net): Privacy respecting alternative front-end to Reddit. Check the description [here](https://teddit.net/about). 

- [Simple Web Project](https://simple-web.org/): Simple Web Project has a lot of frontends: [a frontend for Google- and LibreTranslate](https://simple-web.org/projects/simplytranslate.html), [a frontend for SepiaSearch and PeerTube](https://simple-web.org/projects/simpleertube.html), [a frontend for numerous news websites](https://simple-web.org/projects/simplynews.html), [a frontend for xvideos.com](https://simple-web.org/projects/porninvidious.html).  

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Hosting software development

- [Gitea](https://github.com/go-gitea/gitea) : Gitea is a free software for hosting software development version control using Git as well as other collaborative features like bug tracking, wikis and code review. It supports self-hosting as well.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Keyboard

- [Indic Keyboard](https://indickeyboard.org/) : Indic keyboard is a [privacy-respecting](https://indickeyboard.org/faq.html#how-is-it-privacy-aware) keyboard (it does not even require internet access) for Android and [supports typing in Indian languages](https://indickeyboard.org/faq.html#what-all-languages-are-supported). Indic Keyboard is [available for iOS](https://apps.apple.com/us/app/indic-keyboard-13-languages/id1244444150) as well.

Ditch Google and Apple Keyboard and replace them with a free software keyboard. On Android, if you search for keyboard in F-Droid, you will get other options as well.  

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Livestreaming

- [Owncast](http://owncast.online/) (Replacement of Twitch) : Description [here](https://github.com/owncast/owncast#about-the-project). 

- [PeerTube](https://joinpeertube.org/) : PeerTube is a freedom-respecting, decentralized and federated video platform developed as a replacement of other platforms that centralize our data and attention, such as YouTube, Dailymotion or Vimeo. Check [this link](https://github.com/Chocobozzz/PeerTube#--------) for detailed description. [Here is a video](https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3) explaining what PeerTube is and how it works.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Maps

A free software replacement of Google Maps is [OpenStreetMap](https://www.openstreetmap.org) which is built by a community of mappers that contribute and maintain data about roads, trails, cafés, railway stations, and much more, all over the world. There are no restrctions to the use of OpenStreetMaps data: you are free to use it for any purpose as long as you credit OpenStreetMap and its contributors.

Android apps for real-time location using the data of OpenStreetMaps are: 

- [OsmAnd~](https://f-droid.org/en/packages/net.osmand.plus/) and 

- [Organic Maps](https://f-droid.org/en/packages/app.organicmaps/). 

These apps respect your freedom and privacy. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Music/Video Player

- [VLC Media Player](https://www.videolan.org/vlc) : VLC Media Player is a [free software](https://www.gnu.org/philosophy/free-software-even-more-important.html) multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols. Check out features [here](https://www.videolan.org/vlc/features.html).

- [mpv player](https://mpv.io/) : mpv is a free (as in freedom) media player for the command line. It supports a wide variety of media file formats, audio and video codecs, and subtitle types. It has powerful scripting capabilities can make the player do almost anything. There is a large selection of [user scripts on the wiki](https://github.com/mpv-player/mpv/wiki/User-Scripts). Visit their website for a list of features. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Note-Making

These software are replacements of Evernote, Google Keep, or Microsoft OneNote. 

- [Joplin](https://joplinapp.org/) : Joplin is a free/swatantra and fully-featured note-taking and to-do application which can handle a large number of markdown notes organized into notebooks and tags. It offers end-to-end encryption and can sync through Nextcloud, Dropbox, and more. It also offers easy import from Evernote and plain-text notes. 

Check the description [here](https://github.com/laurent22/joplin).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Office Suite

Don't use Microsoft Office as [Microsoft's software is malware](https://www.gnu.org/proprietary/malware-microsoft.html) and mistreats you in various ways.

- [LibreOffice](https://www.libreoffice.org/) (Replacement of Microsoft Office)  : The LibreOffice suite consists of programs for word processing, creating and editing of spreadsheets, slideshows, diagrams and drawings, working with databases, and composing mathematical formulae. As its native file format to save documents for all of its applications, LibreOffice uses the [Open Document Format](opendocumentformat.org) for Office Applications, or [OpenDocument](https://www.libreoffice.org/discover/what-is-opendocument/). By using ODF, you ensure that your data can be transferred between different computers and operating systems, without having to worry about [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in) or license fees.

Available for: GNU/Linux, Android, iOS, macOS, Windows.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Offline Reading

- [Kiwix](https://kiwix.org/): Kiwix is a Free Software which allows you read Wikipedia, Project Gutenberg, and other similar websites offline. You can download, say, WikiVoyage and all the wiki travel guides are available offline for you. Available for Windows, GNU/Linux, macOS, Raspberry Pi, Android, iOS.

	Quoting the project itself, "Kiwix can compress any website (even large ones like Wikipedia, the Gutenberg project or Stack Exchange) into unique content packages (zim files).

	We can copy text, images, videos, just like the original thing – but highly compressed so that they are easy to share and distribute, for instance on a flash drive or microSD card, or broadcast on inexpensive hotspots.

	The Kiwix reader runs on almost any device (mobile phones, computers, etc.). For the end user it feels pretty much like a regular browser as the experience is almost identical to browsing the source website(s). Except that there is no internet."

	As North Koreans do not have access to free internet, The North Korea Strategy Center [disseminates](https://www.kiwix.org/en/about/stories/north-korea/) these offline resources using Kiwix in North Korea to spread knowledge.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

### Operating System

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

#### For Desktop

Don't use [Microsoft Windows](https://thehackernews.com/2016/02/microsoft-windows10-privacy.html) or [macOS](https://sneak.berlin/20201112/your-computer-isnt-yours/). They are nonfree software and are known to spy on their users and mistreat them in various ways. 

- [GNU/Linux distros](https://www.gnu.org/distros/free-distros.en.html): The distro I suggest is [Debian GNU/Linux](https://debian.org). I myself use PureOS or Debian.  

Usually, people refer to these operating systems as 'Linux', but this is incorrect as Linux is only the kernel of the operating system, I refer to these distros as GNU/Linux. For a detailed argument, check [here](https://www.gnu.org/gnu/why-gnu-linux.html).

If you would like to explore GNU/Linux distros, then check out [Distro Watch](https://distrowatch.com/) or [LibreHunt](https://librehunt.org/) website.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>

#### For mobile phones

Don't use ioS because [it is a malware](https://www.gnu.org/proprietary/malware-apple.html). You can buy an Android phone and install a free software custom ROM in it without Google apps. Another way is to buy a [Pinephone](https://pine64.org) or [Librem 5](https://puri.sm/products/librem-5/). 

- [Replicant OS](https://replicant.us/) : See [List of Supported devices](https://redmine.replicant.us/projects/replicant/wiki/DeviceStatus).

- [Postmarket OS](https://postmarketos.org/) : See [List of supported devices](https://wiki.postmarketos.org/wiki/Devices). 

- [Lineage OS](https://www.lineageos.org/) : [List of supported devices](https://wiki.lineageos.org/devices/).

- [Ubuntu Touch](https://ubuntu-touch.io/) : [List of supported devices](https://devices.ubuntu-touch.io/).


<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### PDF Reader

Source: [pdfreaders.org](https://pdfreaders.org/). For more details, check the [article by Free Software Foundation Europe](https://fsfe.org/activities/pdfreaders/pdfreaders.en.html) on Free Software PDF Readers.

- [SumatraPDF](https://www.sumatrapdfreader.org/free-pdf-reader.html) : SumatraPDF is a free PDF, eBook, XPS, DjVu, CHM, Comic Book reader for Windows. Sumatra PDF is powerful, small, portable and starts up very fast.

- [Skim](https://skim-app.sourceforge.io/) : PDF Reader for macOS.

- [Evince](http://projects.gnome.org/evince/) : PDF Reader for GNU/Linux. 

- [MuPDF](http://mupdf.com/) : PDF Reader for Android, Windows and GNU/Linux.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Password Manager

Password Manager is a program to generate, store and manage passwords. In below mentioned list, Lesspass and Spectre do not require storing passwords anywhere.

I am impressed by the password manager named LessPass. Please do not use proprietary password manager programs, for example, 1Password, LastPass, Roboform, iCloud Keychain. 

- [LessPass](https://www.lesspass.com/) : LessPass is a password manager which takes site address, login name and a master password (which you choose) as inputs and generates a password. The user doesn't need to download any app. They only need to visit lesspass.com and enter these three things. These passwords are not stored anywhere. The same inputs generate the same password everytime and therefore, no storage is required. Lesspass has password generator [in its website](https://www.lesspass.com/), [Android app](https://f-droid.org/en/packages/com.lesspass.android/) and has a command-line program too.

- [Spectre](https://spectre.app/) : Spectre is similar to Lesspass. It requires name, site name and master password to generate a password. Entering the same inputs would generate the same password and therefore, no storage of passwords is required.  

- [pass](https://www.passwordstore.org/) : A command line program for password manager which can store and generate passwords. With pass, each password lives inside of a gpg encrypted file whose filename is the title of the website or resource that requires the password. In addition to storing the password in your system, pass can also store your passwords in a git repository with each file encrypted with your gpg keys, which means only you can decrypt the password files. It has a [Firefox plugin](https://github.com/jvenant/passff#readme) which can be used to enter passwords for websites. The passwords can also be synced with your Android phone using the [Password Store app](https://f-droid.org/en/packages/dev.msfjarvis.aps/), which you can decrypt using an app like [OpenKeyChain](https://www.openkeychain.org/). It has a bunch of other plugins and apps, like, A Chrome plugin, an emacs package, an iOS app, which you can check by visiting [their website](https://passwordstore.org) and scrolling down to the section 'Compatible Clients'. 

Warning: pass is for people who know some basic gpg and git command line. It is not for beginners. 

- [KeePassXC](https://keepassxc.org/) : KeePassXC is a freedom-respecting password manager that stores password locally in your computer and all the passwords are in a single file that makes it easy to backup.  It saves many different types of information, such as usernames, passwords, URLs, attachments, and notes in an offline, encrypted file that can be stored in any location. If you want Bitwarden-like sync, you can put the DB in Nexcloud/Syncthing etc.
Description [here](https://github.com/keepassxreboot/keepassxc#-keepassxc).

The downside is that it creates a new file for every password and therefore harder to backup and there is no Android app.

- [Bitwarden](https://bitwarden.com/) : Very convenient password manager. The passwords are synced between all of your devices. If you don't want to use the Bitwarden server to store your passwords, you can easily host your own Bitwarden server.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Playing Chess

- [Lichess](lichess.org) : lichess.org is a free/libre chess server powered by volunteers and donations. Anyone can play online chess anonymously, although players may register an account on the site to play rated games. Lichess is ad-free and all the features are available for gratis (free of cost), as the site is funded by donations. Features include chess puzzles, computer analysis, tournaments and chess variants. 

- [GNU Chess](https://www.gnu.org/software/chess/) : GNU Chess is a free software chess engine which plays a full game of chess against a human being or other computer program. The goal of GNU Chess is to serve as a basis for research.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Reddit

To view Reddit, you can use the following Free Software:

- [Slide](https://f-droid.org/en/packages/me.ccrama.redditslide/): Slide is an independent app which can be used to view Reddit and log in to Reddit to post something as well.

- [RedReader](https://f-droid.org/en/packages/org.quantumbadger.redreader/): RedReader is a Reddit client for Android. You can view Reddit posts and login to Reddit and post there too.

	If you are interested in only viewing Reddit posts, then check out the [front ends](#front-ends) section of this page.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Remote Storage

Replace proprietary software like Google Drive/Dropbox/One Drive with any of these:

- [Nextcloud](https://nextcloud.com/): Nextcloud is free software that can be installed on a server and can be used for remote storage. Nextcloud can be used for personal storage, for home or office needs, for educational institutes and even for servers with millions of users. It can be self-hosted and it is federated too. Federated means Nextcloud users on different service providers can share files. 

	Nextcloud has many functionalities and users can add a desired missing functionality as well if they would like to. Some of the functionalities of Nextcloud:

	- Calendars
	- Contacts
	- Streaming media
	- Gallery
	- Notes
	- RSS feed reader
	- Document viewer tools from within Nextcloud
	- Viewer for maps
	- Managing of cooking recipes

	Your notes, contacts, calendars can be easily synced across devices using Nextcloud via Nextcloud apps which are available for almost all the operating systems. Otherwise it can be accessed using any regular browser like Firefox.  

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Proxy

Check [Wikipedia article on Proxy](https://en.wikipedia.org/wiki/Proxy_server) for description.

- [Orbot](https://guardianproject.info/apps/orbot/) : Orbot is a free software proxy [app for Android] that empowers other apps to use the internet more securely. Orbot uses Tor to encrypt your Internet traffic and then hides it by bouncing through a series of computers around the world.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Scanner App

A scanner app is usually used in mobile phones to take photos of documents to convert it into an image or a PDF.  

- [Open Note Scanner](https://f-droid.org/en/packages/com.todobom.opennotescanner/) : With Open Note Scanner you can capture documents, handwritten notes, arts, shopping receipts, etcetera. It will automatically adjust the image aspect, contrast and save it. It will allow browsing, view and share the scanned documents.

- [Open Scan](https://github.com/Ethereal-Developers-Inc/OpenScan): On F-Droid, it is [available](https://apt.izzysoft.de/fdroid/index/apk/com.ethereal.openscan) on [IzzyOnDroid's repository](https://apt.izzysoft.de/fdroid/index.php). You must have F-Droid installed to follow these steps. First add the IzzyOnDroid F-Droid Repository by visiting [this URL](https://apt.izzysoft.de/fdroid/repo?fingerprint=3BF0D6ABFEAE2F401707B6D966BE743BF0EEE49C2561B9BA39073711F628937A) in a browser in your Android mobile phone. Then go to the F-Droid app and swipe down to refresh the repositories. Once the repositories are up-to-date, search for openscan and install it.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Screen-Recorder

- [Open Broadcaster Software(OBS)](https://obsproject.com/) : [Free software](https://www.gnu.org/philosophy/free-software-even-more-important.html) for video recording and live streaming for desktop. Available for: GNU/Linux, macOS, Windows.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Search Engine

Usually DuckDuckGo is referred to as privacy-respecting search engine. I do not prefer DuckDuckGo because it is proprietary software and it is centralized. 

Therefore, I suggest Searx because it is free software and decentralized.

- [Searx](https://github.com/searx/searx) : Privacy-respecting, hackable metasearch engine. 

You can self-host Searx or use a [public instance](https://searx.space/). You can try [Disroot Searx](https://search.disroot.org) or [Nixnet's](https://searx.nixnet.services/) as an example instance.

- [YaCy](http://yacy.net/) : The YaCy search engine software provides results from a network of independent peers, instead of a central server. It is a distributed network where no single entity decides what to list or order it appears in.

User privacy is central to YaCy, and it runs on each user's computer, where search terms are hashed before they being sent to the network. Everyone can create their individual search indexes and rankings, and a truly customized search portal.

Each YaCy user is either part of a large search network (search indexes can be exchanged with other installation over a built-in peer-to-peer network protocol) or the user runs YaCy to produce a personal search portal that is either public or private.

YaCy search portals can also be placed in an intranet environment, making it a replacement for commercial enterprise search solutions. A network scanner makes it easy to discover all available HTTP, FTP and SMB servers.

[Git repo link](https://github.com/yacy/yacy_search_server).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Sending donations/ Funding Creators

Freedom-respecting replacements of Patreon. 

- [Liberapay](liberapay.com/) : Liberapay is a recurrent donations platform. It helps you fund the creators and projects you appreciate.

Note: This webapp is not self-hostable.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Social Network

 <figure>
  <img src="/images/network-models.jpg" width="600" height="300">
  <figcaption>From left to right: Centralized, Federated, Distributed
<br>
Source: <a href="https://docs.joinmastodon.org/">Mastodon Docs</a>
</figcaption>
</figure> 

Twitter and [Facebook](https://stallman.org/facebook.html) are centralized and nonfree software social media and they don't respect your freedom. I think the users should leave Facebook entirely and not use it at all. 

The ethical ones are:

- [Mastodon](https://github.com/mastodon/mastodon#) (Replacement of Twitter)  : Mastodon is a free social network server based on ActivityPub where users can follow friends and discover new ones. On Mastodon, users can publish anything they want: links, pictures, text, video. All Mastodon servers are interoperable as a [federated](/glossary/#federated-services) network (users on one server can seamlessly communicate with users from another one, including non-Mastodon software that implements ActivityPub)!

	A good introduction to Mastodon is [here](https://docs.joinmastodon.org/) and [here](https://mstdn.social/@feditips/107537092666623537). A good video introduction is [here](https://yewtu.be/watch?v=S57uhCQBEk0) and [here](https://tilvids.com/w/hRzMpJwSkqEK3ZTNPbSuqu).

	Refer to [this guide](https://instances.social/) to choose suitable Mastodon service for you.

	Check out [Recommended Mastodon apps](https://mstdn.social/@feditips/106353336303297821).

	Don't forget to [follow me](https://masto.sahilister.in/@ravi) on Mastodon.

- [Pleroma](https://git.pleroma.social/pleroma/pleroma/#about) (Replacement of Twitter) : Pleroma is a microblogging server software that can federate (= exchange messages with) other servers that support ActivityPub. What that means is that you can host a server for yourself or your friends and stay in control of your online identity, but still exchange messages with people on larger servers. Pleroma will federate with all servers that implement ActivityPub, like Friendica, GNU Social, Hubzilla, Mastodon, Misskey, Peertube, and Pixelfed.

- [PixelFed](https://pixelfed.org/) (Replacement of Instagram) : A free and ethical photo sharing platform, powered by ActivityPub federation. List of instances [here](https://fediverse.party/en/pixelfed/).

- [Friendica](https://friendi.ca/) (Replacement of Facebook) : Friendica is a privacy-respecting replacement of Facebook. With Friendica,you can connect to anyone on Friendica, Mastodon, Diaspora, GnuSocial, Pleroma, or Hubzilla, regardless where each user profile is hosted. Description [here](https://github.com/friendica/friendica#friendica-social-communications-server).

- [Diaspora](https://diasporafoundation.org) : Diaspora is a privacy-aware, distributed, free social network.

	Check out the diaspora pods [poddery.com](https://poddery.com/) and [diasp.in](https://diasp.in/).

[Tips](https://wiki.diasporafoundation.org/Choosing_a_pod) for finding a pod.

[List of diaspora servers](https://diaspora.podupti.me/) to sign up.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Text Editor

- [Vim](https://www.vim.org/) : A command line text editor. Vim has keybindings which are easy to learn and get used to even for non-techncial people. It can save you hours of work because it can do things simply which are tedious in other text editors. 

(This file is written in Vim Text Editor)

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Translation

- [Simply Translate](https://simple-web.org/projects/simplytranslate.html) : A translation front-end for  Google, DeepL, ICIBA, LibreTranslate. 

- [Translate Shell](https://www.soimort.org/translate-shell/) : A command-line program translator. [Read FAQ](https://github.com/soimort/translate-shell/wiki/FAQ) which describes some limitations of this tool. 

- [Libre Translate](https://github.com/LibreTranslate/LibreTranslate) : Freedom-respecting Machine Translation API, entirely self-hosted. Unlike other APIs, it doesn't rely on proprietary providers such as Google or Azure to perform translations. [Try it online!](https://libretranslate.com/)

- [Lingva Translate](https://github.com/TheDavidDelta/lingva-translate) : Free Software for translation. Inspired by projects like NewPipe, Nitter, Invidious or Bibliogram, Lingva scrapes through GTranslate and retrieves the translation without using any Google-related service, preventing them from tracking. It allows self-hosting as well. Some instances are listed [here](https://github.com/TheDavidDelta/lingva-translate#instances).

- [Varnam](https://www.varnamproject.com/editor) : Varnam is a freedom-respecting cross platform transliterator for Indian languages. [Github link](https://github.com/varnamproject).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### VPN

Quoting [EFF](https://ssd.eff.org/en/module/choosing-vpn-thats-right-you) below:

VPN stands for “Virtual Private Network.” When you connect to a VPN, all data that you send (such as the requests to servers when browsing the web) appears to originate from the VPN itself, rather than your own Internet Service Provider. This masks your IP address, which can be an important tool for protecting your privacy, since your IP address provides an indication of your general location and can therefore be used to identify you.

In practice, VPNs can:

1.  Protect your Internet activity from prying eyes, especially if you’re connected to an unsecure Wi-Fi network in a café, airport, library, or somewhere else.
2. Circumvent Internet censorship on a network that blocks certain sites or services. For example, when you are working from a school’s Internet connection or in a country that blocks content. Note: It’s important to keep up to date on security news for specific countries’ policies on VPNs.
3. Connect you to the corporate intranet at your office while you’re traveling abroad, at home, or any other time you are out of the office.

Using a VPN will not keep your browsing habits anonymous, nor will it add additional security to non-secure (HTTP) traffic.

If you are looking for anonymity, you should use the [Tor Browser](https://www.torproject.org/) instead of a VPN.

Choosing a VPN usually requires us to trust the VPN service. For a basic understanding of how VPN works, check the [Riseup's article](https://riseup.net/en/vpn/how-vpn-works). Also, I suggest you to read the [limitations of using a VPN](https://riseup.net/en/vpn/limitations).

A VPN app must be [free software](https://www.gnu.org/philosophy/free-software-even-more-important.html). Nord VPN, Express VPN are examples of nonfree software and they should not be trusted.

[Privacy Tools](https://privacytools.io/classic) has provided a good [VPN recommendations]() that you can check out and they have a more [objective and detailed criteria](https://privacytools.io/providers/vpn/#criteria) than mine.

Personally, I don't like to login my account to use VPN, so [ProtonVPN](https://protonvpn.com/) is not in my list.

Below are the VPNs that I trust:

- [Riseup VPN](https://riseup.net/en/vpn) : Riseup VPN is a community-maintained VPN run by volunteers. Riseup also has a [good track record of defending and caring for user's privacy](https://en.wikipedia.org/wiki/Riseup#Controversies). Riseup is based in USA which might raise concerns. I personally trust Riseup, so I have no problem in recommending Riseup VPN. This VPN is available for all the desktop OS, Android and iOS. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Video Editor

- [Kdenlive](https://kdenlive.org/) : Kdenlive is a fully-featured video editor which is free(freedom-respecting) software. Visit the [features section of their website](https://kdenlive.org/en/features/) to see what features it has.

- [Blender](https://blender.org) : Blender is a video editor but that is only a part of the job it can do. It is much more than a video editor.  

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Video Games

A list of freedom-respecting games can be found [at this link](https://linux.softpedia.com/get/GAMES-ENTERTAINMENT/). 

- [SuperTuxKart](https://supertuxkart.net) : SuperTuxKart is a free kart racing game. It focuses on fun and not on realistic kart physics. Check the [description here](https://github.com/supertuxkart/stk-code).

- [Pixel Wheels](https://f-droid.org/en/packages/com.agateau.tinywheels.android/) : Pixel Wheels is a top-down retro racing game: race for the first place on various tracks. Pick up bonuses to boost your position or slow down competitors! 

Check Pixel Wheels [official website](https://agateau.com/projects/pixelwheels/).

Source code is [here](https://github.com/agateau/pixelwheels).

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Video Meeting

[Don't use proprietary videoconferencing services](/videoconferencing) like Zoom, Google Meet, Skype and Microsoft Teams.

- [Jitsi Meet](https://github.com/jitsi/jitsi-meet) : A list of Jitsi instances is [here](https://jitsi.github.io/handbook/docs/community/community-instances). It works like this: Go to a Jitsi Meet instance, for example, [meet.fsci.in](https://meet.fsci.in) and type in the 'Room Name' and click on 'Start Meeting', share the link with the people whom you would like to invite, and they just need to visit the link from a browser to join the meeting. You can also download Jitsi Android or iOS app to create/join the meeting. 

- [BigBluebutton](https://github.com/bigbluebutton/bigbluebutton#bigbluebutton) : BigBlueButton is a free software web conferencing system.

BigBlueButton supports real-time sharing of audio, video, slides (with whiteboard controls), chat, and the screen. Instructors can engage remote students with polling, emojis, multi-user whiteboard, and breakout rooms.

Presenters can record and playback content for later sharing with others.

BigBluebutton is designed for online learning (though it can be used for many other applications). The educational use cases for BigBlueButton are

   -  Online tutoring (one-to-one)
   -  Flipped classrooms (recording content ahead of your session)
   -  Group collaboration (many-to-many)
   -  Online classes (one-to-many)


List of BigBlueButton instances is [here](https://wiki.chatons.org/doku.php/la_visio-conference_avec_big_blue_button). 

Example: Choose a BigBluebutton instance, like [meet.nixnet.services](https://meet.nixnet.services). Sign up for an account. Once you are logged in, you can create rooms and share the links with attendees. Joining a BigBlueButton meeting is as easy as clicking a link and loading it in your browser (Recent versions of Chromium and Firefox) either on your laptop/desktop or on your mobile.

Chiguru offers paid plans for [Online classrooms](https://classmeet.chiguru.tech/) sevice based on BigBlueButton. 

- [MiroTalk](https://mirotalk.herokuapp.com/): Mirotalk is a peer-to-peer video calling software with tons of [features](https://github.com/miroslavpejic85/mirotalk#features). With Mirotalk, you can record meetings, share files, write on whiteboard with full privacy.  

- [GNU Jami](jami.net): I myself haven't tried it for video calling.

- [Linphone](https://linphone.org/) : Linphone (contraction of Linux phone) is a free voice over IP softphone, SIP client and service. It may be used for audio and video direct calls and calls through any VoIP softswitch or IP-PBX. Linphone also provides the possibility to exchange instant messages.


<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Video platform

- [PeerTube](https://joinpeertube.org/) (Replacement of YouTube) : PeerTube is a freedom-respecting, decentralized and federated video platform developed as an alternative to other platforms that centralize our data and attention, such as YouTube, Dailymotion or Vimeo. Check [this link](https://github.com/Chocobozzz/PeerTube#--------) for detailed description. [Here is a video](https://framatube.org/videos/watch/217eefeb-883d-45be-b7fc-a788ad8507d3) explaining what PeerTube is and how it works.

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Voice chat/podcasts

- [Mumble](https://www.mumble.info/) (Replacement of proprietary software TeamSpeak) : Mumble is a voice over IP (VoIP) application primarily designed for use by gamers, and is also commonly used for recording podcasts and collaborating on software development as well as in a lot of other situations.

Nixnet has a [Mumble service](https://docs.nixnet.services/Mumble), open to all. Disroot also [has one](https://mumble.disroot.org/). 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Web Browser

Read EFF's coverage on [Browser Fingerprinting](https://coveryourtracks.eff.org/), a method websites use to track you.

Check out [this post](https://unixsheikh.com/articles/choose-your-browser-carefully.html) for an analysis of common web browsers for privacy.

- [Firefox browser](https://www.mozilla.org/en-US/firefox/new/) : Firefox is a free software browser which is highly cutomizable. Check this guide to [customize your Firefox](https://12bytes.org/articles/tech/firefox/the-firefox-privacy-guide-for-dummies/) to make it respect your privacy better than the default settings of Firefox.  

- [Tor Browser](https://www.torproject.org/) : I recommend using Tor Browser for online anonymity. Specifically Tor hides the source and destination of your Internet traffic, this prevents anyone from knowing both who you are and what you are looking at (though they may know one or the other). Tor also hides the destination of your traffic, which can circumvent some forms of censorship. So, when you visit a website, they don't know your IP Address. 

Tor Browser also [protects you from browser fingerprinting techniques](https://blog.torproject.org/browser-fingerprinting-introduction-and-challenges-ahead) that are used to track you.

Note: Tor by itself is not all you need to maintain anonymity. There are several major pitfalls to watch out for (see [Want Tor to really work?](https://en.calameo.com/read/005242387ef11d44d4ae7))

- [Epiphany (or GNOME Web) Browser](https://gitlab.gnome.org/GNOME/epiphany.git): It is a very lightweight browser with focus on user privacy.

- [ungoogled-chromium](https://ungoogled-software.github.io/): It is a Chromium-based web browser without the Google components and Google tracking. 
- [Librewolf](https://librewolf.net/): Librewolf is a privacy-respecting fork of Firefox.

- [Lynx Browser](http://lynx.browser.org/) : A command line browser.

- [Qutebrowser](https://qutebrowser.org): If you would like to use a minimal browser with vim-like keybindings, then Qutebrowser is for you. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>


### Webpage/File Downloader

- [GNU Wget](https://www.gnu.org/software/wget/) : GNU Wget is a free software package for retrieving files using HTTP, HTTPS, FTP and FTPS, the most widely used Internet protocols. It is a non-interactive commandline tool, so it may easily be called from scripts, cron jobs, terminals without X-Windows support, etc. 

Just run the command `wget URL` in the terminal and it will fetch the webpage/file at the URL and download it in your computer. 

<p style="text-color:Red;"><a href="#top"><button class="button">Back to top</button></p>
