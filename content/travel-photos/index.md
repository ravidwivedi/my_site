---
title: "Pictures Collection"
date: 2021-10-11
draft: false
---

All the photos attached below are taken by me. I have released all the photos under [Creative Commons Attribution-Share Alike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/deed.en). Under this license, you are free to use, share(even commercial sharing is allowed), modify for any purpose but you are required to give attribution to me. All the derivative works must also be released under the same license. This requirement gives freedom to all the users of the photo.

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Howrah_Railway_Station.jpg"><img width="512" alt="Howrah Railway Station" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Howrah_Railway_Station.jpg/512px-Howrah_Railway_Station.jpg"></a>
<figcaption>
<a href="https://en.wikipedia.org/wiki/Howrah_railway_station">Howrah railway station</a>.<a href="https://www.openstreetmap.org/node/3321321335">Location on OpenStreetMap</a>.<br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Belur_Math_during_sunset,_Belur,_Howrah,_India.jpg"><img width="512" alt="Belur Math during sunset, Belur, Howrah, India" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Belur_Math_during_sunset%2C_Belur%2C_Howrah%2C_India.jpg/512px-Belur_Math_during_sunset%2C_Belur%2C_Howrah%2C_India.jpg"></a>
<figcaption>
<a href="https://en.wikipedia.org/wiki/Belur_Math">Belur Math</a> during sunset, Howrah, India. <br>License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Beautiful_Victoria_Memorial,_Kolkata,_India_05.jpg"><img width="512" alt="Beautiful Victoria Memorial, Kolkata, India 05" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Beautiful_Victoria_Memorial%2C_Kolkata%2C_India_05.jpg/512px-Beautiful_Victoria_Memorial%2C_Kolkata%2C_India_05.jpg"></a>
<figcaption>
Victoria Memorial, Kolkata, India.<a href="https://www.openstreetmap.org/?mlat=22.544889&mlon=88.342572&zoom=11#map=11/22.5449/88.3426">Location on OpenStreetMap</a>.<br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Back_side_of_beautiful_Victoria_Memorial,_Kolkata,_India.jpg"><img width="512" alt="Back side of beautiful Victoria Memorial, Kolkata, India" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Back_side_of_beautiful_Victoria_Memorial%2C_Kolkata%2C_India.jpg/512px-Back_side_of_beautiful_Victoria_Memorial%2C_Kolkata%2C_India.jpg"></a>
<figcaption>
Back side of the Victoria Memorial, Kolkata, India.<a href="https://www.openstreetmap.org/?mlat=22.544889&mlon=88.342572&zoom=11#map=11/22.5449/88.3426">Location on OpenStreetMap</a>.<br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Howrah_Railway_Station_in_the_morning.jpg"><img width="512" alt="Howrah Railway Station in the morning" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Howrah_Railway_Station_in_the_morning.jpg/512px-Howrah_Railway_Station_in_the_morning.jpg"></a>
<figcaption>
<a href="https://en.wikipedia.org/wiki/Howrah">Howrah railway station</a>.<a href="https://www.openstreetmap.org/node/3321321335">Location on OpenStreetMap</a>.<br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi" href="https://commons.wikimedia.org/wiki/File:Shot-of-kerala-india-from-a-moving-train.jpg"><img width="512" alt="Shot-of-kerala-india-from-a-moving-train" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Shot-of-kerala-india-from-a-moving-train.jpg/512px-Shot-of-kerala-india-from-a-moving-train.jpg"></a>
<figcaption>
This one was shot from a moving train, on the way to Coimbatore from Ernakulam.<br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi" href="https://commons.wikimedia.org/wiki/File:Inside_sarnath_varanasi_india.jpg"><img width="256" alt="Inside sarnath varanasi india" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Inside_sarnath_varanasi_india.jpg/256px-Inside_sarnath_varanasi_india.jpg"></a>
<figcaption>
A huge bell inside Sarnath, Varanasi, Uttar Pradesh, India. <a href="https://www.openstreetmap.org/?mlat=25.38082&mlon=83.02453&zoom=15#map=15/25.3808/83.0245">Location on OpenStreetMap</a>.<br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Libreravi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Bonhooghly-kolkata-india-01.jpg"><img width="512" alt="Bonhooghly-kolkata-india-01" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Bonhooghly-kolkata-india-01.jpg/512px-Bonhooghly-kolkata-india-01.jpg"></a>
<figcaption>
Sunset in Bonhooghly, Kolkata, West Bengal, India. <a href="https://www.openstreetmap.org/?mlat=22.646318&mlon=88.38337&zoom=15#map=15/22.6463/88.3834">Location</a>. <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Hooghly_River,_Bandel,_West_Bengal,_India.jpg"><img width="512" alt="Hooghly River, Bandel, West Bengal, India" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Hooghly_River%2C_Bandel%2C_West_Bengal%2C_India.jpg/512px-Hooghly_River%2C_Bandel%2C_West_Bengal%2C_India.jpg"></a>
<figcaption>
Hooghly river seen from a mosque in Bandel, West Bengal. <a href="https://www.openstreetmap.org/?mlat=22.9093&mlon=88.3999&zoom=15#map=15/22.9093/88.3999">Location on OpenStreetMap</a>.<br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figvaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Mysore-palace.jpg"><img width="512" alt="Mysore-palace" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Mysore-palace.jpg/512px-Mysore-palace.jpg"></a>
<figcaption>
<a href="https://en.wikipedia.org/wiki/Mysore_Palace"> Mysore Palace</a>. <a href="https://www.openstreetmap.org/way/656323590">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:SisGanj_Sahib_Gurudwara,_Delhi.jpg"><img width="512" alt="SisGanj Sahib Gurudwara, Delhi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/SisGanj_Sahib_Gurudwara%2C_Delhi.jpg/512px-SisGanj_Sahib_Gurudwara%2C_Delhi.jpg"></a>
<figcaption> Gurudwara Sis Ganj Sahib, Old Delhi, India <a href="https://www.openstreetmap.org/?mlat=28.6558&mlon=77.2325&zoom=15#map=15/28.6558/77.2325">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Old_delhi-railway_station.jpg"><img width="512" alt="Old delhi-railway station" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Old_delhi-railway_station.jpg/512px-Old_delhi-railway_station.jpg"></a>
<figcaption> Old Delhi Railway Station, Delhi, India. <a href="https://www.openstreetmap.org/node/913095905">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Kathgodam_Railway_Station_1.jpg"><img width="512" alt="Kathgodam Railway Station 1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Kathgodam_Railway_Station_1.jpg/512px-Kathgodam_Railway_Station_1.jpg"></a>
<figcaption> Kathgodam Railway Station, Uttarakhand, India. <a href="https://www.openstreetmap.org/?mlat=29.266639&mlon=79.546697&zoom=15#map=15/29.2666/79.5467">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. 
</figcaption>
</figure>  
<br>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Kathgodam_Railway_Station_08-Oct-2021.jpg"><img width="512" alt="Kathgodam Railway Station 08-Oct-2021" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Kathgodam_Railway_Station_08-Oct-2021.jpg/512px-Kathgodam_Railway_Station_08-Oct-2021.jpg"></a>
<figcaption> Kathgodam Railway Station, Uttarakhand, India. <a href="https://www.openstreetmap.org/?mlat=29.266639&mlon=79.546697&zoom=15#map=15/29.2666/79.5467">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. </figcaption>
</figure>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Ranikhet_photo_shot_by_Ravi.jpg"><img width="512" alt="Ranikhet photo shot by Ravi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Ranikhet_photo_shot_by_Ravi.jpg/512px-Ranikhet_photo_shot_by_Ravi.jpg"></a>
<figcaption>
Ranikhet area near Golf Course, Ranikhet, Uttarakhand, India. <a href="https://www.openstreetmap.org/?mlat=29.65741&mlon=79.45222&zoom=15&layers=M">(Approximate) Location on OpenStreetMap</a>   <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Bhimtal-Sattal_Road.jpg"><img width="512" alt="Bhimtal-Sattal Road" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Bhimtal-Sattal_Road.jpg/512px-Bhimtal-Sattal_Road.jpg"></a>
<figcaption>
Bhimtal-Sattal Road, Uttarakhand, India. <a href="https://www.openstreetmap.org/?mlat=29.36563&mlon=79.53421&zoom=15#layers=M">(Approximate) Location on OpenStreetMap</a> <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

</figcaption>
</figure>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Lucknow_Charbagh_Railway_Station_3.jpg"><img width="512" alt="Lucknow Charbagh Railway Station 3" src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Lucknow_Charbagh_Railway_Station_3.jpg/512px-Lucknow_Charbagh_Railway_Station_3.jpg"></a>
<figcaption>
Lucknow Charbagh Railway Station, Uttar Pradesh, India. <a href="https://www.openstreetmap.org/node/564718427#map=19/26.83116/80.92436">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Charbagh_Railway.jpg"><img width="512" alt="Charbagh Railway" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Charbagh_Railway.jpg/512px-Charbagh_Railway.jpg"></a>
<figcaption>
Lucknow Charbagh Railway Station, Uttar Pradesh, India. <a href="https://www.openstreetmap.org/node/564718427#map=19/26.83116/80.92436">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>
<br>

<figure>
<a title="Ravi Dwivedi, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:New_Delhi_Railway_Station_Ajmeri_Gate_side,_India._01.jpg"><img width="512" alt="New Delhi Railway Station Ajmeri Gate side, India. 01" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/40/New_Delhi_Railway_Station_Ajmeri_Gate_side%2C_India._01.jpg/512px-New_Delhi_Railway_Station_Ajmeri_Gate_side%2C_India._01.jpg"></a>
<figcaption>
New Delhi Railway Station, India. <a href="https://www.openstreetmap.org/?mlat=28.641956&mlon=77.22235&zoom=15#map=15/28.6420/77.2224">Location on OpenStreetMap</a>  <br> License: <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
</figcaption>
</figure>
<br>
