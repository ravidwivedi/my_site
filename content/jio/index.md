---
title: "Reasons to not use Jio"
date: 2021-11-15
draft: false
---
Last updated: Wednesday 24 November 2021

- Jio has a huge subscriber base in India and thus [threatening to be a monopoly](https://www.thehindubusinessline.com/opinion/columns/c-p-chandrasekhar/the-rising-spectre-of-a-telecom-monopoly/article36063279.ece). We should not allow any company to be a monopoly and Jio is no exception. A monopoly in a sector controls that sector because of lack of competitors. A monopoly means Jio having full control over the telecom sector. [Vodafone and Idea already merged](https://indianexpress.com/article/technology/tech-news-technology/from-vodafone-idea-merger-to-jio-airtel-tariff-hike-how-2019-changed-telecom-sector-6185302/) because they couldn't compete with Jio leaving Indians with lesser number of telecom providers. 

- Jio reportedly [selling user data to ad networks](http://www.thehindubusinessline.com/info-tech/reliance-jio-selling-user-data-to-ad-networks-abroad-says-anonymous/article9147032.ece). 

- Jio [uses Deep Packet Inspection](https://cis-india.org/internet-governance/blog/reliance-jio-is-using-sni-inspection-to-block-websites) to censor websites.

- Jio [blocks VPN sites and violates net neutrality rules](https://thenextweb.com/news/indias-massive-jio-carrier-is-blocking-vpn-sites-and-violating-net-neutrality-rules?fbclid=IwAR0QD59WVkN1ceSL27JEJUymhSpdRo5wWmcXWV046wjrRREgGisnj0ETCqI).

- Jio [censors many proxy websites](https://libredd.it/r/india/comments/ac62hp/jio_is_blocking_proxy_websites/) in India violating the users' freedom. 
