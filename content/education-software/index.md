---
title: "Free Software for Education"
draft: false
date: 2021-07-29
---

Last Updated on: Wednesday 09 February 2022 

### Index

- [Introduction](#introduction)

- [What happens when we invite proprietary software into education](#what-happens-when-we-invite-proprietary-software-into-education)

- [Act Now](#act-now)

- [Replacement Software](#replacement-software)

- [Resources](#resources)

- [Examples of adoption of Free Software in Education](#examples-of-adoption-of-free-software-in-education)

- [Successful Resistance Against NonFree Software](#successful-resistance-against-nonfree-software)

### Introduction 

I would like to urge you to look at the image below:

<figure>
<img src="/images/19-jan-2022_World_map_of_share_of_the_population_fully_vaccinated_against_COVID-19_by_country.png" width="800" height="565">
<figcaption>Taken from: <a href="https://en.wikipedia.org/wiki/File:World_map_of_share_of_the_population_fully_vaccinated_against_COVID-19_by_country.png">Wikipedia</a> </figcaption>
</figure>

The above is an image of the World Map showing the deployment of Covid-19 vaccine at the time of writing this (09-Feb-2022). The map is colored according to the following scheme: The more the percentage of population is vaccinated in a region, the darker green colored the region is, and the more lighter colored the region will be if the lesser share of population is vaccinated in that region. 

Quoting [Our World in Data](https://web.archive.org/web/20220209224138/https://ourworldindata.org/covid-vaccinations).

> <b>61.6% of the world population has received at least one dose of a COVID-19 vaccine, while only 10.6% of people in low-income countries have received at least one dose.</b> 

What does this show? 

It shows <b>inequality and the consequences of artificial restrictions.</b>

The vaccine is patented, which means that the formula is secret, and only the patent holders can manufacture it. This is an example of restricting people from gaining knowledge to control the production. 

What would happen if the formula was available to the public? The local manufacturers of low-income countries would get opportunities to manufacture the vaccine themselves, possibly leading to a greater rate of vaccination.  

This shows that <b>dependence leads to exploitation</b>. The poor countries do not have as many resources to put into research to create their own vaccine and as a result, they are dependent on rich countries for the vaccine. 

Similarly, education sector is hijacked by companies like Google, Microsoft, Facebook(WhatsApp is used by schools to disseminate information), etc. 
Their software is proprietary software which is controlled by the companies and not the educational institutes. Proprietary Software use means that the educational institute is dependent on the developer or company, like Google, Microsoft, etc. for their software needs. It also means that the owner can cut off access or increase prices any time. The source code of these softwares is not available, which means lack of transparency and auditability. In other words, you don't know what the software does.

Imagine what the world would be like if mathematics is declared proprietary, i.e., you can't have proofs of the theorems. Or maybe the mathematics behind encryption is a secret and a patent, nobody else can read and understand it other than the mathematician who discovered it. What would be the world like? Sounds ridiculous, no? Well, that is exactly what these software companies do: they don't provide us the source code, restricting the knowledge. 

Recently, [a patent-free vaccine, named Corbevax, released with no restrictions on use and manufacture](https://www.theguardian.com/us-news/2022/jan/15/corbevax-covid-vaccine-texas-scientists). This gives opportunities for low-income countries to create their vaccine rather than depend on a few profit-hungry pharma companies. Free('Free' as in freedom) Software or Mukt Software or Swatantra Software is similar to this patent-free vaccine. It gives access and opportunities to all. <b>Free Software means that the users get freedom to run, study, modify, share and share the modified versions of the software</b>. Therefore, educational institutes should use only Free Software. Following are some of the consequences of freedom that Free Software gives:

- It is transparent and auditable due to users having source code. In other words, you know what the software does.

- No restrictions on use and no license fee to pay.

- Educational institutes can modify and adapt the software according to them. 

- No dependence on a few companies like Google, Microsoft for their software in education.

- The source code is available from which interested people can learn, which is similar to knowledge of proofs of theorems being available.

- Sharing as well as sharing the modifications is also allowed, which means that even if one educational institute makes some change in the software which is helpful to others, then they can just share the modifications and benefitting everyone.

Check out [how Scribus being Free Software helped newspapers in Kerala](/posts/scribus) in publishing in Malayalam language. For a list of Free Software, check out the [Replacement Software section](#replacement-software) of this page or [my Free Software list](/free-software-list).

### What happens when we invite proprietary software into education

So, what happens when we invite companies like Google, Microsoft into education and depend on their software? In the guise of "spreading education" or "giving access to all for free-of-cost", they take away our rights. For instance, [Electronic Frontier Foundation found that Google basically records students' lives through the software they use for education](https://www.eff.org/wp/school-issued-devices-and-student-privacy). In another instance, Proprietary programs used for online classes, Google Meet, Microsoft Teams, and WebEx [are collecting user's personal and identifiable data](https://www.consumerreports.org/video-conferencing-services/videoconferencing-privacy-issues-google-microsoft-webex/) including how long a call lasts, who's participating in the call, and the IP addresses of everyone taking part. 

Google wants to retain its monopoly in ad business and search engine, and educational institutes using Google are helping feed their students' data into Google's database. Microsoft is another company which sells licenses to educational institutes to use Microsoft Windows, Microsoft Office etc. A lot of times, WhatsApp groups are used in classes for distributing information. These companies will extract as much data as possible from the teachers and students.

If you think massive surveillance is not a problem, or you have nothing to hide, check out this nice resource [socialcooling.com](https://socialcooling.com).

### Act Now

- Sign [Open Letter to Kerala Teachers by FSCI](https://fsci.in/blog/letter-to-kerala-teachers/) in response to KITE accepting free-of-cost offer of Google's G-Suite in education.

- [Join mailing list](https://lists.fsci.in/postorius/lists/fs-edu.fsci.in/) for discussion and promoting Free Software in education.

- Switch to Free Software. If you need help, please contact the email address contact at fsci dot in.

### Replacement Software

Free software replacements of all the proprietary software are readily available and should be preferred. A list of free software for education is here:

- Operating system: GNU/Linux distros like [Debian](https://debian.org), [Ubuntu](https://ubuntu.com). 

- Online Classes: Jitsi, BigBlueButton. Feel free to use [meet.fsci.in](https://meet.fsci.in) for your audio/video calls. 

- Instant Messenger: [Quicksy](https://quicksy.im), Element, Conversations. 

- Uploading videos: [PeerTube](https://joinpeertube.org/)

- E-learning platform: Moodle, BigBlueButton

- Recording Lectures: [OBS](https://obsproject.com/)

- Sharing notes, lecture videos etc. : [Nextcloud](https://nextcloud.com/), Lufi

- Digital writing pad: Xournal

- Document editor: LibreOffice, [Cryptpad](https://cryptpad.fr), Etherpad, EtherCalc.

- Form filling: KoBoToolbox, NextCloud Forms — Disroot provides NextCloud Forms.

- Activities for children - [GCompris](https://gcompris.net/index-en.html): GCompris is a high quality educational software suite, including a large number of activities for children aged 2 to 10.

- Email: Institutes can hire a free software consultancy, like [Deeproot GNU/Linux](https://deeproot.in/), to set up their own mail server. For self-hosting mails, [Mail-in a-Box](https://mailinabox.email/) , [iRedMail](https://www.iredmail.org/) and [Freedom Box](https://www.freedombox.org/) are good options. Further, the users of the mail server(students, teachers, other staff etc.) can [use end-to-end encryption](https://fsci.in/blog/pep-guide/) so that only the participants of a communication can read the mails.

### Resources:

- [Proprietary Software is often malware](https://gnu.org/malware).

- [Free Software for education booklet](/files/free-software-for-edu-booklet.pdf). 

- Read [Self Hosting, Federation and end-to-end encryption - Freedom and privacy in the 'cloud'](https://fsci.in/blog/self-hosting-and-federation/). 
- [Teacher's toolkit for using Free Software in education and freeing the educational resources](https://teacher-network.in/OER/index.php/Teachers'_toolkit_for_creating_and_re-purposing_OER_using_FOSS) by [IT for Change](https://itforchange.net/).

- Karnataka Open Educational Resources [in English](https://karnatakaeducation.org.in/KOER/en/index.php/Main_Page), in [Kannada](https://karnatakaeducation.org.in/KOER), and in [Hindi](https://teacher-network.in/OER/hi/index.php/%E0%A4%AE%E0%A5%81%E0%A4%96%E0%A4%AA%E0%A5%83%E0%A4%B7%E0%A5%8D%E0%A4%A0). 
 

### Examples of adoption of Free Software in Education

Here are some examples of adoption of free software in education. Many of them self-host the services rather than depending on Google, Microsoft etc. If they can do it, others can do it too. 

- [The Department of Scientific Computing, Modeling & Simulation, Savitribai Phule Pune University Pune, India](https://fsf.org.in/case-study/unipune/)

- [Story of mass adoption of free software in education in Kerala](http://www.space-kerala.org/files/Story%20of%20SPACE%20and%20IT%40School.pdf)

	The teachers resisted offers of free of cost training by Microsoft and Intel. They created a custom GNU/Linux distribution with software they need in schools, and they provided technical support and training to teachers across Kerala. 

- [Ambedkar Community Computing Center (AC3)](https://www.gnu.org/education/edu-cases-india-ambedkar.html)
A group of Free Software advocates set up a computing center in a slum in Bangalore to teach computer skills to underprivileged children.

- [Vocational Higher Secondary School Irimpanam (VHSS Irimpanam)](https://www.gnu.org/education/edu-cases-india-irimpanam.html).

	One of the several thousand schools that are using exclusively Free Software as the result of a project carried out by the government in the state of Kerala.

- [Escuela Cristiana Evangélica de Neuquén (ECEN)](https://www.gnu.org/education/edu-cases-argentina-ecen.html)

	An elementary teacher with little technical skills manages to get her school to completely migrate to Free Software on the grounds that proprietary software is against the moral and ethical values promoted by the school.


### Successful Resistance Against NonFree Software

 (Source: [GNU](https://www.gnu.org/education/successful-resistance-against-nonfree-software.html#content))

- A student of Computer Science in Poland [fights back](https://www.gnu.org/education/how-i-fought-to-graduate-without-using-non-free-software.html) proprietary software at his university and manages to graduate using only free software.
    
- In Spain, a father succeeds in getting some of his children's schools to [drop Skype and switch to Jitsi](https://www.gnu.org/education/remote-education-children-freedom-privacy-at-stake.html).
    
- As soon as MIT Prof. Gerald Jay Sussman realized that nonfree licenses were being offered for teaching online classes, he saw the moral issue at stake and started working to ensure he would not use them. [He succeeded in teaching his Spring 2020 online classes using exclusively free software](https://www.gnu.org/education/teaching-my-mit-classes-with-only-free-libre-software).
    
- Free software activists in Valencia, Spain, have set up [a server running BigBlueButton](https://bbb.valenciatech.com/b/), an advanced web conferencing system that features sharing of media files and documents, whiteboard, and other capabilities.
