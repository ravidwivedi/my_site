---
title: "Reasons not to use proprietary videoconferencing services"
date: 2021-07-25
---
Last updated: Wednesday 10 November 2021 

Zoom, Google Meet, Skype, Microsoft Teams are [nonfree software](https://ravidwivedi.in/posts/free-sw) which means that users don't control them. These software are malware and I list some of the ways in which they mistreat you:  

- Zoom [sends data to Facebook](https://www.vice.com/en/article/k7e599/zoom-ios-app-sends-data-to-facebook-even-if-you-dont-have-a-facebook-account).

- Proprietary programs Google Meet, Microsoft Teams, and WebEx are [collecting user's personal and identifiable data](https://www.consumerreports.org/video-conferencing-services/videoconferencing-privacy-issues-google-microsoft-webex/) including how long a call lasts, who's participating in the call, and the IP addresses of everyone taking part. 

- Skype [snoops on you](https://web.archive.org/web/20210317012028/https://www.washingtonpost.com/business/economy/skype-makes-chats-and-user-data-more-available-to-police/2012/07/25/gJQAobI39W_story.html) and does not respect your privacy/liberty.

- Microsoft contractors [listen to Skype calls](https://www.vice.com/en_us/article/xweqbq/microsoft-contractors-listen-to-skype-calls).

- Skype gave [personal data about a Wikileaks supporter](http://www.slate.com/blogs/future_tense/2012/11/09/skype_gave_data_on_a_teen_wikileaks_supporter_to_a_private_company_without.html) to another company without any legal obligation to do so. 

- Zoom [shuts down accounts of activists](https://www.theguardian.com/technology/2020/jun/11/zoom-shuts-account-of-us-based-rights-group-after-tiananmen-anniversary-meeting) who were linked to events to mark the anniversary of the 1989 Tiananmen massacre or were to discuss China’s measures to exert control over Hong Kong.

- Zoom has [agreed to censor events](https://www.theverge.com/2020/6/12/21288995/zoom-blocking-feature-chinese-government-censorship) on behalf of Chinese government. 

- Zoom app [listening through users' microphone even when not in a meeting](https://community.zoom.com/t5/Meetings/Why-is-the-Zoom-app-listening-on-my-microphone-when-not-in-a/td-p/29019).

- A Zoom executive [carried out snooping and censorship for China](https://www.washingtonpost.com/technology/2020/12/18/zoom-helped-china-surveillance/).

- Zoom [lied to users about end-to-end encryption](https://arstechnica.com/tech-policy/2020/11/zoom-lied-to-users-about-end-to-end-encryption-for-years-ftc-says/) for many years.

- A zero-day vulnerability in Zoom which [can be used to launch remote code execution (RCE) attacks](https://www.zdnet.com/article/critical-zoom-vulnerability-triggers-remote-code-execution-without-user-input/) has been disclosed by researchers. The researchers demonstrated a three-bug attack chain that caused an RCE on a target machine, all this without any form of user interaction. It is a popular opinion that software made by professional companies like Zoom are very secure. Proprietary software can also have grave bugs and free software can also have bugs. Also, if proprietary software like Zoom has bugs then you are at the mercy of the developer for the big to be fixed because only the developer can make changes in the software. With [free software](/free-software), users can themselves fix the bug and share the modification with others so that it benefits everyone.

- Another example of serious bug in Zoom [allowed any malicious website to enable your camera without your permission](https://infosecwriteups.com/zoom-zero-day-4-million-webcams-maybe-an-rce-just-get-them-to-visit-your-website-ac75c83f4ef5?gi=69076cfc1a44). The bugs might not be Zoom's mistake but it goes to show that big companies also have bugs in their software. If the software was free, users could have fixed the bugs themselves rather for waiting for Zoom to fix them. 

- Zoom [censors Palestinian seminars](https://theintercept.com/2020/11/14/zoom-censorship-leila-khaled-palestine/) citing anti-terrorism laws. Many years ago, Leila Khaled was involved in hijacking a plane. But this doesn't mean that they are barred from using videoconferecing services for promoting their agenda, in this case, for liberation of Palestine. Why does Zoom gets to decide what is acceptable speech and what is not? 

<br>

An educational institute should not force Zoom/Google Meet/Microsoft Teams/Skype or any other nonfree software on their students and teachers because it attacks their liberty, privacy and security, as discussed above. 

You can use freedom-respecting alternatives like Jitsi Meet and BigBlueButton. These software give users the freedom to run, study, modify, share the software, and also users can install the software in servers controlled by them, taking full control over their communications. Self-hosting can increase costs financially but the attitude that everything must be free-of-cost is very dangerous as it ignores long-term consequences of these choices. Nothing is free, you might use some proprietary videoconferencing services at a cheaper rate, but in exchange you are giving away your liberty and privacy. Worse thing is, you are inviting other people to give away their privacy and liberty as well to talk to you/attend classes.

Further, teachers expect students to have camera-on and see their faces during the lectures. When the self-hosted server can't handle this much load, they switch back to proprietary software. Do we really have to replicate the model of offline-mode education in online classes? Is it required for everyone to turn on their camera? Is it possible that lectures be pre-recorded and live-streamed at the time of the class and students can interact using Etherpad or Matrix? 

The Department of Scientific Computing, Modeling & Simulation, Savitribai Phule Pune University Pune, India [uses only free software for teaching](https://fsf.org.in/case-study/unipune/). In particular, they use BigBlueButton for videoconferencing. If such a small department can use exclusively free software for teaching, then others can do it too.   

[FSCI](https://fsci.in) runs a privacy-respecting Jitsi service. Feel free to use [meet.fsci.in](https://meet.fsci.in) for your daily online meeting needs. You just need to go to the link [meet.fsci.in](https://meet.fsci.in) and create a room name and share the link with the people you would like to meet with. They only need to visit the meeting link from a web browser to join the meeting. If you would like to support meet.fsci.in financially, check out [this page](https://fund.fsci.in).

If you would like to join me in promoting software freedom in education, please join the group:

XMPP address: fs-edu@groups.poddery.com

Matrix address: #fs-edu:poddery.com.
