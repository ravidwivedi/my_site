---
title: "Ravi's Home"
---
Hi, I am Ravi from India. This is my personal website which gives me voice in the online spaces. 

Welcome to my space on the internet. Feel free to roam around and [interact with me](/contact).

If you would like to explore the world with me, I also have a specialized [travel blog](https://travel.ravidwivedi.in).

<figure>
<a href="https://www.fsf.org/fb"><img src="https://static.fsf.org/nosvn/no-facebook-me.png" alt="Not f'd — you won't find me on Facebook" /></a>
<figcaption><a href="https://www.fsf.org/facebook">Boycott Facebook Campaign</a></figcaption>
</figure>

