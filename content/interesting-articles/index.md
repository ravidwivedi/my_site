---
title: "Interesting Articles"
date: 2021-07-31
---
Last updated: Thursday 11 November 2021 

The criteria of an article to be here is, well, whatever I find interesting.

- [Einstellung effect](https://en.wikipedia.org/wiki/Einstellung_effect) : The more we learn, the more blind we become to new ideas. 

	The following water jar test is an illustration of this phenomenon: You have three water jars and each jar can hold a fixed amount of water. The problem is to figure out how to measure a certain amount of water using these jars. Subjects used certain methods that they used to reach the solution previously even when very obvious direct methods existed. 

	Turns out that thinking with an open mind is pretty challenging.

- [The knowledge paradox: why knowing more is knowing less](https://arxiv.org/abs/1702.07227)

- [Coastline paradox](https://en.wikipedia.org/wiki/Coastline_paradox) : The coastline paradox is the counterintuitive observation that the coastline of a landmass does not have a well-defined length.

	An example of the coastline paradox. If the coastline of Great Britain is measured using units 100 km long, then the length of the coastline is approximately 2,800 km. With 50 km units, the total length is approximately 3,400 km, approximately 600 km longer.

- [Somatic marker hypothesis](https://en.wikipedia.org/wiki/Somatic_marker_hypothesis) :  The neuroscientist Antonio Damasio has built a case that although emotions can clearly bias decisions, the absence of emotions does not
necessarily lead to better decision making. In fact, in some cases, the absence of emotions can leave a person unable to decide at all.

- Research shows that setting a price restraint on oneself [can lead to increased spending](http://mindingmarketing.com/wp-content/uploads/2012/05/Budgeting_Backfires_JMR_20123.pdf), even when the consumer is faithful to the constraint. 

- [Deleted files are not really deleted](https://libredd.it/r/YouShouldKnow/comments/1e4p7g/ysk_how_deletion_works_on_a_computer_and_why/). 

- [Cryptophasia](https://en.wikipedia.org/wiki/Cryptophasia) is a phenomenon of a language developed by twins that only the twins can understand.

- [The paradox of value](https://en.wikipedia.org/wiki/Paradox_of_value) (also known as the diamond–water paradox) is the contradiction that, although water is on the whole more useful, in terms of survival, than diamonds, diamonds command a higher price in the market.

- [Individuals are less likely to offer help to a victim when there are other people present](https://en.wikipedia.org/wiki/Bystander_effect).

- [A very clever puzzle designed by New York Times](https://www.crosswordunclued.com/2009/01/nyt-election-day-crossword.html) a day before the results of presidential elections of 1996. It seemed like that the puzzle solvers need to know in advance to solve who will win the elections to solve the puzzle but no matter whether Clinton or Bob Dole is written as an answer, both fit in.  

- [Moral licensing](https://www.pickthebrain.com/blog/moral-licensing-how-being-good-can-make-you-bad/): When a person does something they consider moral that boosts their self-image makes them more suspectible to make immoral choices. For example ([taken from the wiki page](https://en.wikipedia.org/wiki/Self-licensing#Political_opinions_and_racial_bias)), an opportunity to endorse Barack Obama during the 2008 presidential election made his supporters more likely to express views that favored Whites at the expense of African-Americans. Essentially, the endorsement made people feel like they had shown that they were not biased, giving them a license to subsequently express racially prejudicial views. An article on this is [here](https://en.wikipedia.org/wiki/Self-licensing#Political_opinions_and_racial_bias).  

- [Ship of Theseus](https://en.wikipedia.org/wiki/Ship_of_Theseus)

- [Dunning-Kruger Effect](https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect) : Dunning Kruger effect refers to a cognitive bias that novices in a field of study overestimate their abilities and knowledge because they don't even know how little they know and how much there is to learn, while experts underestimate their skills and knowledge of that field. This happens because people knowing a lot about the field understand the richness and complexity, how many unsolved problems there are, and so on.  

- [Zipf Law](https://en.wikipedia.org/wiki/Zipf's_law) : Zipf's Law is an empirical law formulated using mathematical statistics that refers to the fact that for many types of data studied in the physical and social sciences, the rank-frequency distribution is an inverse relation. 

- How Sigmund Freud's nephew Edward Bernays used Freud's techniques to [manipulate the general public](https://en.wikipedia.org/wiki/Public_relations_campaigns_of_Edward_Bernays) paid by companies. This included convincing Americans into believing that the breakfast should be heavy. A bacon company hired him to do that. Another one was to manipulating women into smoking cigarettes publicly for a cigarette selling company.  

- [Mating Plug](https://en.wikipedia.org/wiki/Mating_plug) : Mating Plug is a gelatinous secretion used in the mating of some species. It is deposited by a male into a female genital tract, such as the vagina, and later hardens into a plug or glues the tract together. This gives the male sperm a time advantage to fertilize the female egg. It helps in guarding against rival male sperm. 

- The Economist Magazine's Millenial edition on 1st Jan 2000 chose [contraceptive pill as the greatest invention of the 20th century](https://www.economist.com/science-and-technology/1999/12/23/the-liberator).

- A person faked a South Korean bank robbery [to go to prison for free-of-cost healthcare](https://www.thestar.com.my/news/nation/2019/07/06/man-stages-fake-bank-robbery/). 
