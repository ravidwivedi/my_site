---
title: "Reasons not to use Spotify"
date: 2022-02-07
draft: false
---

Spotify is bad for users, musicians, and music industry as a whole. It has deliberate malpractices built into the platform which scam artists. It is proprietary, does surveillance on users, and it has absolute control over the users through their software. The whole music industry is being held captive by a few music streaming services. Let's reject Spotify. I reject all the music streaming service and you can do it too. To really solve the problem, we need to create more awareness so that artists get funded in ethical and fair ways. 

Rather than listening from this toxic platform, we can fund artists in different ways-- for example, by buying songs directly from them(without any DRM or restrictions on copying and sharing files).

Below are some of the reasons not to use Spotify:

- Spotify client is [nonfree/proprietary software](https://gnu.org/malware). Whenever we want to use a digital service through our devices, it must be accessible via [Free Software](/free-software). Even if it is accessible with Free Software, the service can still independently misbehave with artists and users, so the other points will still be valid.

- Users need to sign a contract, called an EULA, with Spotify to listen to music. In general, I don't think we should sign any contract with anyone to listen to music. Do you sign a contract while purchasing music from an offline store? 

- Spotify implements [DRM (Digital Restrictions Management)](https://www.defectivebydesign.org/) on the music files. DRM is evil in so many ways. Quoting Bruce Schneier, "..trying to make digital files uncopyable is like trying to make water not wet."
	
	DRM means you cannot copy the files or download them into your device. DRM gives control to Spotify and they can delete any song whenever they wish, [similar to Amazon's burning of the copies of 1984 from their Kindle](http://pogue.blogs.nytimes.com/2009/07/17/some-e-books-are-more-equal-than-others/). If Spotify shuts down in the future, all the music on Spotify will be inaccessible, similar to how Microsoft's DRMed ebook store [decided to shut down](https://www.popularmechanics.com/technology/a27021744/microsoft-ebook-store-shutdown/) which lead users to lose access of all the books. You can't save music to listen to it later or elsewhere, you can't take a snippet of a song and use it for something else like a presentation or review. Similar arguments apply to other DRM-enforcing dis-services like Amazon Prime, Hotstar, Netflix, etc. This feature gives these companies total control over the content accessible through them. I don't use any of them. You can do it too. 

- Spotify [snoops on the users](https://www.theguardian.com/music/2021/dec/01/spotify-wrapped-is-free-advertising-that-says-nothing-about-the-joy-of-music). Users have to create an account and Spotify knows everything about the listening history of the users. 

- Spotify [collects a lot of data on each user](https://web.archive.org/web/20160313214751/http://www.theregister.co.uk/2015/08/21/spotify_worse_than_the_nsa/) which they get from users in the guise of "improving our services for you". 

- Spotify [can tell whether you are sad or happy](https://www.theguardian.com/commentisfree/2018/sep/16/spotify-can-tell-if-youre-sad-heres-why-that-should-scare-you). The data and control that Spotify has over you, can be used to manipulate you in certain ways. To give you an example, Facebook can identify when [teenagers felt “insecure” and “worthless” or needed “a confidence boost”](https://www.theguardian.com/technology/2017/may/01/facebook-advertising-data-insecure-teens). They also ran experiments to see if it could [manipulate the mood of its users](https://www.theguardian.com/technology/2014/oct/02/facebook-sorry-secret-psychological-experiment-users). The control that these companies have over their users via their software is very dangerous. Now they can emotionally manipulate you. 

- Spotify is [bad for](https://www.theguardian.com/music/2021/jan/16/spotify-are-selling-adverts-not-music-how-to-stream-ethically) [artists' income](https://www.theguardian.com/commentisfree/2020/dec/03/music-streaming-major-labels-musicians-uk-government). It does not compensate them well for their work. A poll found that 82% of respondents earned less than £200 from streaming across the whole of 2019.

- This one is another layer of malpractice on the top of proprietaryness of the service. Spotify [creates fake artists](https://www.musicbusinessworldwide.com/spotify-denies-its-playlisting-fake-artists-so-why-are-all-these-fake-artists-on-its-playlists/) in its platform which directly limits the opportunities for the real artists by taking away playlist spots from them. These fake artists [had a total of 2.85 billion streams](https://www.rollingstone.com/pro/features/fake-artists-have-billions-of-streams-on-spotify-is-sony-now-playing-the-service-at-its-own-game-834746/) on Spotify in a matter of two years. This dilutes the market share of the real artists because the artists revenue is their fraction of total streams. Fake artists having billions of streams dilutes an artists' fraction of total streams on Spotify. This is Spotify intentionally being unfair to the artists.

- Spotify to [let artists promote music for cut in their income from the platform](https://www.theguardian.com/technology/2020/nov/03/spotify-artists-promote-music-exchange-cut-royalty-rates-payola-algorithm).
