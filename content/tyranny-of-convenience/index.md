---
title: "The Tyranny of Convenience"
draft: false
date: 2021-10-04
---
Last Updated: 08 October 2021 06:59:51 PM IST

I suggest you to read New York Times article titled ['The Tyranny of Convenience'](https://www.nytimes.com/2018/02/16/opinion/sunday/tyranny-convenience.html).

We are increasingly depending on technology in exchange for convenience without even thinking about the cost we pay. Many of these "services" are free-of-cost in terms of money. Like Google products do not require paying money for the services. But the [cost we are paying for free-of-cost services are very very high](https://socialcooling.com). 

Quoting the above NY Times article

- "we err in presuming convenience is always good, for it has a complex relationship with other ideals that we hold dear. Though understood and promoted as an instrument of liberation, convenience has a dark side. With its promise of smooth, effortless efficiency, it threatens to erase the sort of struggles and challenges that help give meaning to life. Created to free us, it can become a constraint on what we are willing to do, and thus in a subtle way it can enslave us."

- As task after task becomes easier, the growing expectation of convenience exerts a pressure on everything else to be easy or get left behind. We are spoiled by immediacy and become annoyed by tasks that remain at the old level of effort and time. When you can skip the line and buy concert tickets on your phone, waiting in line to vote in an election is irritating. This is especially true for those who have never had to wait in lines (which may help explain the low rate at which young people vote).

- Everyone, or nearly everyone, is on Facebook: It is the most convenient way to keep track of your friends and family, who in theory should represent what is unique about you and your life. Yet Facebook seems to make us all the same. Its format and conventions strip us of all but the most superficial expressions of individuality, such as which particular photo of a beach or mountain range we select as our background image.

- Today’s cult of convenience fails to acknowledge that difficulty is a constitutive feature of human experience. Convenience is all destination and no journey. But climbing a mountain is different from taking the tram to the top, even if you end up at the same place. We are becoming people who care mainly or only about outcomes. We are at risk of making most of our life experiences a series of trolley rides.

- Embracing inconvenience may sound odd, but we already do it without thinking of it as such. As if to mask the issue, we give other names to our inconvenient choices: We call them hobbies, avocations, callings, passions. These are the noninstrumental activities that help to define us. They reward us with character because they involve an encounter with meaningful resistance — with nature’s laws, with the limits of our own bodies — as in carving wood, melding raw ingredients, fixing a broken appliance, writing code, timing waves or facing the point when the runner’s legs and lungs begin to rebel against the runner.

What cost are we paying in exchange of convenience? Is convenience always good? Do we really need to use a newly launched, say, Google product, in exchange for being put in surveillance all the time? 

I think that if you are giving away your freedom/liberty/privacy/freedom of speech in exchange for convenience, it is too high a cost to pay and the tradeoffs are not worth exchangng for convenience. 
