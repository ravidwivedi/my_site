---
title: "Quotes"
date: 2021-11-12
draft: false
---
- "Freedom of religion is important because freedoms are important, and not because religions are important." - [The Hindu Editorial](https://www.thehindu.com/opinion/editorial/essentially-flawed-the-hindu-editorial-on-the-karnataka-high-courts-hijab-verdict/article65231490.ece) on Karnataka High Court's verdict upholding the ban on the wearing of haed scarves(hijab) by the students.

- "The dependency of European countries on gas from Russia is also comparable to the dependency for IT infrastructure on a few US companies. At this point it's just not possible to boycott Amazon without completely breaking typical internet access." - [Bob Mottram](https://epicyon.libreserver.org/@bob/108010399257265050).

- "I cannot understand why we idle discussing religion. If we are honest—and scientists have to be—we must admit that religion is a jumble of false assertions, with no basis in reality. The very idea of God is a product of the human imagination. It is quite understandable why primitive people, who were so much more exposed to the overpowering forces of nature than we are today, should have personified these forces in fear and trembling. But nowadays, when we understand so many natural processes, we have no need for such solutions. I can't for the life of me see how the postulate of an Almighty God helps us in any way. What I do see is that this assumption leads to such unproductive questions as to why God allows so much misery and injustice, the exploitation of the poor by the rich, and all the other horrors He might have prevented. If religion is still being taught, it is by no means because its ideas still convince us, but simply because some of us want to keep the lower classes quiet. Quiet people are much easier to govern than clamorous and dissatisfied ones. They are also much easier to exploit. Religion is a kind of opium that allows a nation to lull itself into wishful dreams and so forget the injustices that are being perpetrated against the people. Hence the close alliance between those two great political forces, the State and the Church. Both need the illusion that a kindly God rewards—in heaven if not on earth—all those who have not risen up against injustice, who have done their duty quietly and uncomplainingly. That is precisely why the honest assertion that God is a mere product of the human imagination is branded as the worst of all mortal sins." - [Paul Dirac](https://en.wikipedia.org/wiki/Paul_Dirac#Views_on_religion).

- "How much would someone have to pay before you would let them read your diaries, find out what your religious beliefs, political leanings or sexual preferences were, or where your children go to school? 

	What many of us are not aware of is that we are freely giving away exactly this kind of information to websites that we use every day. More importantly, we are doing this on a massive scale." - [Eben Moglen](http://news.bbc.co.uk/2/hi/technology/8508814.stm)

- "Now that corporations dominate society and write the laws, each advance or change in technology is an opening for them to further restrict or mistreat its users." - [Richard Stallman](https://www.gnu.org/philosophy/stallmans-law.html)

- "It's not that we don't dare do things because they are difficult; rather, they are difficult because we don't dare" - Seneca

- "The natural flow of technology tends to move in the direction of making surveillance easier" and "the ability of computers to track us doubles every eighteen months." - [Phil Zimmermann](https://web.archive.org/web/20130815064716/http://gigaom.com/2013/08/11/zimmermanns-law-pgp-inventor-and-silent-circle-co-founder-phil-zimmermann-on-the-surveillance-society/), creator of PGP

- "What’s wrong with Mr. Zuckerberg is that he pretends that he is renting you a private hotel room and he’s got a peephole" - [Eben Moglen](https://web.archive.org/web/20130902002208/https://www.forbes.com/sites/adamtanner/2013/08/28/the-man-who-would-not-use-facebook-for-anything-less-than-a-million-dollars/)

- "Some folks: if we just let billionaires get as powerful as they can, they will save the world.

	Billionaires: *fuck off to Mars with indentured servants after strip-mining the planet*" -[Aran Balkan](https://mastodon.ar.al/@aral/107541209495867028)

- "The concept of talent as a god-given blessing is inherently wrong. It actively hinders people to try new stuff and suck at it at first.
Because that is what you'll do when you try new things: you will suck. For some time. Yes, some will learn faster than others, experiences differ! But if you want to learn something new, please don't think you need any kind of talent for that.
What you need is passion. And time.
And the courage to fail." - [A toot from Mastodon](https://weirder.earth/@TQ/107557487160841403).

- "We live in a world that’s rapidly stripped away from one of our core values: freedom of choice. Freedom doesn’t exist without real choice. This mainly applies to a component that’s becoming more and more part of our daily lives: our mobile devices. These are often not designed with our best interests in mind, but yet they fulfill a central place in our lives. The mobile market is dominated by a few big companies. They let us use their services and make us dependent on them, in return for our data (how, where and what we communicate). These companies control the complete supply chain, from manufacturers to employees, from telecom providers to subcontractors, from developers to end-users. One way or another, this affects you." -[UBports Manifesto](https://ubports.com/nl/manifesto).

- "There's a lot of privilege in the whole 'don't have anything to hide'. It assumes people aren't out to get you just for being who you are, that you can trust cops and government not to persecute you, etc and so forth.

	I live 'loud and proud' because I can't do anything else, not because it is safe for me to do so. And I am lucky and privileged in having more safety than many who can't afford to trust our neighbors or government." - [Jess Mahler's Mastodon toot](https://wandering.shop/@jessmahler/107462894373849512)

- "Governments can change on a dime. A more tolerant regime could be replaced at any time by an intolerant one, whether they're democratically elected or not. Now all of a sudden you *do* have something to hide." - [Mastodon Toot](https://chitter.xyz/@faoluin/107463008001608324)

- "All it costs to fulfill the average person's needs for digital services is a $5 per month VPS instance. It's even cheaper if you buy a single board computer and host your services at home.

	We've been letting companies steal our personal data and sell it to the highest bidder and destroying democracy in the process all to save what? A coffee a month?

	This is one of the worst deals in history." - [Joseph Nuthalapati](https://social.masto.host/@njoseph/100299477861821188) 

- "There is surely no more reliable way to kill enthusiasm and interest in a subject than to make it a mandatory part of the school curriculum." -[The Mathematician's Lament](https://www.maa.org/external_archive/devlin/LockhartsLament.pdf)

- "Nations are just imaginary boundaries" - Ravi

- "Arguing that you don't care about privacy because you have nothing to hide is no different than saying you don't care about free speech because you have nothing to say." - Edward Snowden

- "I bought a Liberated Computer and the reason is simple: the attitude that freedom comes first and features are secondary." -	Ravi Dwivedi

- "Computers are like humans – they do everything except think." - John von Neumann

- "Teach everything you know. -	Even if it's already been taught 100 times before.

	The experiences you've had and the way you word things will be different than the 100 times before. 

	Because of that, there are people that it will just _click_ for them.

	Because _you_ taught it." - [@g@hackers.town](https://hackers.town/@g/107260586230033574)

- "Open Source wants to create better software, Free Software wants to create better society." - Pirate Praveen

- "If I can provide to everyone all goods of intellectual value or beauty, for the same price that I can provide the first copy of those works to anyone, why is it ever moral to exclude anyone from anything? If you could feed everyone on earth at the cost of baking one loaf and pressing a button, what would be the moral case for charging more for bread than some people could afford to pay?"  -	[Eben Moglen](http://moglen.law.columbia.edu/publications/maine-speech.html)

- "With software, there are only two possiblities: either the user controls the software or the software controls the user. If the software controls the user and the developer controls the software, the program becomes an instrument of unjust power." - Richard Stallman

- "You can't force others to pay you over and over for the same piece of code you wrote."  - Pirate Bady

- "Adding proprietary license to a software is like polluting a clean source of water so that you can sell bottled water." - Arun from FSF India

- "The only way someone can never "manipulate" your thoughts is by never coming in contact with you." -	[Akshay S Dinesh](https://blog.learnlearn.in/2021/10/on-leadership.html)

- "You always have an option to learn law by yourself and handle your own case in the court, but if you don't have time, interest or confidence, you can depend on someone else who has more expertise than you. This is also the case with Free Software, you always have an option to learn it yourself and make it work for you as you needed, but if you don't have time or interest, you can outsource it." - Pirate Praveen

- "Proprietary Software is cancer which needs to be eradicated completely." -	[Snehal Shekatkar](https://inferred.co)

- "Activism is now necessary to use Free Software. So let's see if necessity is really mother of inventions and we get more activists." - Pirate Praveen

- "Even in the situations in which you are "forced" to use WhatsApp or Zoom or any other nonfree software because of pressure by educational institute or jobs, at least give a fight. Don't assume that they won't be convinced." - Ravi Dwivedi

- "If people do not believe that mathematics is simple, it is only because they do not realize how complicated life is." - John von Neumann

- "In mathematics you don't understand things. You just get used to them" - John von Neumann

- "You know, some people worry that Russia will manipulate the US election through Facebook and Twitter. Well, I'm sure it's trying to, but I'm even more scared of the rich Americans who are doing even more of that. So, you know, we have these horrible trends. And we have to reverse them somehow. And the paradox is, the only reason it's hard is because each of us thinks, “I'm alone, what could I possibly do?” Or even, “I and my 10 friends, what could we possibly do?” But if you try, sometimes your effort grows and grows. So you must try." - Richard Stallman said [here](https://www.rt.com/shows/sophieco-visionaries/501613-stallman-data-protection-privacy/).

- "If You Are Not Paying for the Product, You Are the Product!" - [Source](https://medium.com/change-your-mind/if-you-are-not-paying-for-the-product-you-are-the-product-4dbc15b9a3f2)

- “We should not be comfortable or content in a society where the only way to remain free of surveillance and repression is if we make ourselves as unthreatning, passive, and compliant as possible.” ― Glenn Greenwald

- "'Terrorism': the word that means nothing, yet justifies everything." -Glenn Greenwald

- "It is like 5 blind people visiting an elephant. Each of them touched a different part of the animal and later described the elephant in 5 different ways to other blind people once they returned. So those who listen got 5 different versions of the elephant. In the same way, we should describe what touches us (what we think is important) to others. If it is freedom of every user and a better society, then please use Free Software or Freedom Respecting Software or one of the local language terms like Mukt Software, Swathanthra Software, Libre Software etc. If you think creating better software is more important, that is fine too, you can use Open Source. If you don't want to take sides in this debate, you can use foss or floss. But whichever term you use, understand it is your choice and choose wisely." - Pirate Praveen on explaining the difference between Free Software and Open Source Software

- "With chains of matrimony and modesty, you can shackle my feet. The fear will still haunt you that crippled, unable to walk, I shall continue to think." - Kishwar Naheed
