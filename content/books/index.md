---
title: "Book Suggestions"
date: 2021-09-06
---

Last Updated: Tuesday 26 April 2022 

These are the non-academic books I read and liked. Some of them I like very much like 'A Clockwork Orange' but others are just okayish and my likings of them is not at the same level as A Clockwork Orange. There are books I read and did not like. They are not in this list such as Ernest Hemingway's The 'Old Man and the Sea'.

<b>I have collected some quotes from these books on a <a href="/book-quotes">separate page</a>.</b>

### Fiction

When I read George Orwell's book 'Nineteen Eighty-Four', I liked it so much that I searched for other dystopian novels. Dystopia became my favorite genre. My favorite authors are Margaret Atwood, George Orwell, Franz Kafka, Albert Camus, Herman Hesse, Chuck Palahniuk, Kazuo Ishiguro, Phillip K. Dick, Haruki Murakami.

- [The Remains of the Day](https://en.wikipedia.org/wiki/The_Remains_of_the_Day) by Kazuo Ishiguro: A literary masterpiece.

- [Flowers for Algernon](https://en.wikipedia.org/wiki/Flowers_for_Algernon) by Daniel Keyes: Heartbreaking. Highly recommended.

- [Galactic Pot Healer by Phillip K. Dick](https://en.wikipedia.org/wiki/Galactic_Pot-Healer) : A totalitarian future USA in which information and knowledge are not free. Citizens' moves are monitored and thoughts are controlled. A pot healer on earth is unemployed due to his skill becoming obsolete as people prefer buying plastic pots over ceramic pots. An alien from other planet recruits the pot healer and other experts from other planets for a task. The book has many themes: repressive state, free will, destiny etc. 

- [A Scanner Darkly by Philip K. Dick](https://en.wikipedia.org/wiki/A_Scanner_Darkly): I think I need a reread as I didn't understood it completely but I liked the fictional world created by Phillip K. Dick in this one.

- [A Clockwork Orange by Anthony Burgess](https://en.wikipedia.org/wiki/A_Clockwork_Orange_(novel)) : This raises a very important moral question, "Is it better to choose to be bad rather than being conditioned to do only good?"

- [The Castle by Franz Kafka](https://en.wikipedia.org/wiki/The_Castle_(novel)) : I think it is a satire on bureacracy. 

- [Nineteen Eighty-Four by George Orwell](https://en.wikipedia.org/wiki/Nineteen_Eighty-Four) : 

- [Klara and the Sun by Kazuo Ishiguro](https://en.wikipedia.org/wiki/Klara_and_the_Sun)

- [Cat's Cradle by Kurt Vonnegut](https://en.wikipedia.org/wiki/Cat%27s_Cradle)

- [Oryx and Crake by Margaret Atwood](https://en.wikipedia.org/wiki/Oryx_and_Crake)

- [Invisible Monsters by Chuck Palahniuk](https://en.wikipedia.org/wiki/Invisible_Monsters)

- [Survivor by Chuck Palahniuk](https://en.wikipedia.org/wiki/Survivor_(Palahniuk_novel))

- [A Tale for the Time Being by Ruth Ozeki](https://en.wikipedia.org/wiki/A_Tale_for_the_Time_Being) : A very beautiful and tragic story. It made me very sad and I got burst into tears while reading it. 

- [The Metamorphosis by Franz Kafka](https://en.wikipedia.org/wiki/The_Metamorphosis) : Explores the theme of alienation through surrealism.

- [The Stranger by Albert Camus](https://en.wikipedia.org/wiki/The_Stranger_(Camus_novel))

- [To Kill a Mockingbird by Harper Lee](https://en.wikipedia.org/wiki/To_Kill_a_Mockingbird) : A hypocritic society seen through the lens of children of around 8 years old. 

- [Norwegian Wood by Haruki Murakami](https://en.wikipedia.org/wiki/Norwegian_Wood_(novel))

- [Siddhartha by Hermann Hesse](https://en.wikipedia.org/wiki/Siddhartha_(novel))

- [Fight Club by Chuck Palahniuk](https://en.wikipedia.org/wiki/Fight_Club_(novel))

- [In the Miso Soup by Ryū Murakami](https://en.wikipedia.org/wiki/In_the_Miso_Soup)

- [Kafka on the Shore by Haruki Murakami](https://en.wikipedia.org/wiki/Kafka_on_the_Shore)

- [Animal Farm by George Orwell](https://en.wikipedia.org/wiki/Animal_Farm)

- [Ubik by Phillip K. Dick](https://en.wikipedia.org/wiki/Ubik) -Science fiction and philosophical fiction which blurs the boundary between reality and imagination. 

- [The Handmaid's Tale by Margaret Atwood](https://en.wikipedia.org/wiki/The_Handmaid%27s_Tale)

- [The Hairdresser of Harare by Tendai Huchu](https://en.wikipedia.org/wiki/The_Hairdresser_of_Harare)

- [The Murder of Roger Ackroyd by Agatha Christie](https://en.wikipedia.org/wiki/The_Murder_of_Roger_Ackroyd) : Very good plot. If I tell you anything about a detective novel like this, it will spoil the novel for you. 

### Nonfiction

- The Selfish Gene by Richard Dawkins.

- [How Democracies Die](https://en.wikipedia.org/wiki/How_Democracies_Die) by Steven Levitsky and Daniel Ziblatt

- [The Silent Coup: A History of India’s Deep State](/posts/the-silent-coup) by Josy Joseph.

- [Annihilation of Caste](https://en.wikipedia.org/wiki/Annihilation_of_Caste) by Ambedkar.

- [Phantoms in the Brain by Sandra Blakeslee, Vilayanur S. Ramachandran](https://en.wikipedia.org/wiki/Phantoms_in_the_Brain)

- [Thinking, Fast and Slow by Daniel Kahneman](https://en.wikipedia.org/wiki/Thinking%2C_Fast_and_Slow)

- Six Easy Pieces by Richard Feynman

- [Sapiens: A Brief History of Humankind](https://en.wikipedia.org/wiki/Sapiens:_A_Brief_History_of_Humankind)

- [Freakonomics by Steven D. Levitt and Stephen J. Dubner](https://en.wikipedia.org/wiki/Freakonomics)

- [Free Software Free Society: Selected Essays of Richard M. Stallman, 3rd Edition](https://shop.fsf.org/books-docs/free-software-free-society-selected-essays-richard-m-stallman-3rd-edition)

- [Data and Goliath: The Hidden Battles to Collect Your Data and Control Your World by Bruce Schneier](https://www.schneier.com/books/data-and-goliath/)

- [No Place to Hide by Glenn Greenwald](https://en.wikipedia.org/wiki/No_Place_to_Hide_%28Greenwald_book%29)

- [Predictably Irrational: The Hidden Forces That Shape Our Decisions by Dan Ariely](https://en.wikipedia.org/wiki/Predictably_Irrational)

- [Prisoners of Geography by Tim Marshall](https://en.wikipedia.org/wiki/Prisoners_of_Geography)

- [The Problems of Philosophy by Bertrand Russell](https://en.wikipedia.org/wiki/The_Problems_of_Philosophy)

<br>
