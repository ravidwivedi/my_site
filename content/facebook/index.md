---
title: "Delete your Facebook account"
date: 2022-01-10
draft: true
---
<figure>
<a href="https://www.fsf.org/fb"><img src="https://static.fsf.org/nosvn/no-facebook-me.png" alt="Not f'd — you won't find me on Facebook" /></a>
<figcaption><a href="https://pirates.org.in/statements/boycott-facebook">Boycott Facebook</a> and use <a href="https://joinmastodon.org/">Mastodon</a>.</figcaption>
</figure>

Here is a [guide to delete your Facebook account](https://deletefacebook.com/guide/).

### Surveillance

- 
