---
title: "External Links"
date: 2021-07-13
---
Last Updated: Sunday 26 June 2022 03:07:44 AM IST

- [Social Cooling](https://www.socialcooling.com/) - On the inhibiting effects of knowing someone is always watching.

- A Decade of Darkness: Article-14's New Database Reveals [How A Law Discarded By Most Democracies Is Misused In India](https://article-14.com/post/a-decade-of-darkness-our-new-database-reveals-how-a-law-discarded-by-most-democracies-is-misused-in-india-61fcb8768d15c).

- [Why Software Should Be Free](https://www.gnu.org/philosophy/shouldbefree.html)

- Coffee as a [symbol of exploitation and slavery](https://foodispower.org/coffee/).

- [Richard Stallman's printer story which lead him to understand the problems with proprietary software](https://www.gnu.org/philosophy/rms-nyu-2001-transcript.txt). 

- [There is no security without privacy. And liberty requires both security and privacy](https://www.schneier.com/blog/archives/2008/01/security_vs_pri.html). The famous quote attributed to Benjamin Franklin reads: “Those who would give up essential liberty to purchase a little temporary safety, deserve neither liberty nor safety.”  

- [IDs and the Illusion of Security](https://www.schneier.com/essays/archives/2004/02/ids_and_the_illusion.html)

- [Browser fingerprinting: How to browse the Web with all privacy](https://www.opensourceforu.com/2017/08/browser-fingerprinting-private-web-browsing/).

- [Tracking Exposed](https://tracking.exposed/).

- [Dear Sites, I Do Not Block Ads](https://nandakumar.org/blog/2021/08/not-an-ad-blocker.html).

- [How Technology is Hijacking Your Mind](https://medium.com/thrive-global/how-technology-hijacks-peoples-minds-from-a-magician-and-google-s-design-ethicist-56d62ef5edf3).

- [PrivacyTools](https://privacytools.io/classic/) - Encryption and tools to protect against global mass surveillance.

- [Pussthecat privacy guide](https://wiki.pussthecat.org/en/privacy-guide)

- [Freeing the Mind : Free Software and the Death of Proprietary Culture by Eben Moglen](http://old.law.columbia.edu/publications/maine-speech.html) - In this article, Eben Moglen raises a very important moral question, "If I can provide to everyone all goods of intellectual value or beauty, for the same price that I can provide the first copy of those works to anyone, why is it ever moral to exclude anyone from anything? If you could feed everyone on earth at the cost of baking one loaf and pressing a button, what would be the moral case for charging more for bread than some people could afford to pay?" 

    The snowdrift website also [discusses the same idea](https://wiki.snowdrift.coop/about/economics) as Eben Moglen's article. 

    Richard Stallman's article on the same idea [is here](https://stallman.org/articles/end-war-on-sharing.html). 

- The [Eternal Value of Privacy](https://www.schneier.com/essays/archives/2006/05/the_eternal_value_of.html).

- [RSS Feeds for arbitrary websites](https://feed-me-up-scotty.vincenttunru.com/).

- [Common Myths about "Private Browsing"](https://support.mozilla.org/en-US/kb/common-myths-about-private-browsing).

- [The Danger of Software Patents](https://www.gnu.org/philosophy/danger-of-software-patents.en.html).

- [Anti-DRM campaign](https://www.defectivebydesign.org/).

- [Why Data Mining Won't Stop Terror](https://www.schneier.com/essays/archives/2005/03/why_data_mining_wont.html).

- Visit [Cover Your Tracks project by EFF](https://coveryourtracks.eff.org/) that allows you to understand how easy/difficult it is to identify and track your online activities based on how your browser appears to websites.

- How HTTPS and Tor Work Together to [Protect Your Anonymity and Privacy](https://www.eff.org/pages/tor-and-https).

- Here are some [bad things](https://truthout.org/articles/spy-on-me-im-innocent/) that could happen to you thanks to massive surveillance.

- Tor is [usually used by common people](https://www.torproject.org/about/torusers) contrary to the popular myth that only criminals and security breakers use Tor. Also, criminals can and do [build, sell, and trade](https://web.archive.org/web/20210102103321/http://voices.washingtonpost.com/securityfix/2008/08/web_fraud_20_tools.html) far larger and [more powerful networks](https://web.archive.org/web/20210102103324/http://voices.washingtonpost.com/securityfix/2008/08/web_fraud_20_distributing_your.html) than Tor on a daily basis.

- Security through obscurity is a process of implementing security within a system by enforcing secrecy and confidentiality of the system's internal design architecture. It is well known that [security with obscurity is a bad practice](https://en.wikipedia.org/wiki/Security_through_obscurity). In particular, this applies to proprietary software which do not allow users to access the source code, in other words, making source code secret. 

    More on why systems with secret designs [only give false sense of security](https://www.schneier.com/essays/archives/2004/10/the_non-security_of.html).

- [Browser Leaks](https://browserleaks.com/) is a website full of resources on how to protect yourself from tracking by websites.

- [Privacy analysis of common web browsers](https://unixsheikh.com/articles/choose-your-browser-carefully.html).

- True user privacy is [impossible without free software](https://www.fsf.org/bulletin/2019/spring/its-not-just-about-privacy).

- [Ethical Ads](https://www.ethicalads.io/).

- [The Firefox Privacy Guide for Dummies!](https://12bytes.org/articles/tech/firefox/the-firefox-privacy-guide-for-dummies/)

- [Bady's post](https://bady.miraheze.org/wiki/Blog:Rethinking_marriages_for_a_better_future) on Rethinking Marriages.

- [There's no good reason to trust blockchain technology](https://www.wired.com/story/theres-no-good-reason-to-trust-blockchain-technology/)

- [The Tyranny of Convenience](https://www.nytimes.com/2018/02/16/opinion/sunday/tyranny-convenience.html)

- [Does voting every four or so years to elect the public face of a highly centralized and bureaucratic machine mean that ordinary people control the state?](http://anarchy.be/faq/secB2.html#secb24)

- [How does capitalism affect liberty?](http://anarchy.be/faq/secB4.html)

- [TSA's airline security is a theater](https://stallman.org/airlines.html).

- [Business-Supremacy Treaties](https://stallman.org/business-supremacy-treaties.html).

- [Advice to run your own freedom-respecting software business](https://freesoftware.business/)

- [Why is capitalism deeply exploitative](http://anarchy.be/faq/secCint.html)

- India launched a program which spent money on an app to track which kids were malnourished, [but it didn't offer sufficient funds to feed them](https://scroll.in/article/1007521/).

- [Predators of Press Freedom Gallery](https://rsf.org/en/portraits/predator).

- [How to become a GOOD Theoretical Physicist](https://www.goodtheorist.science/) by [Gerard 't Hooft](https://en.wikipedia.org/wiki/Gerard_%27t_Hooft).

- [What is software obsolescence](https://fsfe.org/activities/upcyclingandroid/upcyclingandroid.html#id-overcoming-software-obsolescence-with-free-software)? Manufacturers often stop updating the software of a phone device. If the user continues using it, they face security risks. Or, they have to buy a new phone.

- [Is your printer spying on you?](https://www.eff.org/issues/printers)

- [Stalman's arguments for abolishing the patent law](https://www.luc.edu/media/lucedu/law/students/publications/llj/pdfs/45_1_stallman.pdf).

- [How a terrorism law in India is being used to silence Modi’s critics](https://www.theguardian.com/world/2021/dec/10/how-terrorism-law-india-used-to-silence-modis-critics).

- A very small list of ways in which [the Modi harmed India](https://thewire.in/government/narendra-modi-says-focus-on-duties-and-forget-rights-but-hes-let-india-down-on-all-11-duties).

- There is a huge gap between upper castes and lower castes sections in Indian society that even affirmative action policies are [not able to bridge this gap](https://thewire.in/caste/india-affirmative-action). Upper castes enjoy a lot of privilege in Indian society and fail to acknowledge it and even claim that they are victims of affirmative action. No surprise because people from upper caste section are the ones who gets to build India, make its laws, and usually the narrators of the story. 

-  Most of the profit of tech companies of today are generated by their users-- like, [by getting spied by their proprietary software](https://en.wikipedia.org/wiki/Surveillance_capitalism), adding places to Google Maps etc. rather than work by employees. Yanis Varoufakis argues that [this gives them a lot of power where users end up doing things on their behalf](https://nettime.org/Lists-Archives/nettime-l-2201/msg00051.html). In other words, you are running on a treadmill and they are earning money and power from it.  

- How Byju’s sells expensive subscription to underprivileged parents in India [exploiting the culture of emphasis on grades in Indian society and the hyper-competititve environment in higher education due to less number of seats in good colleges](https://restofworld.org/2021/inside-india-edtech-BYJUs/).

- [How to clean background noise in a video using ffmpeg and sox](https://www.zoharbabin.com/how-to-do-noise-reduction-using-ffmpeg-and-sox/)

- [A good explanation of problems of Web3 and NFT](https://blackgnu.net/on-blockchain.html).

- [In Yogi's UP, 48 Journalists Assaulted, 66 Booked, 12 Killed](https://thewire.in/media/in-yogis-up-48-journalists-assaulted-66-booked-12-killed-report).

- [Introduction to the wrecker of India](https://rationalwiki.org/wiki/Narendra_Modi).

- [Corrupt Modi](https://corruptmodi.com/en/).

- [Modi complicit in Gujarat riots 2002](https://narendramodifacts.com/faq_Modi2002.html?PageSpeed=noscript) [with total impunity](https://narendramodifacts.com/faq_courts.html?PageSpeed=noscript). If I disappear after linking these, you know whom to blame.

- [The former Delhi high court chief justice says the 'love-jihad' law creates an unnecessary communal rift in a peaceful society](https://thewire.in/law/love-jihad-ordinance-communal-rhetoric-divisive-justice-ap-shah).

- MoviePass [will track people’s eyes through their phone’s cameras](https://www.independent.co.uk/tech/moviepass-track-eyes-phone-cameras-b2013273.html) to make sure they don’t look away from ads. This is what you get from proprietary software.

- Arundhati Roy claims that resonances of Nazi Germany can be seen in Indian politics today but fascism in India [will ultimately be a failed experiment](https://thewire.in/politics/full-text-narendra-modis-star-is-falling-arundhati-roy).

- Bihar's [implementation of farm laws tells us how draconian they were](https://www.newsclick.in/Bihar-Shows-What-Happens-if-Agri-Trade-Left-Free-Market).

- India's simplistic potrayal of West Asian countries as Muslim countries, combined with the false narrative that constructs West Asia in opposition to India (the word 'India' is usually treated synonymously with Hindus), is used to create anti-Muslim sentiments among Hindus and [is used to justify the Indian state’s violence perpetuated against the innocent Muslims in India](https://thewire.in/religion/islamic-world-the-problems-with-indias-construction-of-west-asia-as-uniformly-muslim). As Indian citizens, we must not fall into this propaganda by the state and society, and treat citizens without the lens of communal, or any other, divide.

- [The Vast Difference Between What Demonetisation Achieved and How It Was Perceived](https://thewire.in/economy/vast-problematic-difference-demonetisation-achieved-perceived).

- Why Indian Muslim women are [a special target for Hindutva hatred](https://scroll.in/article/1018259/why-indian-muslim-women-are-a-special-target-for-hindutva-hatred). Majoritarian forces are threatened by the attempt of Muslim women to gain economic independence and assert their identity while enjoying their civil liberties.

- Gujarat riots 2002 has been monumental incidence in Indian history. It has [promoted and normalized a culture of revenge, rather than justice](https://scroll.in/article/1018256/gujarat-violence-what-2002-taught-us).

- India [promised Kashmiris a referendum and never conducted it](https://www.theindiaforum.in/article/article-370-federalism-and-basic-structure-constitution).

- [Facts Modi-fied by Modi](https://factsmodified.factchecker.in/).

- Twenty years after the Gujarat 2002 pogrom, [the propaganda against minorities has gone national](https://scroll.in/article/1018339/teesta-setalvad-its-vital-to-recall-the-sparks-of-hate-that-burst-into-gujarats-brutal-2002-riots).

- Covid-19 lockdown: Had India done what Malaysia did – kept everyone where they were by ensuring that their economic futures were not imperilled by the lockdown – [we would have been in a much better place right now](https://thewire.in/economy/the-lockdown-that-backfired).

- [How Gujarat 2002 turned India into a nation of pathological liars](https://thewire.in/communalism/how-gujarat-2002-turned-india-into-a-nation-of-pathological-liars).

- While hijabi Muslim women are asked to be secular, [practices of Hinduism have been secularised in the name of ‘Indian culture’](https://thewire.in/rights/why-do-secularists-see-hinduism-as-cultural-and-islam-as-religious).

- The CAA, which treats the modern day equivalents of people like Badshah Khan as 'illegal immigrants', and the 'all-India NRC' which jeopardises the status of millions of Indians have [energised a new moment of dissent](https://thewire.in/politics/by-protesting-a-law-that-divides-and-discriminates-we-are-forging-new-maps-of-belonging).

- Lynching is a modern form of tribalism, where enemies – differentiated by religion, race, caste or ideology – [are bracketed for elimination](https://thewire.in/communalism/the-lynching-of-a-nation).

- What we have witnessed over the last six years has been the slow but steady [demise of empathy, especially amongst those of the upper and middle classes](https://thewire.in/rights/aurangabad-migrant-deaths-empathy).

- Putting a ban on porn [won’t reduce cases of sexual violence](https://livewire.thewire.in/gender-and-sexuality/putting-a-ban-on-porn-wont-reduce-cases-of-sexual-violence/).

- The Wire [explains the Modi's phenomenon](https://thewire.in/politics/understanding-narendra-modis-political-appeal), despite all the failures or sabotaging he has done to the public, as greater disillusionment with democracy, heightened interest in politics, and increasing preference for a strongman leader, of the people born in or after 1970s, who, for the first time in 2014, became a decisive majority in the electorate.

- Rafale Deal: [Nine questions the Supreme Court verdict leaves unanswered](https://thewire.in/government/eight-questions-on-rafale-the-supreme-court-verdict-leaves-unanswered).

- It is becoming increasingly easy to circulate fake news, fake narratives and bogus claims, and [most importantly they are being believed by the people](https://thewire.in/politics/beware-of-the-avalanche-of-stupidity-threatening-to-bury-us-all). These are injected into our brains to manipulate people, change their opinion and political gains. Differentiating between real news and fake news has become a challenge for the people in this era. 

- [Very excellent analysis of problems with ELectoral Bonds](https://www.huffpost.com/archive/in/entry/rbi-warned-electoral-bonds-arun-jaitley-black-money-modi-government_in_5dcbde68e4b0d43931ccd200).

- Godhra, Where the [Fall of India's Democracy Began](https://thewire.in/communalism/godhra-where-the-fall-of-indias-democracy-began).

- [Dangerous Speech Project](https://dangerousspeech.org/).

- In ‘The Law of Force’, Thomas Blom Hansen [examines how violence has crossed over into the domain of political performance in India](https://scroll.in/article/1018602/how-and-why-did-indias-public-come-to-approve-of-state-violence-being-built-into-administration).

- Have politicians [licensed the people to enjoy violence and punish those they see as enemies](https://scroll.in/article/987914/have-politicians-licensed-the-people-to-enjoy-violence-and-punish-those-they-see-as-enemies)?

- [An Open Letter to The Prime Minister of India](https://livewire.thewire.in/politics/an-open-letter-to-the-prime-minister-of-india/).

- Muslim women in India [face discrimination in getting jobs and at workplaces](https://thewire.in/communalism/wont-look-good-for-business-muslim-women-face-workplace-discrimination-for-wearing-hijab) due to them wearing hijabs and other religious attire. We must have the right to wear whatever we want. And so, Muslim women, or even other non Muslim people, must have the right to wear hijab or burqa. Many Muslim women wear hijabs by choice.Even non-Muslim women wear headscarfs. This discrimination is not an attempt at "liberalizing" women, this only makes them feel targeted and I suspect that it is traumatizing for these women to be selectively discriminated. For example, I am not religious myself. I actively say that I am a non-believer of god and do not belong to any religion. Still, I do not eat meat, which is common in brahmins, due to my habit. I won't feel inclusive if someone making this abstainence from meat as my brahmin identity. What if people forcibly feed me meat in a guide of "liberalizing" me?

- State of caste-based oppression in India: [Quoting Human Rights Watch](https://www.hrw.org/report/2007/02/12/hidden-apartheid/caste-discrimination-against-indias-untouchables), "inhuman, and degrading treatment of over 165 million people in India has been justified on the basis of caste. Caste is descent-based and hereditary in nature. It is a characteristic determined by one's birth into a particular caste, irrespective of the faith practiced by the individual. Caste denotes a traditional system of rigid social stratification into ranked groups defined by descent and occupation. Caste divisions in India dominate in housing, marriage, employment, and general social interaction-divisions that are reinforced through the practice and threat of social ostracism, economic boycotts, and physical violence."

- [Summary of Annihilation of Caste by Ambedkar](https://thewire.in/caste/on-ambedkar-jayanti-remembering-the-annihilation-of-caste): Mere encouragement of inter-caste dining and inter-case marriages is not enough to break up the caste system. Hindus need to realize that caste is deeply rooted in their religion; unless they realize this and question the authority of the holy books, we cannot get rid of the caste system, which leaves us with the question: Is it reasonable to expect that the Brahmins will ever consent to lead a movement, the ultimate result of which is to destroy the power and prestige of the Brahmin caste?

- Gandhi family has become [active facilitators of Hindutva authoritarianism](https://scroll.in/article/1019295/ramachandra-guha-how-the-gandhi-family-are-actively-facilitating-hindutva-authoritarianism), by being electorally incompetent with BJP, even in places where BJP's governance was abysmal, and by allowing and encouraging the BJP to dictate the terms of the national political debate – by locating it in the past rather than in the present. 

- While upholding Hijab ban, Karnataka High Court [misconstrued several constitutional principles](https://thewire.in/law/karnataka-high-court-order-hijab-ban-constitutional-principles).

- Why most people [fail to speak up against authoritarian regimes](https://scroll.in/article/947953/why-most-people-fail-to-speak-up-against-authoritarian-regimes).

- Trump’s false or misleading [claims total 30,573 over 4 years](https://www.washingtonpost.com/politics/2021/01/24/trumps-false-or-misleading-claims-total-30573-over-four-years/).

- [Similarities between Modi and Indira Gandhi, who was a dictator](https://thewire.in/politics/ten-reasons-modi-is-just-like-indira-gandhi-and-thats-not-a-good-thing).

- [Oxygen’s in the air: Great expectations of good governance](https://www.thehindu.com/society/satire-oxygens-in-the-air-great-expectations-of-good-governance/article34504694.ece).

-  [Essentially flawed](https://www.thehindu.com/opinion/editorial/essentially-flawed-the-hindu-editorial-on-the-karnataka-high-courts-hijab-verdict/article65231490.ece): On the Karnataka High Court’s hijab verdict.

- [It’s time to scrap sedition law](https://www.deccanherald.com/opinion/first-edit/it-s-time-to-scrap-sedition-law-993528.html).

- [The Hindu Editorial](https://www.thehindu.com/opinion/editorial/overdue-review-the-hindu-editorial-on-the-sedition-law/article35372665.ece) on strucking down the sedition law(Section 124A).

- [The Indian Express Editorial](https://indianexpress.com/article/opinion/editorials/section-124a-supreme-court-sedition-chief-justice-n-v-ramana-7408463/): CJI Ramana’s observations questioning use of sedition law are enormously welcome, send strong message to government.

- Scrap the sedition law | [HT Editorial](https://www.hindustantimes.com/editorials/scrap-the-sedition-law-hteditorial-101614866925400.html).

- [Modi’s Raid Raj](https://thewire.in/politics/modis-raid-raj-janampatri-has-emerged-as-key-instrument-of-power-against-the-opposition): ‘Janampatri’ Has Emerged as Key Instrument of Power Against the Opposition.

- [It is important for free software to use free software infrastructure](https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html).

- [What I Want Non-Autistic People To Know by Emily](https://www.authenticallyemily.uk/blog/what-i-want-non-autistic-people-to-know). 

- Github(a website where people share their project's source code) [suspended accounts of Russian developers](https://www.bleepingcomputer.com/news/security/github-suspends-accounts-of-russian-devs-at-sanctioned-companies/) due to US sanctions on Russia. 

	This highlights the problem with centralized services and depending on a single country for all the software needs. The assumption that our country's interests will never go against the US interests is fundamentally flawed.

- [Why is Debian the most secure and trusted operating system](https://lists.debian.org/debian-security/2022/04/msg00047.html).

- [How the bulldozer has destroyed any hope Muslims might have in India’s institutions](https://scroll.in/article/1022607/how-the-bulldozer-has-destroyed-any-hope-muslims-might-have-in-indias-institutions).

- [BJP's winning strategy in polls is communal riots](https://thewire.in/politics/in-poll-bound-states-bjp-unfurls-its-winning-strategy-communal-riots).

- How the collapse of the BSP [marks the end of an old model of politics in Uttar Pradesh](https://scroll.in/article/1023637/how-the-collapse-of-the-bsp-marks-the-end-of-an-old-model-of-politics-in-uttar-pradesh).

- 19 May 2022: In the world's largest democracy, ['looking Muslim' could cost your life](https://edition.cnn.com/2022/05/19/opinions/indian-muslims-violence-hindu-nationalism-singh-abbas/index.html).

- Modi's biggest achievement is [institutionalization of lies](https://theprint.in/opinion/modi-govts-deepest-damage-isnt-hindi-muslim-divide-but-pms-mogic-dumbing-us-down/1006992/).

- Good arguments on [dangers of cryptocurrencies and the uselessness of blockchain](https://www.schneier.com/blog/archives/2022/06/on-the-dangers-of-cryptocurrencies-and-the-uselessness-of-blockchain.html). 

- Police in India [planted fake evidence](https://www.wired.com/story/modified-elephant-planted-evidence-hacking-police/) on activists' and human rights defenders' computers in order to arrest them.
