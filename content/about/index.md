---
title: "About me"
date: 2021-10-14
draft: false
---

So, you want to know about me?

My name is Ravi. I am just another human being like you. 

In general, I am a curious person. I am curious about many things.

I am from mathematics background. I like Topology/Geometry.

I am a believer of

- All software must be [free software](/free-software) where 'free' refers to freedom and not price.

- Everyone should [fear massive surveillance](https://socialcooling.com). In other words, everyone should care about their privacy.

- [Free Culture](https://en.wikipedia.org/wiki/Free-culture_movement).

- [Human Rights](https://en.wikipedia.org/wiki/Human_rights). 

- There is no evidence that god exists.
