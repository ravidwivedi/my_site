---
title: "WhatsApp is a malware"
date: 2021-07-29
---
Last Updated: Saturday 08 January 2022

Think of it: WhatsApp gives you a chatting service for free-of-cost. How are they earning money? Are they a charity? What are the hidden costs behind this free-of-cost service? You are trading away your rights like Freedom of Speech, privacy, freedom that every software user should get, and so on, just to use this free-of-cost service.

<figure style="float:right">
<a title="WhatsApp Inc., Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:No-whatsapp.png"><img width="100" height="100" alt="No-whatsapp" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/No-whatsapp.png/512px-No-whatsapp.png"></a>
<figcaption><b>Delete WhatsApp.</b>
</figcaption>
</figure>

WhatsApp is a malware, equivalent to a virus designed to mistreat you. The reasons are listed below. You can use Quicksy, [any XMPP app](https://xmpp.org/software/clients/) or [any Matrix app](https://matrix.org/clients) for chatting. [Check this post](/posts/chatting-apps) on why I recommend these chatting apps. Please avoid WhatsApp as much a possible. 

- WhatsApp is a [nonfree software](/posts/free-sw). A nonfree software is usually a [malware](https://gnu.org/malware) which mistreats its users. A nonfree/proprietary software controls the user and users don't have control over it. WhatsApp does not provide you the source code of the app and therefore you cannot trust the app that it really encrpyts the messages without [backdoors](/glossary/#backdoor) (Backdoor means WhatsApp or any third-party can get access to your system remotely and control it or take your data. Some examples of backdoors found in nonfree software are listed [here](https://www.gnu.org/proprietary/proprietary-back-doors.html).). Even if we assume that WhatsApp messages are [end-to-end encrypted](/glossary/#end-to-end-encryption), backdoors can selectively decrypt those messages when WhatsApp wants them to. A backdoor-like feature [was found in WhatsApp](https://techcrunch.com/2017/01/13/encrypted-messaging-platform-whatsapp-denies-backdoor-claim/) that can be used nullify its encryption.

	How do we know if WhatsApp doesn't keep a copy of private key(for all or some of its users)? How do we know that they don't turn off encryption for all or some selected users in an update? We don't know because we don't have the source code. This means that any trust on WhatsApp is a blind trust. 

-  WhatsApp makes [extensive use of outside contractors and artificial intelligence systems to examine user messages, images and videos; and turns over to law enforcement metadata including critical account and location information](https://www.propublica.org/article/how-facebook-undermines-privacy-protections-for-its-2-billion-whatsapp-users). This means it doesn't respect user privacy.

- The government also has [access to the data that WhatsApp collects](https://www.rollingstone.com/politics/politics-features/whatsapp-imessage-facebook-apple-fbi-privacy-1261816/).

- It is not sustainable for the whole world to depend on WhatsApp for their communications. It has been [down for hours](https://www.theverge.com/2021/10/4/22709575/facebook-outage-instagram-whatsapp) earlier, disrupting all the world communications. If there are many service providers (as there are in XMPP or Matrix), then outage of one server does not disrupt the communications of the whole world. This is one of the many problems with [centralized services](/glossary/#centralized-services).  

- WhatsApp requires a phone number to register. In many countries, it is required to link your SIM card to your identity, for example, by providing passport details or driving license. This is a privacy violation and can be used to identify users very easily.  

- WhatsApp is owned by Facebook, a data collection company whose business model [relies on recording people's lives](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy). It also [combines the data](https://www.theguardian.com/technology/2016/aug/25/whatsapp-backlash-facebook-data-privacy-users) it gets from WhatsApp with Facebook. Check [Social Cooling](https://socialcooling.com) if you are curious on why you should care about your digital privacy. 

- WhatsApp collects a lot of data on each user. For example, WhatsApp gets to know - what mobile phone you use (Hardware model), what Operating System you use, your time zone, your IP address through which your precise location can be tracked (unless you use VPN or Tor, which hides your IP address), your profile picture and status, your phone number, information about all your contacts which is saved in your WhatsApp, your device's battery percentage, network strength in your device, app version, browser information, connection information (including phone number, mobile operator or ISP), language, about and last seen. WhatsApp also collects information provided to WhatsApp by third party companies or apps. So, these are all the information which WhatsApp can collect from you and can give it to Facebook and even other third party companies. [Reference here](https://www.whatsapp.com/legal/updates/privacy-policy/?lang=en).

	Note that all this data collection is just a symptom of WhatsApp being proprietary software and users don't control it. If the software was free, then the users could have removed these malicious features.

- A good article on how [WhatsApp domesticates users](https://seirdy.one/2021/01/27/whatsapp-and-the-domestication-of-users.html). This is called [vendor lock-in](/glossary/#vendor-lock-in), which means that once you start using the service/product it is diffcult for you to switch to another similar service without substantial costs. In WhatsApp's case, the cost of switching is losing all the WhatsApp contacts. Any person switching from WhatsApp to another service needs to make effort to convince and get his other contacts switched to the new service. 

	This predatory behaviour of WhatsApp gives you another reason to uninstall it from your device. 

	If you care about freedom and switch to a freedom-respecting platform, your real friends will still be in touch via other methods. Other friends-- I am sure that you can live without them. 

- WhatsApp chat backups are stored [unencrypted in Google Drive](https://www.zdnet.com/article/whatsapp-warns-free-google-drive-backups-are-not-encrypted/) and [iCloud](https://faq.whatsapp.com/iphone/chats/how-to-back-up-to-icloud) (Visit the URL and note this line "Media and messages you back up aren't protected by WhatsApp end-to-end encryption while in iCloud.").

- On Android, if you force stop WhatsApp, it [automatically turns on within 30 minutes](https://stackoverflow.com/questions/11237064/how-does-whatsapp-service-gets-restarted-even-if-i-force-stop-app). This is a malware-like functionality.

- [“Deleted” WhatsApp messages are not entirely deleted](https://techcrunch.com/2016/07/29/research-shows-deleted-whatsapp-messages-arent-actually-deleted/). They can be recovered in various ways.

- WhatsApp has serious insecurities which [can be used to decrypt messages](https://arstechnica.com/information-technology/2014/02/crypto-weaknesses-in-whatsapp-the-kind-of-stuff-the-nsa-would-love/) sent using WhatsApp. 

- WhatsApp can ban its users anytime. [Over 2 million Indian accounts were banned by WhatsApp in 6 months](https://www.thehindu.com/sci-tech/technology/over-175-million-indian-accounts-banned-by-whatsapp-in-november-2021-compliance-report/article38091253.ece). WhatsApp having this power is wrong. Users should be allowed to run their own server and federate with WhatsApp, and therefore be allowed to set up their own rules. 

- WhatsApp sends [threat mail to people who develop independent bots](https://blog.learnlearn.in/2015/08/response-to-whatsapp-cease-and-desist-threat.html) for automating tasks on WhatsApp.
