---
title: "My Talks"
---
- 07 February 2022: [Importance of Free Software in Education](https://videos.fsci.in/videos/watch/1c6f69c4-0842-4112-9e7b-2df489505aea).

	Language: Hindi
	
	Recorded for [IT For Change](https://itforchange.net) to raise awareness about Free Software to teachers in Hindi langauge.

- 13 December 2021: [Interview published on LibreOffice blog](https://blog.documentfoundation.org/blog/2021/12/13/community-member-monday-ravi-dwivedi/). 

- 20 November 2021: [Freedom-respecting Software, Free Society](https://videos.fsci.in/videos/watch/0562cbc4-02bb-41a2-abfe-6ceb9994f2da?autoplay=1&auto_play=1&start=18s).

	At: [Department of Scientific Computing, modeling and simulation](https://scms.unipune.ac.in/), Savitribai Phule Pune University, Pune, India.

	Language: English.

- 07 November 2021: [Raising funds to fix matrix-xmpp bifrost bridge](https://videos.fsci.in/videos/watch/bb05da12-f780-4ba1-b38e-148681e815d7).

	Event: [FOSSHack 2021](https://fossunited.org/fosshack/2021).

	Language: English.	

	This talk was a part of Lightning talks by FOSSHack 2021. For the full event, please check [this URL](https://yewtu.be/watch?v=pOYsL_w2K_4). 

- 23- January-2021: [An Introduction to Free Software Communication Tools(Hindi)](https://laotzu.ftp.acc.umu.se/pub/debian-meetings/2021/MiniDebConf-India/20-an-introduction-to-free-software-communication-tools-hindi.webm) | [PeerTube Link](https://peertube.debian.social/w/6jV1N96sMgAhLhBpxXu6zX) | [Invidious Link](https://yewtu.be/watch?v=oH70znbaj6g)

	If you would like to check out other videos of MiniDebConf India 2021, then please visit [this URL](https://peertube.debian.social/video-channels/mdcoin2021/videos). Event: [MiniDebConf India 2021](https://in2021.mini.debconf.org/). 
- 23-January-2021: [BoF: Digital privacy in Indian context and some Free software](https://peertube.debian.social/videos/watch/417bd62c-cce1-494a-b96c-b0e39741cb09) (in Hindi). Event: [MiniDebConf India 2021](https://in2021.mini.debconf.org/). 
