---
title: "Problems with Signal messenger app"
date: 2022-01-22
draft: false
---
Last updated on: Tuesday 08 February 2022 to embed the Fediverse toot. 

[Signal](https://signal.org) is a chatting app popular for respecting user privacy. Here I discuss some problems with the Signal chat app and why I do not use it. It is good that Signal's app is [free/swatantra/mukt software](/free-software). But only software freedom is not enough for users to have full control over a chat system(software + service). 

An account on Fediverse put it nicely and succintly

<iframe src="https://mastodon.online/@FediFollows/107762703616261359/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe>

The main issue is that of centralization. Centralization means that the whole infrastructure is controlled by one entity. Signal Foundation gets to decide what app you can use, what service you can use, whether a cryptocurrency will be integrated with Signal app etc. This is a point of single failure. Signal might be good for now and have commitments towards user privacy, it can change in future at the whim of Signal Foundations. The users of Signal app are also locked into a single service provider. They have to accept whatever terms and conditions Signal service writes. It is like a dictatorship where users have no say over the decisions.

I list here the problems with Signal, which are mostly a consequence of centralization:

- The communications of the whole world get dependent on a single service provider, which is Signal Foundation.

- Every user has to give their phone number to sign up for the service. Remember that phone number is a very sensitive data and not every user might want to give that data to a service. Providing phone number should be a choice and not mandatory.

- The service can be compromised in the future, for example, by change in the leadership of Signal Foundation. Users cannot do anything about this. They either accept the changed service or leave it. The organization controlling the service can be sold to a different organization, change or even shut down the operation, etc.

- Users are locked into a single service provider. A bad consequence of getting locked into a service provider is if the terms or privacy policy changes in the future and users do not agree with it, then they have to leave but all their contacts are lost, so they would need to convince their contacts to switch as well. 

- It is easy to put backdoors in centralized services by governments. 

- Easy to ban by governments due to its centralized nature.

- Signal [forbids independent apps from connecting to their service](https://github.com/LibreSignal/LibreSignal/issues/37#issuecomment-217211165). This forces users to use only the Signal app developed by Signal Foundation for using the service.

Signal is intentionally a centralized service. They were offered a chance to federate with other providers, but chose not to.

If you want convenience that Signal, WhatsApp, Telegram provides, then you can use [Quicksy app](https://quicksy.im) for Android, which is compatible with any XMPP app such as Conversations app for Android, Snikket app for iOS, Dino for GNU/Linux, Gajim for Windows, Monal IM for Mac OS. Quicksy asks for user's phone number which give convenience, but the difference is that users have choice not to give their phone number and still be able to contact the Quicksy users. 
