---
title: "Issues with online payment"
date: 2021-09-29T01:41:01+05:30
---
Last Updated: 09 October 2021 

When we pay online (say, using Paypal) or using credit cards, debit cards, this identifies us in the transaction. Credit card companies like Visa, Mastercard also get this information. This puts users under surveillance (unless the transaction is under the user's name) and this is an injustice. These payment gateways also pose a threat to our freedom in many ways. They use their power to [censor](https://www.reuters.com/article/paypal-obscenity/paypal-sparks-furor-over-limits-on-obscene-e-books-idUSL2E8E7DJM20120307), [discriminate](https://www.theatlantic.com/technology/archive/2011/07/visa-says-its-still-not-processing-transactions-wikileaks/352596/), spy on users.   

We need online payments which respect users' freedom - do not spy on them and from which nobody can be excluded under any conditions, just as cash does not spy on people and does not exclude anyone to use it. Also, obviously, there should not be any requirement to use proprietary software for the payments. The payment system must be accessible through [free software](/posts/free-sw). 

- Paytm [shared user's data with Indian government](https://thewire.in/media/cobrapost-expose-top-paytm-official-claims-that-firm-was-asked-to-share-user-data-with-pmo).

- Paypal [shut payments to Wikileaks](https://www.theguardian.com/media/2010/dec/04/paypal-shuts-down-wikileaks-account). These intermediaries are not neutral. They can cut off access to things they don't like or pressured into cutting off by the government. 

- AMEX [lowered a person’s credit limit](https://consumerist.com/2008/12/22/amex-lowers-your-credit-limit-if-you-shop-where-deadbeats-shop/) because they shopped at Walmart and AMEX didn’t like that.

- [Visa and Mastercard are Trying to Dictate What You Can Watch on Pornhub](https://www.eff.org/deeplinks/2020/12/visa-and-mastercard-are-trying-dictate-what-you-can-watch-pornhub).

- Paypal is [denying service to Palestinians](https://www.eff.org/deeplinks/2021/10/why-paypal-denying-service-palestinians). An online payment system ought to be neutral like cash. Paypal uses its power to exclude groups it does not like.

- [Paypal censored erotic literature](http://www.zdnet.com/article/paypal-strong-arms-indie-ebook-publishers-over-erotic-content/).

- Paypal [froze account on a Minecraft developer without explanation](https://www.rockpapershotgun.com/paypal-freezes-minecraft-devs-600k-euros).

- PayPal [blocked the account of a Russian human rights organisation](globalvoicesonline.org/2015/05/15/paypal-blocks-donations-for-printing-boris-nemtsovs-ukraine-war-report/), which supported political prisoners arrested at Bolotnaya Square.

- Paypal [shuts accounts](https://libredd.it/r/WTF/comments/ejzfp/paypal_shut_my_account_today_because_my_business/) whose business donates to wikileaks. 

- PayPal [froze $45,000 of community donations](https://web.archive.org/web/20111020123552/http://blog.diasporafoundation.org:80/2011/10/19/how-diaspora-found-its-tiger-stripe-in-the-midst-of-a-paypal-fiasco.html) to the Diaspora project without any reason.

- PayPal [blocks payment](http://www.cityofsound.com/blog/2013/07/paypal-and-the-word-iranian.html) for books that have "Iran" in their name.

- Mastercard [took users' personal data](https://www.theage.com.au/technology/mastercard-to-access-facebook-user-data-20141006-10qrqy.html) from Facebook.   
