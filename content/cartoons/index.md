---
title: "Cartoons"
date: 2022-02-11
---
<figure>
<img src="/images/how-to-wrap.png" width="200" height="200">
<figcaption>
Taken from: https://edwardsnowden.substack.com/p/all-seeing-i 
<br>
I don't know the copyright license of this image.
</figcaption>
</figure>

<br>
<figure>
<img src="/images/suffer-under-capitalism.jpeg" width="200" height="200">
<figcaption>
Taken from:<a href="https://assets.merveilles.town/media_attachments/files/107/576/744/248/803/266/original/7a1c7e207d98b659.jpeg">This Mastodon post</a>. 
<br>
I don't know the copyright license of this image.
</figcaption>
</figure>

<br>
<figure>
<img src="/images/answers.jpg">
<figcaption>
The world’s solution to almost every complex issue these days
<br>
I don't know the copyright license of this image.
<br>
Source: <a href="https://poddery.com/posts/6629022">poddery</a>
</figcaption>
</figure>
