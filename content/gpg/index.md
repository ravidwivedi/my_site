---
title: "GPG Keys"
date: 2022-06-14
---
Check out an [introduction to gpg](https://wiki.archlinux.org/title/GnuPG#Create_a_key_pair) if you are not sure what gpg keys are.

Text file of my public GPG keys are [here](/files/ravidwivedi.asc). Creation date: 14-June-2022.

To import my key from a key server, use the following command:

`gpg --recv-keys 5E9F47BE14DD8BE6`

My key fingerprint is: 

<span style="color:pink">AFCA 169F 18B6 8814 ABE6  102A 5E9F 47BE 14DD 8BE6 </span>
