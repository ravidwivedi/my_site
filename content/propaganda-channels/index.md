---
title: "List of propaganda channels in India"
---
These are news channels fueled by government propaganda, fake news and communal poison. A lot of them were [uncovered by a string operation by CobraPost](https://thewire.in/media/large-media-houses-seen-striking-deals-for-paid-news-to-promote-hindutva-agenda).

- Zee News

- Aaj Tak

- India TV

- News 18 India

- [Dainik Jagran](http://cobrapost.com/blog/Dainik-Jagran/1011).

- [Hindi Khabar](http://cobrapost.com/blog/Hindi-Khabar/1012).

- [SAB Group](http://cobrapost.com/blog/SAB-Group/1013).

- [DNA](http://cobrapost.com/blog/DNA/1014).

- [Amar Ujala](http://cobrapost.com/blog/Amar-Ujala/1015).

- [UNI](http://cobrapost.com/blog/UNI/1017).

- [9X Tashan](https://thewire.in/235304/statement-about-time-indian-science-woke-up-to-shifting-social-realities/).

- [Samachar Plus](http://cobrapost.com/blog/Samachar-Plus/1019).

- [HNN 24x7](http://cobrapost.com/blog/HNN-24%C3%977/1020).

- [Punjab Kesari](http://cobrapost.com/blog/Punjab-Kesari/1021).

- [Swatantra Bharat](http://cobrapost.com/blog/Swatantra-Bharat/1023).

- [ScoopWhopp](http://cobrapost.com/blog/Scoop-Whoop/1024).

- [Rediff](http://cobrapost.com/blog/Rediff-Com/1025).

- [Aaj](http://cobrapost.com/blog/Aj-Hindi-Daily/1027).

- [Sadhna Prime News](http://cobrapost.com/blog/Sadhna-Prime-News/1028).
