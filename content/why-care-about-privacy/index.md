---
title: "Why Care About Privacy"
date: 2021-07-28T15:31:45+05:30
---

Last updated: Tuesday 11 January 2022

I would like to start with [a quote by Eben Moglen](http://news.bbc.co.uk/2/hi/technology/8508814.stm), "How much would someone have to pay before you would let them read your diaries, find out what your religious beliefs, political leanings or sexual preferences were, or where your children go to school?

What many of us are not aware of is that we are freely giving away exactly this kind of information to websites that we use every day. More importantly, we are doing this on a massive scale."

What is privacy? Privacy means control and choice over whom to tell and what to tell.

Let me put the frog-in-the-boiling-pot analogy: When a frog is suddenly put into boiling water, it feels the heat, realizes it immediately and run away, but if the frog is put in warm water which is warmed slowly, the frog does not perceive the danger and boiled to death. 

Similar is the situation with us. Gradual changes in massive surveillance all over the world have made people comfortable with surveillance. From the frog analogy, people do not perceive gradual threats and do not react to them. Things like how Facebook changed over time gradually. People became comfortable with Facebook and locked into it. Majority of people do not bother to react to a new shitty news about Facebook. 

[Quoting Riseup](https://riseup.net/en/security#why-security-matters), "The increasing importance of information and communication has brought with it another phenomenon: the rise of a surveillance society. You can think of surveillance as an attempt by the powerful to maintain their dominance by asserting control over communication."

Our privacy and dignity are under attack due to constant massive surveillance. I present here some adverse affects of massive surveillance. Also, massive surveillance effects all of us (maybe in different ways).

Rather than waiting for government to pass privacy laws, my advice is to not expect too much from law enforcement for our privacy. History illustrates very well that governments like surveillance on its citizens because that gives them more power. We can avoid privacy-invading technologies for our daily use as much as possible rather than wait for privacy laws to be passed. Corporate surveillance is no less dangerous than government surveillance.  

<figure>
<img src="/images/govt-enc.png" width="300" height="200">
<figcaption><a href="https://mastodon.online/@madargon/105974184596632632">Credits</a></figcaption>
</figure>


And these days, governments can easily spy on us through our devices, such as mobile phones. If you think that government surveillance is used only to catch criminals, then think again. Do you think government cannot be corrupt? Do you think this database of data collection cannot be abused in future? Governments like power and surveillance gives them power. We need to care about our own privacy so that the government doesn't get data about us. Some people argue that privacy laws should be made and I do support that, but we don't have to wait for privacy laws. We can reject privacy-invading technologies like [malware by Google, Facebook, Amazon, Apple, Microsoft etc](https://www.gnu.org/proprietary/proprietary-surveillance.html), rather than waiting for some government to make laws or companies to protect your privacy. Do not fall for convenience as it is a terribly foolish practice to trade your privacy for convenience. 

I myself go to great amount of inconvenience to protect my privacy. You can do it too.  

I would like to quote a [Mastodon Toot](https://chitter.xyz/@faoluin/107463008001608324) on this 'nothing to hide' argument: "Governments can change on a dime. A more tolerant regime could be replaced at any time by an intolerant one, whether they're democratically elected or not. Now all of a sudden you *do* have something to hide."

I suggest you to watch [Richard Stallman's interview](https://yewtu.be/watch?v=COoRi3g-tyw) (26 minutes duration) by Sophie.

Another article worth reading is [How Much Surveillance Can Democracy Withstand?](https://www.gnu.org/philosophy/surveillance-vs-democracy.en.html) by Richard Stallman. 

<figure>
<img src="/images/someone-watching-you.png" width="150" height="150">
</figure>

- [Inhibiting effects](https://www.socialcooling.com/) of knowing that someone is watching.

- [The Eternal Value of Privacy](https://www.schneier.com/essays/archives/2006/05/the_eternal_value_of.html).

- CCTV cameras in India recorded violence against mosques, [but police is not investigating them](https://scroll.in/article/1023257/cctvs-recorded-violence-against-mosques-during-navratri-but-are-the-police-watching-it). This shows how the argument that the surveillance is for catching criminals falls apart. 

- [Statistics show](https://theintercept.com/2016/04/28/new-study-shows-mass-surveillance-breeds-meekness-fear-and-self-censorship/) that mere existence of a surveillance state breeds fear and conformity and stifles free expression.

- Cambridge Analytica created psychological profiles on all Americans to try and [dissuade people from voting](http://adage.com/article/media/broke-facebook-cambridge-analytica-whistleblower/312791/).

- Google [handed over the document](https://www.indiatoday.in/technology/news/story/disha-ravi-arrest-puts-privacy-of-all-google-india-users-in-doubt-1769772-2021-02-16) shared by Disha Ravi stored on Google Docs with the Indian law enforcement. This shows that Indian government also have access to all the data that the surveillance companies like Google, Facebook collect.

- Massive surveillance has [adverse effects on mental health](https://theprint.in/theprint-valuead-initiative/surveillance-a-massive-challenge-for-mental-health-so-india-needs-robust-data-protection-laws/512430/). These include insomnia, social anxiety, depression, hypervigilance, specific situational type phobias, post-traumatic stress disorder (PTSD), agoraphobia, obsessive-compulsive disorder (OCD), and many other issues. Further, surveillance affects those in vulnerable communities and lower-income groups far more simply because of the lack of resources and awareness of their rights, but most importantly, where there is a limited expectation of political accountability and transparency.

- Indian government [has passed draconian IT rules](https://fsci.in/blog/fsci-challenges-intermediary-rules-2021/) in February 2021.

- Andhra Pradesh and Telangana are [building citizen's profiles using Aadhar details](https://www.hindustantimes.com/india-news/despite-govt-denials-states-building-databases-for-360-degree-profiles-of-citizens/story-qnSLHGyZIXiZiO4ce84UuO.html). 

- Election commission of India is using [data from Census records and GPS to track voters](https://www.rediff.com/news/2008/aug/25ec.htm).

- You may not get that dream job if your [data suggests](https://www.theguardian.com/science/2016/sep/01/how-algorithms-rule-our-working-lives) you're not a very positive person.

- Pakistan's [biometric national ID database illustrates how such databases are dangerous](https://www.codastory.com/authoritarian-tech/pakistan-biometric-identification-nadra/). It can cancel the citizenship of many generations and is also used for repression and bigotry. 

	Remember that India's Aadhar card is also a biometric national ID database.  

- [AMEX lowered a person's credit limit](https://consumerist.com/2008/12/22/amex-lowers-your-credit-limit-if-you-shop-where-deadbeats-shop/) because they shopped at Walmart and AMEX didn't like that. 

- FBI conducted a series of covert and illegal projects [to spy on organizations and activists they deemed subversive](https://en.wikipedia.org/wiki/COINTELPRO). Governments like surveillance. And these days, governments can easily spy on us through our devices like mobile phones.  

- China forces Uighurs outside China to [submit to surveillance](https://www.thedailybeast.com/chinese-police-are-spying-on-uighurson-american-soil) and compels them by threatening to punish their relatives at home. A good example of how surveillance is a preparatory step for repression. 

- Some Indian states have [installed CCTV cameras with facial recognition technology using biometrics from the Aadhar card](https://web.archive.org/web/20180202190150/http://tunein23.com/EN/?p=2757).

- Some governments have [leaked personal data of citizens who express dissenting opinions](http://www.slate.com/blogs/the_slatest/2017/07/15/white_house_publishes_names_emails_phone_numbers_home_addresses_of_critics.html), effectively discouraging people from exercising their freedom of speech. 

- Digital reputation systems are limiting our ability and our will to protest injustice.

- In China each adult citizen is getting a government mandated "[social credit score](http://www.bbc.com/news/world-asia-china-34592186)". This represents how well behaved they are, and is based on crime records, what they say on social media, what they buy, and even the scores of their friends.

If you have a low score you can't get a government job, visa, cheap loan, or even a nice online date.

Social pressure is the most powerful and most subtle form of control.

- Snowden's quote to argue against "I don't have anything to hide" argument:

<br>
"But saying that you don't need or want privacy because you have nothing to hide is to assume that no one should have, or could have, to hide anything -- including their immigration status, unemployment history, financial history, and health records. You're assuming that no one, including yourself, might object to revealing to anyone information about their religious beliefs, political affiliations, and sexual activities, as casually as some choose to reveal their movie and music tastes and reading preferences."

 --  Edward Snowden

- [How Much Surveillance Can Democracy Withstand?](https://www.gnu.org/philosophy/surveillance-vs-democracy.en.html)

- US Government [used location data from a prayer app](https://forum.fairphone.com/t/how-a-muslim-prayer-app-was-used-for-drone-operations/71711) for drone strikes.

- The Egyptian government [tracked opponents and activists through phone apps](https://www.theregister.co.uk/2019/10/04/egypt_smartphone_spying/).

- A person tweeted on Twitter : “Free this week, for quick gossip/prep before I go and destroy America.”  His comment wasn’t serious, but [he was questioned for five hours and then sent back home](https://www.bbc.com/news/technology-16810312). 


- Ex-Facebook workers admitted that Facebook's algorithms [were intenionally used to supress conservative news in Facebook feeds](https://gizmodo.com/former-facebook-workers-we-routinely-suppressed-conser-1775461006).

- Are you listening to sad songs on spotify a lot? [It could limit your job opportunities](https://betanews.com/2016/07/22/spotify-sells-user-data-to-advertisers/).

- Some writers have [started self-censoring due to massive surveillance](https://www.nytimes.com/2015/01/05/arts/writers-say-they-feel-censored-by-surveillance.html?_r=0). They have avoided, or have considered avoiding, controversial topics in their work or in personal communications as a result. This tramples their freedom of speech. 

- Columbia University law professor [Eben Moglen wrote](https://www.theguardian.com/technology/2014/may/27/-sp-privacy-under-attack-nsa-files-revealed-new-threats-democracy) that “omnipresent invasive listening creates fear. And that fear is the enemy of reasoned, ordered liberty.” Surveillance is a tactic of intimidation.

- Massive surveillance has [chilling effects](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1002145) on internet use American muslims and by groups like environmentalists, gun-rights activists, drug policy advocates, and human rights workers.

- According to a [Human Rights Watch report](https://www.hrw.org/sites/default/files/reports/usnsa0714_ForUPload_0.pdf), journalists covering stories on the intelligence community, national security, and law enforcement have been significantly hampered by government surveillance. Sources are less likely to contact them, and they themselves are worried about being prosecuted. Human Rights Watch concludes that stories in the national interest that need to be reported don’t get reported, and that the public is less informed as a result.

- [A quote by Bruce Schneier](https://www.schneier.com/blog/archives/2016/03/mass_surveillan_1.html) - "Across the US, states are on the verge of reversing decades-old laws about homosexual relationships and marijuana use. If the old laws could have been perfectly enforced through surveillance, society would never have reached the point where the majority of citizens thought those things were okay. There has to be a period where they are still illegal yet increasingly tolerated, so that people can look around and say, “You know, that wasn’t so bad.” Yes, the process takes decades, but it’s a process that can’t happen without lawbreaking........We need imperfect security­ — systems that free people to try new things, much the way off-the-record brainstorming sessions loosen inhibitions and foster creativity. If we don’t have that, we can’t slowly move from a thing’s being illegal and not okay, to illegal and not sure, to illegal and probably okay, and finally to legal.

This is an important point. Freedoms we now take for granted were often at one time viewed as threatening or even criminal by the past power structure. Those changes might never have happened if the authorities had been able to achieve social control through surveillance."

- Politicians have used surveillance to intimidate the opposition and harass people who annoy them. One example is from 2014: police in New Jersey [routinely photographed protesters at events](https://www.nj.com/politics/2014/03/attorney_general_to_state_police_stop_photographing_protesters_at_chris_christie_town_halls.html) hosted by Governor Chris Christie until the state attorney general ordered them to stop.  

- Snowden's documents show that [NSA employees routinely pass around intercepted nude photos](https://arstechnica.com/tech-policy/2014/07/snowden-nsa-employees-routinely-pass-around-intercepted-nude-photos/).

- NSA employees use their eavesdropping power to [spy on their love interests](https://www.foxnews.com/politics/nsa-officers-sometimes-spy-on-love-interests).

- US has spied on the [Occupy movement](http://www.nytimes.com/2012/12/25/nyregion/occupy-movement-was-investigated-by-fbi-counterterrorism-agents-records-show.html), [pro- and anti-abortion activists](http://www.nytimes.com/2009/12/17/us/17disclose.html), [peace activists](https://www.aclu.org/national-security/aclu-uncovers-fbi-surveillance-maine-peace-activists), and [other political protesters](https://www.aclu.org/files/assets/Spyfiles_2_0.pdf). 

- NSA has been [collecting data on the porn-viewing habits of Muslim “radicalizers”](https://www.huffpost.com/entry/nsa-porn-muslims_n_4346128), who through political speech might radicalize others- with the idea of blackmailing them. 

- A debt collection agency worked for a number of hospitals in Minnesota which was in charge of billing and collection for those hospitals, but it also coordinated scheduling, admissions, care plans, and duration of hospital stays. The agency [collected extensive patient data and used it for its own purposes](https://www.startribune.com/accretive-banned-from-minnesota-for-at-least-2-years-to-pay-2-5m/164313776/), without disclosing to patients the nature of its involvement in their healthcare. It used information about patient debts when scheduling treatment and harassed patients for money in emergency rooms. 

This goes to show how easy it is for our data to be abused. 

- Data that companies get through surveillance [is used to discriminate](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2325784). 

- Companies use data to learn everything possible about you as an individual and [deny service or charge more for products to particular groups based on race, sex, or where they lived](https://www.theatlantic.com/business/archive/2014/03/redlining-for-the-21st-century/284235/).i

-  A website  highlighted different prices for hotel rooms [depending on whether viewers were using Mac or Windows](https://www.mdgadvertising.com/marketing-insights/on-orbitz-mac-users-steered-pricier-hotels/).

- Amercian Express [punish people by lowering their credits](https://cnszu.com/card-companies-adjusting-credit-limits/) for the shopping habits it doesn't like.

- Google's search engine can [manipulate voters’ preferences substantially  without their awareness](https://aibrt.org/downloads/EPSTEIN_and_Robertson_2013-Democracy_at_Risk-APS-summary-5-13.pdf).

- Social media platforms [can easily manipulate public opinion](https://harvardlawreview.org/2014/06/engineering-an-election/). They can amplify the voices of people they agree with and dampen others. [China government already does this](https://www.voanews.com/east-asia-pacific/who-are-chinese-trolls-50-cent-army) by hiring people to post supporting comments in favor of the government and to challenge comments which opposes the Chinese government. Samsung also[ hired people to post negative comments](https://www.bbc.com/news/technology-22166606) about phones made by Taiwan's HTC.

-  Facebook Helped Advertisers [Target Teens](https://arstechnica.com/business/2017/05/facebook-helped-advertisers-target-teens-who-feel-worthless/) Who Feel "Worthless".

Heavy use of Facebook [tends to make some people feel worthless](https://stallman.org/facebook.html#exploitation). 

- Police aerial surveillance [endangers our ability to protest](https://www.eff.org/deeplinks/2021/11/police-aerial-surveillance-endangers-our-ability-protest).
