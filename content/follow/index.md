---
title: "Follow this blog"
date: false
---
- To follow this website via RSS Feeds, just copy the URL https://ravidwivedi.in/index.xml in an RSS Feed Reader like Thunderbird. [Here](https://ncase.me/rss/) is a guide to RSS Feeds and why prefer them over newsletters.  

- Follow me [on Mastodon](https://masto.sahilister.in/@ravi). If you are not already using Mastodon, then [join it](https://joinmastodon.org/) or the larger family known as [Fediverse](https://jointhefedi.com/). Refer to [this guide](https://instances.social/) to choose suitable Mastodon service for you.

- Follow me [on diaspora](https://poddery.com/u/ravi).
