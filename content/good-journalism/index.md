---
title: "Support good journalism"
date: 2022-02-18
draft: false
---
I list here some Indian news websites or journalist channels and appeal to support them financially. 

### Criteria to be in the list:

- No communal poison. 

- Committed to reporting the truth even if it goes against the government or corporates.

- Not backed up or funded by the government.

- Accessible via the Tor Network.

### List

- [The Wire](https://thewire.in/). 

- [The Hindu](https://www.thehindu.com/).

- [The Scroll](https://scroll.in/).

- [Feminism in India](https://feminisminindia.com/).

- [Newslaundry](https://www.newslaundry.com).

- [Kashmir Walla](https://thekashmirwalla.com).

- [The Quint](https://www.thequint.com/).

- [Article 14](https://www.article-14.com/).

- [Alt News](https://www.altnews.in/): Alt News is a fact checker website, which debunks fake news.

- [NDTV](https://ndtv.com), especially Prime Time at 9PM by [Ravish Kumar](https://en.wikipedia.org/wiki/Ravish_Kumar).

- [HW News](https://hwnews.in/).

- [Telegraph India](https://www.telegraphindia.com/).

- [The News Minute](https://www.thenewsminute.com/).

- [Down to Earth](https://www.downtoearth.org/).

### Non-Indian Media

- [BBC](https://bbc.com): It has English as well as regional languages too.

- [The Guardian](https://www.theguardian.com/).

- [Wired](http://wired.com/).

- [The New York Times](https://www.nytimes.com/).

- [Common Dreams](https://www.commondreams.org/).

- [CNN](https://www.cnn.com/).

### Magazines

- [Caravan Magazine](https://caravanmagazine.in/).

- [India Today](https://www.indiatoday.in/).

- Tehelka.

### Human Rights Organizations

- [Amnesty](https://www.amnesty.org).

- [Human Rights Watch](https://www.hrw.org/).

### YouTube Channels

Rather than watching these channels on YouTube, watch them on Newpipe or FreeTube app.

- [Dhruv Rathee](https://yewtu.be/channel/UC-CSyyi47VX1lD9zyeABW3w/videos).

- [The Deshbhakt](https://yewtu.be/channel/UCmTM_hPCeckqN3cPWtYZZcg).

- [Unscripted](https://yewtu.be/channel/UC6JEz6BKg7hX7idKecN7QYA).

- [Official PeeingHuman](https://yewtu.be/search?q=peeing+human).

- [Lallantop](https://www.thelallantop.com).

### Removed From The List

These were listed earlier but removed from the list:

- Dainik Bhaskar for publishing [this column](https://www.bhaskar.com/opinion/news/chetan-bhagats-column-hindus-are-the-majority-in-india-not-in-the-world-why-hesitate-to-talk-about-the-plight-of-hindus-129551092.html) by Chetan Bhagat mocking people who are believing that Kashmir Files is a propaganda movie.

- The Print for [this tweet](https://nitter.1d4.us/shekhargupta/status/1251381206827950081).
