---
title: "Flyers/Posters"
date: 2021-10-30
draft: false
---

Please share these flyers and posters with your friends, contacts, groups to raise awareness. 

<figure>
<a title="DSFGS https://activism.openworlds.info/@dsfgs, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Fediflyer-cropped-2.jpg"><img width="512" alt="Fediflyer-cropped-2" src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Fediflyer-cropped-2.jpg/512px-Fediflyer-cropped-2.jpg"></a>
<figcaption> DESIGN : DSFGS <a href="https://activism.openworlds.info/@dsfgs"> activism.openworlds.info/@dsfgs</a>
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure>


<figure>
<a title="DSFGS https://activism.openworlds.info/@dsfgs, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Fediflyer-cropped-1.jpg"><img width="512" alt="Fediflyer-cropped-1" src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Fediflyer-cropped-1.jpg/512px-Fediflyer-cropped-1.jpg"></a>
<figcaption> DESIGN : DSFGS <a href="https://activism.openworlds.info/@dsfgs"> activism.openworlds.info/@dsfgs</a>
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure>

<figure>
<a title="LibreOffice., CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Libreoffice-flyer-for-universities-modified-by-ravi.png"><img width="256" alt="Libreoffice-flyer-for-universities-modified-by-ravi" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/Libreoffice-flyer-for-universities-modified-by-ravi.png/256px-Libreoffice-flyer-for-universities-modified-by-ravi.png"></a>
<figcaption>  
Credits: LibreOffice. 
<br>
Source: https://wiki.documentfoundation.org/File:Schools_universities_flyer.odg
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/3.0/deed.en">Creative Commons Attribution-Share Alike 3.0 Unported license</a>
 </figcaption>
</figure>

<figure>
<a title="Riya Sawant. Free Software Foundation of India., CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Instant-messengers-poster.png"><img width="256" alt="Instant-messengers-poster" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/Instant-messengers-poster.png/256px-Instant-messengers-poster.png"></a>
<figcaption>  
Credits: Riya Sawant 
<br>
Source: https://fsf.org.in/assets/img/ims.png 
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure>

<br>

<figure>
<img src= "/images/Printed_vs_DRM_ebooks-1.png" width="300" height="300" >
<figcaption>  
Credits: Free Software Foundation 
<br>
Source: https://www.defectivebydesign.org/printable
<br>
License: Not clearly indicated.  
</figcaption>
</figure>

<figure>
<figcaption>  
<a title="Jeison Yehuda Amihud, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:4-freedoms-poster.png"><img width="256" alt="4-freedoms-poster" src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/4-freedoms-poster.png/256px-4-freedoms-poster.png"></a>
<br>
Credits: Jeison Yehuda Amihud
<br>
Source: https://www.gnu.org/graphics/amihud-4-freedoms.html 
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure>

<br>
<figure>
<figcaption>  
<br>
<a title="April, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Open-formats.jpg"><img width="512" alt="Open-formats" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b5/Open-formats.jpg/512px-Open-formats.jpg"></a>
Author: April
<br>
Source: https://fsfe.org/contribute/spreadtheword.en.html 
<br>
Source Files: <a href="https://download.fsfe.org/advocacy/promomaterial/DFD/">Here</a>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure>

<br>
<figure>
<figcaption>
<br>
<a title="Free Software Foundation, Europe https://fsfe.org., CC BY 4.0 &lt;https://creativecommons.org/licenses/by/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Tired-of-replacing-smartphone-every-year.png"><img width="512" alt="Tired-of-replacing-smartphone-every-year" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6a/Tired-of-replacing-smartphone-every-year.png/512px-Tired-of-replacing-smartphone-every-year.png"></a>
<br>
How to upcycle your phone and extend its usage lifetime
<br>
Author: Free Software Foundation Europe
<br>
Source: https://fsfe.org/activities/upcyclingandroid/informationmaterial.en.html#head 
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure>

<br>
<figure>
<figcaption>
<br>
<a title="Free Software Foundation, Europe https://fsfe.org., CC BY 4.0 &lt;https://creativecommons.org/licenses/by/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Phone-production-effect-on-environment.png"><img width="512" alt="Phone-production-effect-on-environment" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Phone-production-effect-on-environment.png/512px-Phone-production-effect-on-environment.png"></a>
<br>
How does phone production affect the environment? 
<br>
Author: Free Software Foundation Europe
<br>
Source: https://fsfe.org/activities/upcyclingandroid/informationmaterial.en.html#head 
<br>
LICENSE: <a href= "https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
 </figcaption>
</figure>
