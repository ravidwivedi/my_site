---
title: "Quotes from the Books I read"
date: 2022-01-08
---
I have modified the quotes for gender-neutrality.

- “I don’t know what’s worse: to not know what you are and be happy, or to become what you’ve always wanted to be, and feel alone.” - Daniel Keyes, Flowers for Algernon 

- "The true measurement of a person's worth isn't what they say they believe in, but what they do in defense of those beliefs. If you're not acting on your beliefs, then they probably aren't real" : Snowden , No Place to Hide

- "You can only be jealous of someone who has something you think you ought to have yourself." - Margaret Atwood, The Handmaid's Tale 

- "Ignoring isn’t the same as ignorance, you have to work at it." - Margaret Atwood, The Handmaid's Tale 

- "Beware of the person who works hard to learn something, learns it, and finds themselves no wiser than before." - Kurt Vonnegut, Cat's Cradle 

- "Advertising has us chasing cars and clothes, working jobs we hate so we can buy shit we don't need." - Chuck Palahniuk, Fight Club 

- "The things you own end up owning you. It's only after you lose everything that you're free to do anything."	- Chuck Palahniuk, Fight Club 
