---
title: "Don't buy Apple devices"
date: 2021-10-24
draft: false
---
Last Updated: Wednesday 08 December 2021

Apple is the most unethical tech company. It is bad in every aspect-- be it software, hardware, right to repair, saving the environment, treating workers, producing devices in sweatshops, privacy of users, tax practices. Apple's practices hurt users, workers, environment, economy and the society as a whole.  The only way to get freedom in Apple devices is not to use them entirely. Please don't listen to Apple when they say that they are concerned about your privacy and security. They are not. They restrict you for their own profits. 

When buying an Apple device, [you are buying a jail for yourself](https://www.gnu.org/proprietary/malware-apple.html#jails). 

What can we do about this? We can stop buying from Apple and then raise awareness about the mistreatment that Apple does to its users and workers. 

Cory Doctrow wrote on [how the Apple's locked system goes against you](https://pluralistic.net/2021/06/08/leona-helmsley-was-a-pioneer/#manorialism). 

### Malware

Apple's software is [malware](https://www.gnu.org/proprietary/malware-apple.en.html). The software that Apple develops is [nonfree/proprietary software](/posts/free-sw) and therefore users do not control it. Only Apple controls this software. This means users cannot inspect, study, modify or share the software. Apple uses this power over their users to introduce malfunctionalities into the software to spy on them and abuse them in various ways.   

#### Surveillance

Don't get fooled by Apple's empty privacy promises. It is a popular mistake that while Google and Facebook are ad companies and collect user data, Apple is different. Apple is not different. It also collects as much data it can and [it is also an ad company](https://www.apple.com/legal/privacy/data/en/apple-advertising/). 

- Apple [collects a lot of data on each user through iPhones](https://www.apple.com/legal/privacy/en-ww/). This includes-- location, IP Address, network information, bluetooth information, connected devices, accessories, personal demographics, browsing history, browser fingerprint, device fingerprint, search history, app data, usage data, performance, diagnostics, product interaction, transaction details, payment records, purchasing records, contacts, social network/graph, watching history, music/podcasts data, reading lists, call metadata, device information, messaging metadata, email addresses, salary, income, assets, health data, ad interactions, in-app purchases, in-app subscriptions, app downloads, music/movies/TV shows downloads, Apple ID, IDFA, Random Unique ID, UUID, IMEI, Hardware Serial Number, SIM serial number, phone number, telemetry, cookies, Nearby WiFi MAC address, Siri request history, web sign-in, songs played, play and pause times, playlists, engagement, library. 

- [Apple’s plan to scan images will allow governments into smartphones](https://www.theguardian.com/commentisfree/2021/oct/16/apples-plan-to-scan-images-will-allow-governments-into-smartphones).

- Apple plans to push a new and uniquely intrusive surveillance system which puts every [iPhone users' right to privacy, freedom of speech, liberty in danger](https://edwardsnowden.substack.com/p/all-seeing-i).

- [Apple's privacy policy](https://web.archive.org/web/20211118054658/https://www.apple.com/legal/privacy/pdfs/apple-privacy-policy-en-ww.pdf) says "Apple may rely on your consent or the fact that the processing is necessary to fulfill a contract with you, protect your vital interests or those of other persons, or to comply with law. We may also process your personal data where we believe it is in our or others’ legitimate interests", which says they collect data on each user and can be used for any purpose by Apple. 

- Massive surveillance disclosure documents revealed by Edward Snowden show that [Apple is a part of U.S. government spying program named PRISM](https://www.theguardian.com/world/2013/jun/06/us-tech-giants-nsa-data).

- A whistleblower working inside Apple reported that they were assigned to [listen to the recordings received from Apple devices in France and correct the transcriptions of Apple’s vocal assistant (Siri)](https://www.politico.eu/wp-content/uploads/2020/05/Public-Statement-Siri-recordings-TLB.pdf). So, Siri listens to you all the time and you are being spied. 

- Apple [announced that it will scan all the photos](https://web.archive.org/web/20210815183624/https://www.nytimes.com/2021/08/05/technology/apple-iphones-privacy.html) in Apple devices in the name of curbing child abuse. This can be used for other sorts of surveillance.

-  Apple's browser Safari [sends data to Chinese company Tencent](https://blog.cryptographyengineering.com/2019/10/13/dear-apple-safe-browsing-might-not-be-that-safe/).

- Apple [sends user's call history logs](https://theintercept.com/2016/11/17/iphones-secretly-send-call-history-to-apple-security-firm-says/) to its servers. The logs surreptitiously uploaded to Apple contain a list of all calls made and received on an iOS device, complete with phone numbers, dates and times, and duration. They also include missed and bypassed calls.

- The new version of MacOS [informs Apple of every time the Macbook launches an app](https://sneak.berlin/20201112/your-computer-isnt-yours/).

- Apple's new MacOS [does not allow users to launch apps not written by Apple itself](https://www.eff.org/deeplinks/2020/11/macos-leaks-application-usage-forces-apple-make-hard-decisions). This was caused due to Apple's server being down. Each time an app is launched in MacOS, it sends data to Apple for verification if the app is okay to launch. [Apple is doing this from 2018](https://lapcatsoftware.com/articles/notarization-privacy.html). 

- [Apple had a secret data sharing deal with Facebook](nytimes.com/interactive/2018/06/03/technology/facebook-device-partners-users-friends-data.html). 

- [Apple's Siri listens to all the conversations around it](https://www.bloomberg.com/news/features/2019-12-11/silicon-valley-got-millions-to-let-siri-and-alexa-listen-in) and then records it, sending it back to Apple where Apple workers make transcripts of these recordings.

#### DRM

- Apple locks batteries in iPhones using DRM, i.e., the batteries are tied with the device they are shipped with. Apple's proprietary software [turns off certain features when batteries are replaced other than by Apple](https://www.gnu.org/proprietary/malware-apple.html#M201908150). This tramples users' right to repair.

- Apple [does not allow users to charge Apple devices with a generic USB cable](https://www.vice.com/en/article/bmvxp4/switzerland-wants-a-single-universal-phone-charger-by-2017).

### Anti-competitive behaviour

- [Apple charges 30% commission on the transaction fees on payments made to apps through its App Store](https://www.fsf.org/blogs/community/pumpkins-markets-and-one-bad-apple). 

- iPhone users can only install apps available in Apple's App Store. App Store only contains apps approved by Apple. This gives Apple so much power over their users and results in anti-competitive behviour as independent developers need to comply with Apple's (unfair) rules to distribute their software in the App Store as users cannot install apps other than App Store.  

- Apple also prevents you from changing the operating system on the devices, so there's no way to escape the restrictions. If you try to change the software on your device, Apple's lawyers claim you are a criminal under the Digital Millennium Copyright Act (DMCA). They've done this as recently as [December 2019](https://www.vice.com/en_us/article/pkeeay/apple-dmca-take-down-tweet-containing-an-iphone-encryption-key), when they used the DMCA to remove a post to Twitter that revealed an iPhone encryption key. Do you really own the iPhone you bought?

- Apple [removes apps which compete with Apple's apps](https://web.archive.org/web/20171207015519/https://www.macrumors.com/2015/11/12/flux-for-ios-pulled-no-side-loading/). This is one way Apple abuses its power. 

### Planned Obsolescence

- Apple deliberately [degrades the performance of older phones to force users to buy their newer phones](https://www.theguardian.com/technology/2018/oct/24/apple-samsung-fined-for-slowing-down-phones). 

### Censorship

- Apple [banned a Quran app from App Store in China](https://www.theverge.com/2021/10/15/22728257/apple-quran-majeed-app-removed-china). 

- Apple has a deal with Chinese government [to prevent users of China to enter numbers that refer to the date of the Tiananmen Square Massacre, censoring Chinese words like "human rights" or "democracy"](https://www.theverge.com/2021/8/19/22632029/apple-engraving-filter-censorship-keyword-china-hong-kong-taiwan), and to [manipulate maps to show Chinese claimed islands to be bigger than they actually are](https://www.theverge.com/2021/12/10/22826695/apple-china-mou-275-billion-tim-cook-icloud).   

- Apple [censored an app designed to help Hong Kong protesters communicate](https://www.theguardian.com/world/2019/oct/10/hong-kong-protests-apple-pulls-tracking-app-after-china-criticism).

- Apple [removed New York Times app from its App Store](https://www.theguardian.com/world/2017/jan/05/apple-removes-new-york-times-app-in-china). Remember that users have no way to install apps in iPhones from outside the App Store.

- Apple [censored all Bitcoin apps](http://www.theguardian.com/technology/2013/dec/10/apple-blocks-bitcoin-payments-on-secure-messaging-app-gliph).

- Apple [censors information about abortion providers](https://www.alternet.org/story/153248/why_is_iphone%27s_siri_hiding_abortion_info_10_things_the_device_will_help_you_get_instead_of_abortion).

- Apple [banned the novel The Proof of the Honey](http://www.guardian.co.uk/books/2012/nov/16/erotic-novel-removed-itunes-cover) from iTunes.

- Apple [blocked access to its App Store in Iran](https://www.commondreams.org/newswire/2018/03/16/apples-shut-down-iranians-disturbing-trend-us-government-must-reverse). The Apple' total hold on App Store gives it too much power. And it exercises them too. 

- Apple [blocked Telegram from updating its app](https://www.theverge.com/2018/5/31/17412396/telegram-apple-app-store-app-updates-russia). This is because users cannot download apps from outside the App Store and Apple's tight control over the App Store in its iPhones. 

- Apple [removed VPN apps from App Stores in China](https://techcrunch.com/2017/07/30/apples-capitulation-to-chinas-vpn-crack-down-will-return-to-haunt-it-at-home/?ncid=rss&guccounter=1&guce_referrer=aHR0cHM6Ly9zdGFsbG1hbi5vcmcvYXBwbGUuaHRtbA&guce_referrer_sig=AQAAACmKffLmCFvJZcAFjEEwtFw_kgaJeInvyj4aWibg-j8Bou9tbE0uYR2saS1hzHkLRBXFALYRMqQVHPGBP8MgvEEUelblxJ-ptvFbw4jFKrsJOZT2GEwKRGUw85hKpOTGQluz1_UlYP28GTpq24AYsJHpA95Ba8C8hoSoFpfFJkrs).  

- Apple [removed opposition leader Alexei Navalny’s voter guide app from the App Store in Russia](https://gizmodo.com/apple-and-google-pull-opposition-app-from-russian-store-1847695238). Apple is not even a Russian company but it censors for Russia. It can censor for Indian government too. 

- Apple updated its App Store guidelines which now says: ["hookup apps" that include pornography would not be allowed on the App Store](https://web.archive.org/web/20211230132035/https://developer.apple.com/app-store/review/guidelines/). 

Apple could do this because the App Store is nonfree software and users cannot install apps which are not in the App Store. This tight control of Apple over the users leaves them at the mercy of Apple. Don't buy from Apple.  

### Quashing Right to Repair

- Apple's iPhone 7 [gets bricked](https://www.vice.com/en/article/kbjm8e/iphone-7-home-button-unreplaceable-repair-software-lock) when repaired and fixed by someone else other than Apple. 

### Mistreatment of workers

- Apple's devices are [made in sweatshops](https://www.theguardian.com/commentisfree/2010/oct/14/apple-foxconn-china-workers). Sweatshop means a place with very poor working conditions with workers facing issues such as child labour, long working hours, getting very low wages, dangerous workplace conditions usually leading to lifelong injuries. 

- BBC's investigation [reveals Apple mistreating workers in 2014](https://www.theguardian.com/technology/2014/dec/19/apple-under-fire-again-for-working-conditions-at-chinese-factories).

- Sweatshops Are Good For Apple and Foxconn, [But Not For Workers](https://www.scribd.com/document/95395223/Sweatshops-Are-Good-for-Apple-and-Foxconn-But-Not-for-Workers).

- Apple's workers in China worked in conditions so harsh that [many of them took their own life](https://en.wikipedia.org/wiki/Foxconn_suicides). 

### Tax evasion

- Apple [pays next to no tax](http://www.nytimes.com/2012/04/29/business/apples-tax-strategy-aims-at-low-tax-states-and-nations.html?_r=1&pagewanted=1&hp).

### Miscellaneous

-Apple has turned iMessage into a status symbol among US teens, [creating peer pressure](https://www.theverge.com/2022/1/10/22876067/google-apple-ios-android-imessages-bullying-lockheimer) for young people to buy iPhones and sometimes leading to the ostracization of Android users.

- Apple is [systematically undermining interoperability](https://www.theguardian.com/technology/2021/may/30/gadgets-have-stopped-working-together-interoperability-apple). At the hardware level, it does this via nonstandard plugs, buses and networks. At the software level, it does this by not letting the user have any data except within one app. 

- Apple suspected a user of a fraud and [permanently disabled their account for life](https://qz.com/1683460/what-happens-to-your-itunes-account-when-apple-says-youve-committed-fraud/). The person also lost access to all the music, movies and other stuff they purchased from Apple. Let's not buy Apple devices. Let's not fund Apple.  

- Apple can decide to close user accounts [without giving any reasons and chance for a trial](https://merecivilian.com/apple-broke-up-with-me/). The
article says 99% of Apple users won't face this issue but the fact that Apple has this power over users itself is a good enough reason not to buy from Apple. It doesn't have to affect you or anyone else personally to boycott Apple. 

- Apple said that it will [stop fixing the vulnerabilities in Quicktime for Windows](https://www.theregister.com/2016/04/14/uninstall_quicktime_for_windows/). Since the software is proprietary, only Apple can fix it and won't let users to fix. If the Quciktime was [free software](/posts/free-sw), then users would have fixed it themselves. 

- Apple can remotely [cut off any developer's access to the tools for developing software](https://www.theguardian.com/games/2020/aug/18/apple-sets-deadline-in-feud-with-fortnite-maker-epic-games) for iOS or MacOS.

- Apple [falsely claimed their iPhones to be water resistant and didn't provide any support to customers whose phones got damaged by water](https://www.morningstar.com/news/dow-jones/202011301341/italian-antitrust-authority-fines-apple-12-million).
