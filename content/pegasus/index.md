---
title: "Pegasus Spyware"
date: 2021-07-19
---

(Last Updated: 31-July-2021)

On 18-July-2021, [Project Pegasus revelations](https://www.theguardian.com/world/2021/jul/18/revealed-leak-uncovers-global-abuse-of-cyber-surveillance-weapon-nso-group-pegasus) revealed that a spyware named Pegasus is used by authoritarian governments around the world, [including the Indian Government](https://www.theguardian.com/news/2021/jul/19/key-modi-rival-rahul-gandhi-among-indian-targets-of-nso-client), to spy on journalists, dissidents and human rights organizations. The spyware [can secretly infect a mobile phone and harvest its information](https://www.theguardian.com/news/2021/jul/18/what-is-pegasus-spyware-and-how-does-it-hack-phones). Emails, texts, contact books, location data, photos and videos can all be extracted, and a phone’s microphone and camera can be activated to covertly record the user. The blog post also provides arguments on why locking the customers is actually bad for user security and providing user freedom actually imrpoves it. Librem 5 has hardware kill switches and so the users can physically disable the microphone, camera, WiFi and the cellular modem whenever they want to know for sure they have privacy even if the spyware enters their phone.

**Important** : Purism's phone Librem-5 [can defend against a spyware like Pegasus](https://puri.sm/posts/defending-against-spyware-like-pegasus/). Librem-5 respects your freedom and privacy (your phone network will know your location when you make calls; Librem 5 cannot protect you from that). You control your own device and this design of user control helps in protecting you from the Pegasus spyware. Librem 5 has a [lockdown mode](https://puri.sm/posts/lockdown-mode-on-the-librem-5-beyond-hardware-kill-switches/) that can protect against spyware that might log GPS coordinates even if the user disables location services in software. 

A list of domains which are known to contain malicious mobile phone spyware is [jjjxu's Pegasus-Hosts-Formatted.txt](https://raw.githubusercontent.com/jjjxu/NSO_Pegasus_Blocklist/master/Pegasus-Hosts-Formatted.txt). You can block these domains using [Nebulo app](https://github.com/Ch4t4r/Nebulo) to combat malicious mobile phone spyware. This list is a result of research by Amnesty. 

Some important information about Pegasus:

- [Coverage by The Guardian](https://www.theguardian.com/news/series/pegasus-project).

- [Citizen Lab's independent analysis](https://citizenlab.ca/2021/07/amnesty-peer-review/)

- [Blog post by Bruce Schneier](https://www.schneier.com/blog/archives/2021/07/nso-group-hacked.html).

- [An Anatomy of the Pegasus Spyware](https://sflc.in/anatomy-pegasus-spyware) by SFLC India.

- [Edward Snowden's statement](https://www.theguardian.com/news/2021/jul/19/edward-snowden-calls-spyware-trade-ban-pegasus-revelations).

- [Report by Amnesty International](https://www.amnesty.org/en/latest/research/2021/07/forensic-methodology-report-how-to-catch-nso-groups-pegasus/).

- While Apple claims that its phones are very secure against attack by latest surveillance technologies, the iPhones running the latest operating system have [still been penetrated by NSO Group’s Pegasus spyware](https://www.theguardian.com/news/2021/jul/19/how-does-apple-technology-hold-up-against-nso-spyware). Apple's operating system is nonfree and so the users need to wait for Apple to fix the vulnerabilites. A free operating system would've let people to fix the bugs for themselves.

- [Article by The Wire](https://thewire.in/government/project-pegasus-journalists-ministers-activists-phones-spying).

- Also, the leak [shatters the lie](https://www.theguardian.com/news/2021/jul/18/huge-data-leak-shatters-lie-innocent-need-not-fear-surveillance) that the innocent need not fear surveillance.
