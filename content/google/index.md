---
title: "Reasons not to use Google"
date: 2022-01-02
draft: false
--- 
Users of Google do not have to pay any money to use any of Google's software, yet as of writing this, according to Forbes, it is second most valuable brand. So, how is Google earning so much money? Why are they providing services free-of-cost?  

Google is funded by spying on you! They collect a lot of data on each user and sell it to advertisers who target users with ads based on their personal data. [Google knows](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy) every search query that you make, everything you purchase, every website you visit, your location throughout the day, all the emails of the user, deleted emails and files uploaded to Google Drive. 

The root of the problem is that Google's software is nonfree and it does not respect user's freedom. A nonfree software is not controlled by its users. Any [free software](/free-software) is controlled by its users.  

The following is a list of reasons not to use Google: 

### Nonfree/proprietary software

- Google's software is proprietary software. Any nonfree/proprietary software is an injustice to the user because users do not control it. The power of the developer of a proprietary software over the user is [mostly translated into malware functionalities and resulting in the abuse of the user](https://gnu.org/malware). Google's software being proprietary is the root cause of its malware type functionalities. 

### Malware

A software designed to intentionally hurt the user is called malware. 

#### Surveillance

[Quoting this website](https://fuckoffgoogle.de/google-is-evil/), "Google’s business model of collecting all data and profiling everyone for profit set the trend towards generalized practices of mass-surveillance as a dominant business model. Google initiated and normalized what has since then been called “surveillance capitalism”, in which everyone’s data about their private life and most intimate behaviour is seen as “available” for companies to extract, exploit and profit from." 

Also check out [chilling effects of massive surveillance](https://socialcooling.com). [Quoting John Naughton from The Guardian](https://www.theguardian.com/commentisfree/2017/jun/18/google-not-gchq--truly-chilling-spy-network), "It illustrates the way social media and other surveillance companies assemble a “data mosaic” about each user that includes not just the demographic data you’d expect, but also things such as your real (as opposed to your “projected”) sexual orientation, whether you’ve been a victim of rape, had an abortion, whether your parents divorced before you were 21, whether you’re an “empty nester”, are “easily addictable” or “into gardening”, etc. On the basis of these parameters, you are assigned a score that determines not just what ads you might see, but also whether you get a mortgage.

Once people come to understand that (for example) if they have the wrong friends on Facebook they may pay more for a bank loan, then they will start to adjust their behaviour (and maybe change their friends) just to get a better score. They will begin to conform to ensure that their data mosaic keeps them out of trouble. They will not search for certain health-related information on Google in case it affects their insurance premiums. And so on. Surveillance chills, even when it’s not done by the state. And even if you have nothing to hide, you may have something to fear."

Google collects data and then targets ads on users based on that data. [Here is a good read](https://www.eff.org/deeplinks/2022/03/ban-online-behavioral-advertising) on the harms of advertising based on data.

- Snowden's documents show that [Google spies for the NSA](https://www.theguardian.com/world/2013/jun/06/us-tech-giants-nsa-data).

- [Google knows](https://www.theguardian.com/commentisfree/2018/mar/28/all-the-data-facebook-google-has-on-you-privacy) every search query that you make, everything you purchase, every website you visit, your location throughout the day, all the emails of the user, deleted emails and files uploaded to Google Drive.  

- Google's proprietary browser Google Chrome [reports to Google about all the websites visited by the user](https://www.mercurynews.com/2019/06/21/google-chrome-has-become-surveillance-software-its-time-to-switch/). In addition, on Android, Google Chrome reports user's location to Google. If the user has a Gmail account, then that is logged into Chrome to make it easy for Google to spy on the user.  

- [Google Chrome contains a key logger](https://web.archive.org/web/20190126075111/http://www.favbrowser.com/google-chrome-spyware-confirmed/), which means whatever you type into Google Chrome is sent to Google-- for example, all your passwords, all chats etc. 

- Google Play Store [tracks your movements](https://www.extremetech.com/mobile/235594-yes-google-play-is-tracking-you-and-thats-just-the-tip-of-a-very-large-iceberg) to recommend apps based on your location. Fuck nonfree software Google Play Store. Use free software app store [F-Droid](https://f-droid.org) to replace the Google Play Store.

- Google [stores a list of all purchases](https://www.cnbc.com/2019/07/05/google-gmail-purchase-history-cant-be-deleted.html) a user has made that in any way mention the user's a gmail account. 

- Gmail analyzes every email sent through it and [develops pschyologicial profiles on its users and non-users who send emails to Gmail users](https://www.alternet.org/2013/12/google-using-gmail-build-psychological-profiles-hundreds-millions-people/?paging=off).

- Google [handed over protestor Disha Ravi's data to police](https://www.indiatoday.in/technology/news/story/disha-ravi-arrest-puts-privacy-of-all-google-india-users-in-doubt-1769772-2021-02-16).

- Google [targets ads by the highest-bidding advertiser based on user's data](https://www.eff.org/deeplinks/2020/03/google-says-it-doesnt-sell-your-data-heres-how-company-shares-monetizes-and).

<figure>
<img src="/images/how_google_earns_money.png" width="600" height="600">
<figcaption>How Google collects user data and send it to advertisers for real-time bidding.
<br>
Credits: <a href="https://www.eff.org/deeplinks/2020/03/google-says-it-doesnt-sell-your-data-heres-how-company-shares-monetizes-and">EFF</a>
<br>
License: <a href="http://creativecommons.org/licenses/by/3.0/us/">CC-BY</a>. For details, see <a href="https://www.eff.org/copyright">to this page</a>.
</figcaption>
</figure>


The image above shows how ads targeted on Google work. When a user visits a website, Google knows about it (through cookies or by using other forms of tracking). This data about the user visiting a website triggers a bid request by supply-side platforms (or SSPs), which contains sensitive personal data about the user such as user’s demographic information, browsing history, location, and the page being loaded. Then the request goes to an ad exchange which organize auctions between them and advertisers. The advertisers bid in real time to serve ads. The highest bidder’s ad gets published. This also shows that the ads shown to you based on your data might not be for your good which is usually assumed by the people. It is the highest bidder who gets to show you ads. 

#### Censorship

- Google [falsely flagged FairEmail as spyware](https://web.archive.org/web/20220519085159/https://email.faircode.eu/) without a reasonable opportunity to appeal and removed it from their Play Store. Google has too much power and therefore it is hard for an app to be successful of it is not in the Play Store. Let's avoid Google's Play Store and replace it with [F-Droid](https://f-droid.org).
