---
title: "Community-run services"
draft: false
---
Mastodon has written a [good article](https://blog.joinmastodon.org/2017/02/the-power-to-build-communities/) explaining the importance of community-run services and why federation is essential for user freedom and control over the digital spaces like social media. I suggest you to read the article to understand the importance of community-powered services. 

This list contains community-run services like email, online storage (usually known as 'cloud'), Jitsi, BigBluebutton, Etherpad, Matrix, XMPP, Searx search engine etc. These services are usually run by volunteers in their free time and rely on user donations and support from its community and users of services. All the services mentioned here are open to all.

 They can be used as alternatives to the data-collection proprietary services hosted by Google, Microsoft, Facebook etc. 

- [FSCI](https://fsci.in/) 

- [Disroot](https://disroot.org)

- [Nixnet](https://nixnet.services)

- [Riseup](https://riseup.net)

- [Autistici](https://autistici.org)

- [Pussthecat](https://pussthecat.org)

- [diasp.in](https://diasp.in/)

- [Snopyta](https://snopyta.org) [Onion Link](http://cct5wy6mzgmft24xzw6zeaf55aaqmo6324gjlsghdhbiw5gdaaf4pkad.onion/)

- Check Riseup's [list of servers](https://riseup.net/en/security/resources/radical-servers). 

