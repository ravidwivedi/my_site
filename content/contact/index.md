---
title: "Contact"
date: 2021-07-12
---
Let's use only [free software](/free-software) to communicate. You can consult [this list](/free-software-list) to explore free software options.
 
- The easiest way to chat with me is to download Quicksy app from [Google Play Store](https://play.google.com/store/apps/details?id=im.quicksy.client&hl=en_US&gl=US) or [F-Droid](https://www.f-droid.org/en/packages/im.quicksy.client/). If you have my phone number, then my contact will automatically show in your Quicksy account, otherwise you can add me by clicking [this link](https://blabber.im/en/i/ravi@poddery.com) and then click on 'ADD RAVI' box, which you can open in the Quicksy app.

	You can also add me manually in Quicksy or using any other [XMPP app](https://xmpp.org/software/clients.html) using my XMPP address: ravi@poddery.com.  If you are curious why I recommended Quicksy, please read [this article](/posts/quicksy-app). Read here for [reasons not to use WhatsApp](/whatsapp). 

- Email: ravi at ravidwivedi dot in .

	Please don't forget to encrypt your mails with [my GPG keys](/gpg). 

	If you aren't encrypting your emails already, [try pep](https://www.pep.security/en/), which is a [free/swatantra software](https://www.gnu.org/philosophy/free-software-even-more-important.html), very easy to use and your mails will be encrypted by default. A guide for encryption using pep is [here](https://fsci.in/blog/pep-guide/).

	Further, I recommend you to read [useplaintext.email](https://useplaintext.email/) which explains why you should write emails in plaintext and not in HTML.

- My IRC nickname is ravi on irc.libera.chat.

- Mastodon <a rel="me" href="https://masto.sahilister.in/@ravi">@ravi@masto.sahilister.in</a> 
