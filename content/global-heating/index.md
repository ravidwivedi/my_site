---
title: "Global Heating"
date: 2022-03-23
draft: false
---
"Global heating" is [a more accurate term](https://www.theguardian.com/environment/2018/dec/13/global-heating-more-accurate-to-describe-risks-to-planet-says-key-scientist) than "global warming" to describe the changes taking place to the world’s climate.

[Human actions](https://ourworldindata.org/ghg-emissions-by-sector) in past few decades [lead to rapid rise in temperature](https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_Chapter1_High_Res.pdf) and weather patterns. [Just 100 companies are responsible for 71% of global greenhouse gas emissions](https://www.theguardian.com/sustainable-business/2017/jul/10/100-fossil-fuel-companies-investors-responsible-71-global-emissions-cdp-study-climate-change). US oil company [Exxon knew about global heating for at least 50 years](https://exxonknew.org/), but instead [launched a widespread misinformation campaign to deny global heating](insideclimatenews.org/news/22102015/Exxon-Sowed-Doubt-about-Climate-Science-for-Decades-by-Stressing-Uncertainty). French company [Total also knew and did the same as Exxon](https://www.commondreams.org/news/2021/10/20/bombshell-total-knew-about-climate-threat-fossil-fuels-decades-denied-it).

[Wired's complete guide to climate change](https://www.wired.com/story/guide-climate-change/) is a very good read on this topic.

### Consequences

Global heating has grave consequences. [Greta Thunberg's quote](https://web.archive.org/web/20200131211423/https://www.aljazeera.com/ajimpact/trump-attend-davos-impeachment-iran-concerns-loom-200114141412998.html) "Our house is on fire" captures the urgency of the situation. 

- India [could lose the equivalent of 34 million jobs](https://www.thehindubusinessline.com/news/india-could-lose-the-equivalent-of-34-million-jobs-in-2030-due-to-global-warming-says-ilo/article28259436.ece) in 2030 due to global heating.

- In India, [onion prices are hiking due to global heating](https://www.thehindu.com/news/national/karnataka/climate-change-behind-spike-in-onion-price-every-alternate-year-study/article30232967.ece).

- In 2018, India saw extreme weather conditions, [causing economic losses of Rs 2.7 lakh crore](https://scroll.in/article/946017/india-is-the-fifth-most-vulnerable-country-to-effects-of-climate-change-says-new-report) – nearly as much as its defence budget.

- India’s [economic loss due to climate change](https://scroll.in/article/953029/the-reserve-bank-of-india-cant-counter-food-inflation-without-factoring-in-extreme-weather-events) was a hefty $37 billion in 2018. Between 2012 and 2017, Indian banks took a hit of $8.4 billion after many agricultural loans turned bad following crop losses due to extreme weather events.

- Between 2000 and 2019, [over 475,000 people lost their lives](https://www.hindustantimes.com/india-news/india-seventh-most-affected-by-climate-change-in-2019-globally-report-101611552192319.html) as a direct result of more than 11,000 extreme weather events globally and losses amounted to around US $2.56 trillion.

- Maldives and Tuvalu are [at a risk of submerging in the near future](https://www.unhcr.org/4df9cb0c9.pdf). 

- Floods are [on the rise due to the global heating](https://www.nrdc.org/stories/flooding-and-climate-change-everything-you-need-know).

- Loss of sea ice, accelerated sea level rise and longer, more intense heat waves [are now occurring due to climate change](https://web.archive.org/web/20220221091306/https://climate.nasa.gov/effects/).

- In recent years, [wildfires in California have grown measurably more intense](https://www.wired.com/story/guide-climate-change/) thanks to climate change. They burn hotter and faster, leading to catastrophes like the Camp Fire north of San Francisco, which [virtually obliterated](https://www.wired.com/story/the-terrifying-science-behind-californias-massive-camp-fire/) the 27,000-person town of Paradise, becoming the deadliest and most destructive wildfire in state history. 

- Australia suffered an [unprecedentedly brutal fire season](https://www.wired.com/story/australias-bushfires/), giving the world perhaps the most dramatic manifestation of the climate crisis to date. 

- Permafrost is thawing so fast, [it’s gouging holes in the Arctic](https://www.wired.com/story/abrupt-permafrost-thaw/).

- Heatwaves in the waters of Indian Ocean [are reducing monsoonal rains over central India, while the rapid warming of the Indian Ocean’s northern portions is intensifying cyclones](https://thewire.in/environment/climate-change-is-altering-the-dynamics-of-the-indian-ocean).

- Extreme flooding due to climate change [leads to deaths in Indonesia and Mozambique](https://www.newscientist.com/article/2201224-extreme-flooding-leads-to-deaths-in-indonesia-and-mozambique/).

### Solutions

Scientific studies on the matter are public and any government can read them and solve the issues. Solution is a matter of political will, although there are some individual steps we can take.

We need to cut down emissions of greenhouse gases to zero.

If [Indian Pirates](https://pirates.org.in) forms the government, we will take steps to solve the issue.
